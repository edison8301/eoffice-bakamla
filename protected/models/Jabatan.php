<?php

/**
 * This is the model class for table "jabatan".
 *
 * The followings are the available columns in table 'jabatan':
 * @property integer $id
 * @property integer $id_atasan
 * @property string $nama_jabatan
 * @property string $pemangku
 * @property string $nama_pemangku
 * @property integer $id_jabatan_jenis
 */
class Jabatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public function tableName()
	{
		return 'jabatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_induk, id_jabatan_jenis, id_jabatan_eselon', 'numerical', 'integerOnly'=>true),
			array('nama, singkatan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_induk, nama, pemangku, nama_pemangku, id_jabatan_jenis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jabatan_jenis'=>array(self::BELONGS_TO,'JabatanJenis','id_jabatan_jenis'),
			'jabatan_eselon'=>array(self::BELONGS_TO,'JabatanEselon','id_jabatan_eselon'),
			'tata_usaha'=>array(self::HAS_ONE,'TataUsaha','id_jabatan_induk')

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_induk' => 'Atasan',
			'nama_jabatan' => 'Nama Jabatan',
			'pemangku' => 'Pemangku',
			'nama_pemangku' => 'Nama Pemangku',
			'id_jabatan_jenis' => 'Jenis Jabatan',
			'id_jabatan_eselon' => 'Eselon Jabatan'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_induk',$this->id_induk);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('pemangku',$this->pemangku,true);
		$criteria->compare('nama_pemangku',$this->nama_pemangku,true);
		$criteria->compare('id_jabatan_jenis',$this->id_jabatan_jenis);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function dialog()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($_GET['Jabatan']))
			$this->attributes = $_GET['Jabatan'];

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_induk',$this->id_induk);
		$criteria->compare('nama',$this->nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
        		'pageSize'=>5,
    		),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Jabatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function findAllSubjabatan()
	{
		$model = Jabatan::model()->findAllByAttributes(array('id_induk'=>$this->id));
		if($model!==null)
			return $model;
		else
			return false;

	}

	public static function getListAutoComplete()
	{
		$list = array();

		foreach(Jabatan::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public static function findByUserId()
	{
		$username = Yii::app()->user->id;
		$model = Jabatan::model()->findByAttributes(array('username'=>$username));
		
		if($model!==null)
			return $model;
		else
			return null;
	}

	public static function findAllStruktural()
	{
		return Jabatan::model()->findAllByAttributes(array('id_jabatan_jenis'=>1));
	}

	public function findAtasan()
	{
		$model = Jabatan::model()->findByPk($this->id_induk);

		if($model!==null)
			return $model;
		else
			return null;
	}

	public function countPegawai()
	{
		return Pegawai::model()->countByAttributes(array('id_jabatan'=>$this->id));
	}

	public function createParaf($id_surat)
	{
		
		if($this->hasTataUsaha())
		{
			$tu = $this->findJabatanTataUsaha();

			if($tu!==null)
				$tu->createParaf($id_surat);
		}
	
		$paraf = new Paraf;
		$paraf->id_surat = $id_surat;
		$paraf->setUrutan();
		$paraf->id_jabatan = $this->id;
		$paraf->username_pemaraf = Pegawai::getUsernameByIdJabatan($this->id);
		$paraf->id_paraf_status = 2;

		$paraf->urutan_tampil = $this->id_jabatan_eselon*10;

		if($this->isTataUsaha())
			$paraf->urutan_tampil = 84;

		$paraf->save();

		return true;
	}

	public function createParafAtasan($id_surat,$id_jabatan_penandatangan)
	{
		$jabatan = $this->findAtasan();

		if($jabatan!==null)
		{
			$jabatan->createParaf($id_surat);

			if($jabatan->id_jabatan_eselon != 1 AND $jabatan->id_induk != 2) 
			{
				if($jabatan->id != $id_jabatan_penandatangan)
					$jabatan->createParafAtasan($id_surat,$id_jabatan_penandatangan);

			}
		}

		return true;
	}

	public function createParafPersuratan($id_surat,$id_jabatan_penandatangan)
	{
		if($id_jabatan_penandatangan==1)
		{
			$jabatan = array(
				array('id_jabatan'=>63,'urutan_tampil'=>80), //Subgag Persuratan PAD
				array('id_jabatan'=>15,'urutan_tampil'=>11), //biro umum
				array('id_jabatan'=>70,'urutan_tampil'=>82), //TU Sestama
				array('id_jabatan'=>2,'urutan_tampil'=>9), //Sestama
				array('id_jabatan'=>69,'urutan_tampil'=>81), //TU Kepala
				array('id_jabatan'=>1,'urutan_tampil'=>8), //Kepala
			);
		}

		if($id_jabatan_penandatangan==2)
		{
			$jabatan = array(
				array('id_jabatan'=>63,'urutan_tampil'=>80), //Subgag Persuratan PAD
				array('id_jabatan'=>15,'urutan_tampil'=>11), //biro umum
				array('id_jabatan'=>70,'urutan_tampil'=>82), //TU Sestama
				array('id_jabatan'=>2,'urutan_tampil'=>9), //Sestama
			);
		}

		foreach($jabatan as $data)
		{
			$paraf = new Paraf;
			$paraf->id_surat = $id_surat;
			$paraf->setUrutan();
			$paraf->id_jabatan = $data['id_jabatan'];
			$paraf->username_pemaraf = Pegawai::getUsernameByIdJabatan($data['id_jabatan']);
			$paraf->id_paraf_status = 2;

			$paraf->urutan_tampil = $data['urutan_tampil'];

			$paraf->save();
		}

		return true;
	}

	public function createDistribusi(Distribusi $distribusi)
	{
		if($this->hasTataUsaha())
		{
			$tu = $this->findJabatanTataUsaha();

			if($tu!==null)
				$tu->createDistribusi($distribusi);
		}

		$model = new Distribusi;
		$model->attributes = $distribusi->attributes;

		$model->id_jabatan_penerima = $this->id;
		$model->username_penerima = Pegawai::getUsernameByIdJabatan($this->id);

		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dibuat = date('Y-m-d H:i:s');

		$model->save();

		return true;
	}

	public static function getNamaJabatanById($id)
	{
		$model = Jabatan::model()->findByPk($id);
		if(!empty($model->nama))
			return $model->nama;
		else
			return null;
	}

	public static function getNamaById($id)
	{
		$model = Jabatan::model()->findByPk($id);
		if(!empty($model->nama))
			return $model->nama;
		else
			return null;
	}

	public static function findByNama($nama)
	{
		$model = Jabatan::model()->findByAttributes(array('nama'=>$nama));
		if($model!==null)
			return $model;
		else
			return null;
	}

	public function findAllPegawai()
	{
		return Pegawai::model()->findAllByAttributes(array('id_jabatan'=>$this->id));
	}

	public function findAllParafByStatus($status)
	{
		$model = Paraf::model()->findAllByAttributes(array('id_jabatan'=>$this->id,'id_paraf_status'=>$status));

		return $model;
	}

	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getNamaPemangku()
	{
		return Pegawai::getNamaByIdJabatan($this->id);
	}

	public static function getNamaPemangkuByIdJabatan($id_jabatan)
	{
		return Pegawai::getNamaByIdJabatan($id_jabatan);
	}

	public static function getIdByNamaJabatan($nama_jabatan)
	{
		$model = Jabatan::model()->findByAttributes(array('nama'=>$nama_jabatan));
		if($model!==null)
			return $model->id;
		else
			return null;
	}

	public function findEselon($eselon)
	{
		if($this->id_jabatan_eselon==$eselon)
			return $this;
		else {
			$atasan = $this->findAtasan();
			if($atasan!==null)
				return $atasan->findEselon($eselon);
			else
				return null;
		}
	}

	public function setJabatanEselon()
	{
		if($this->id_jabatan_jenis!=1)
		{
			$this->id_jabatan_eselon=9;
		}

		return true;

	}

	public function getSingkatan()
	{
		if($this->singkatan!=null)
			return $this->singkatan;
		else
			return $this->nama;
	}

	public function hasTataUsaha()
	{
		$jumlah = TataUsaha::model()->countByAttributes(array('id_jabatan_induk'=>$this->id));

		if($jumlah>0)
			return true;
		else
			return false;
	}

	public function isTataUsaha()
	{
		$jumlah = TataUsaha::model()->countByAttributes(array('id_jabatan'=>$this->id));
		
		if($jumlah>0)
			return true;
		else
			return false;
	}

	public function findJabatanTataUsaha()
	{
		$criteria = new CDbCriteria;
		
		$tu = TataUsaha::model()->findByAttributes(array('id_jabatan_induk'=>$this->id));

		if($tu!==null)
			return Jabatan::model()->findByPk($tu->id_jabatan);
		else
			return null;

	}

	public function getIdJabatanTU()
	{
		$id_jabatan_tu = null;

		if($this->id==1)
			$id_jabatan_tu = 69;

		if($this->id==2)
			$id_jabatan_tu = 70;

		if($this->id==3)
			$id_jabatan_tu = 71;

		if($this->id==4)
			$id_jabatan_tu = 72;

		if($this->id==5)
			$id_jabatan_tu = 73;

		if($this->id==6)
			$id_jabatan_tu = 74;

		return $id_jabatan_tu;
	}

	public function getIdJabatanEselonTU()
	{
		return 4;
	}

	public function findAllStaf()
	{
		return Pegawai::model()->findAllByAttributes(array('id_jabatan_induk'=>$this->id));
	}
}
