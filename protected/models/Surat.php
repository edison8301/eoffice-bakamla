<?php

/**
 * This is the model class for table "surat_keluar".
 *
 * The followings are the available columns in table 'surat_keluar':
 * @property integer $id
 * @property string $no_surat
 * @property integer $id_surat_keluar_jenis
 * @property string $file
 * @property integer $id_surat_keluar_status
 * @property string $pembuat
 * @property string $waktu_dibuat
 */
class Surat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $penomoran;

	public $paraf;
	
	public function tableName()
	{
		return 'surat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_surat_jenis, id_surat_sifat, id_jabatan_penandatangan', 'required','message'=>'{attribute} harus diisi'),
			array('id_surat_jenis, id_jabatan_penandatangan, id_jabatan_penerbit, id_surat_sifat, id_surat_status, id_jabatan_pembuat, id_jabatan_pengaju, id_surat_kop', 'numerical', 'integerOnly'=>true),
			array('nomor, username_pengaju, username_pembuat, username_penerbit, username_penandatangan', 'length', 'max'=>255),
			array('waktu_dibuat, tembusan, tujuan, tujuan_jabatan, tujuan_pegawai, tembusan_jabatan, tembusan_laporan, tembusan_pegawai, waktu_diajukan,ringkasan, waktu_terbit, penomoran, tanggal', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomor, id_surat_jenis, file, waktu_diterbitkan, id_surat_status, pembuat, 
				waktu_dibuat', 'safe', 'on'=>'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'surat_status'=>array(self::BELONGS_TO,'SuratStatus','id_surat_status'),
			'surat_jenis'=>array(self::BELONGS_TO,'SuratJenis','id_surat_jenis'),
			'surat_sifat'=>array(self::BELONGS_TO,'SuratSifat','id_surat_sifat'),
			'jabatan_pengaju'=>array(self::BELONGS_TO,'Pegawai','id_jabatan_pengaju'),
			'jabatan_pembuat'=>array(self::BELONGS_TO,'Pegawai','id_jabatan_pembuat'),
			'jabatan_penandatangan'=>array(self::BELONGS_TO,'Jabatan','id_jabatan_penandatangan'),
			'distribusi'=>array(self::HAS_MANY,'Distribusi','id_surat')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor',
			'tanggal'=>'Tanggal',
			'berkas'=>'Berkas Final',
			'id_surat_jenis' => 'Jenis',
			'file' => 'File',
			'id_surat_status' => 'Status',
			'id_surat_kop'=>'Kop Surat',
			'pembuat' => 'Pembuat',
			'tujuan_jabatan' => 'Tujuan (Jabatan)',
			'tujuan_pegawai' => 'Tujuan (Pegawai)',
			'tembusan_jabatan' => 'Tembusan (Jabatan)',
			'tembusan_laporan' => 'Tembusan (Sebagai Laporan)',
			'tembusan_jabatan' => 'Tembusan (Jabatan)',
			'id_surat_sifat'=>'Sifat',
			'paraf'=>'Paraf/Otentifikasi',
			'penomoran'=>'Pemberian Nomor dan Tanggal',
			'id_jabatan_penandatangan' => 'Penandatangan',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('id_surat_jenis',$this->id_surat_jenis);
		$criteria->compare('id_surat_status',$this->id_surat_status);
		$criteria->compare('username_pembuat',$this->username_pembuat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		if(!User::isAdmin())
		{
			$criteria->compare('id_surat_status',1);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function terbit()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->addCondition('id_surat_status = 1');

		if(isset($_GET['sumber']))
		{
			if($_GET['sumber']=='dibuat')
			{
				$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR id_jabatan_pengaju = :id_jabatan OR t.username_pembuat = :username OR username_pengaju = :username');
			}

			if($_GET['sumber']=='tembusan')
			{	
				$criteria->with = array('distribusi');
				$criteria->together = true;

				$criteria->addCondition('distribusi.id_distribusi_jenis = 1');
				$criteria->addCondition('distribusi.id_jabatan_penerima = :id_jabatan OR distribusi.username_penerima = :username');
			}
			
			if($_GET['sumber']=='disposisi')
			{
				$criteria->with = array('distribusi');
				$criteria->together = true;

				$criteria->addCondition('distribusi.id_distribusi_jenis = 2');
				$criteria->addCondition('(distribusi.id_jabatan_penerima = :id_jabatan OR distribusi.username_penerima = :username) AND distribusi.id_distribusi_jenis = 2');
			}

			if($_GET['sumber']=='terusan')
			{
				$criteria->with = array('distribusi');
				$criteria->together = true;

				$criteria->addCondition('distribusi.id_distribusi_jenis = 3');
				$criteria->addCondition('(distribusi.id_jabatan_penerima = :id_jabatan OR distribusi.username_penerima = :username) AND distribusi.id_distribusi_jenis = 3');
			}

		} else {
			
			$criteria->with = array('distribusi');
			$criteria->together = true;			
			
			$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username OR id_jabatan_pengaju = :id_jabatan OR username_pengaju = :username OR distribusi.id_jabatan_penerima = :id_jabatan OR distribusi.username_penerima = :username');

		}

		if(isset($_GET['Surat']['nomor']))
		{
			$criteria->addCondition("nomor LIKE :nomor");
			$params[':nomor']="%".$_GET['Surat']['nomor']."%";
		}

		if(isset($_GET['Surat']['ringkasan']))
		{
			$criteria->addCondition("ringkasan LIKE :ringkasan");
			$params[':ringkasan']="%".$_GET['Surat']['ringkasan']."%";
		}

		if(isset($_GET['Surat']['tanggal']))
		{
			$criteria->addCondition("tanggal LIKE :tanggal");
			$params[':tanggal']="%".$_GET['Surat']['tanggal']."%";
		}
		
		$params[':username'] = Yii::app()->user->id;
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;

		$criteria->compare('id_surat_jenis',$this->id_surat_jenis);
		$criteria->compare('waktu_diterbitkan',$this->waktu_diterbitkan,true);
		
		//$criteria->group = 't.nomor';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function siap()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_surat_status = 5');

		if(Pegawai::isStruktural()) 
		{
			$criteria->addCondition('id_jabatan_penandatangan = :id_jabatan OR id_jabatan_penandatangan IN (SELECT id_jabatan_penandatangan FROM penerbit WHERE id_jabatan_penerbit = :id_jabatan)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
		} else {
			$criteria->addCondition('username_penandatangan = :username OR id_jabatan_penandatangan IN (SELECT id_jabatan_penandatangan FROM penerbit WHERE username_penerbit = :username)');
			$params[':username'] = Yii::app()->user->id;
		}
		
		if(isset($_GET['Surat']['ringkasan']))
		{
			$criteria->addCondition("ringkasan LIKE :ringkasan");
			$params[':ringkasan']="%".$_GET['Surat']['ringkasan']."%";
		}

		

		$criteria->params = $params;

		$criteria->compare('id_surat_jenis',$this->id_surat_jenis);
		$criteria->compare('waktu_diterbitkan',$this->waktu_diterbitkan,true);
		
		//$criteria->group = 't.nomor';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function isCreateRevisiPermitted()
	{
		return true;
	}

	public static function getListAutoCompleteTujuan()
	{
		$list = array();

		$list[]='Semua Pegawai';

		foreach(Jabatan::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(Pegawai::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(DistribusiGrup::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public static function getListAutoCompleteTembusan()
	{
		$list = array();

		$list[]='Semua Pegawai';

		foreach(Jabatan::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(Pegawai::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(DistribusiGrup::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public static function getListAutoCompleteDisposisi()
	{
		$list = array();

		foreach(Jabatan::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(Pegawai::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public static function getListAutoCompleteTerusan()
	{
		$list = array();

		$list[] = 'Semua Pegawai';

		foreach(Jabatan::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		foreach(Pegawai::model()->findAll() as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public function setIdJabatanPenerbit()
	{
		if($this->id_jabatan_penandatangan == 1)
			$this->id_jabatan_penerbit = 69;

		if($this->id_jabatan_penandatangan == 2)
			$this->id_jabatan_penerbit = 70;

		if($this->id_jabatan_penandatangan == 3)
			$this->id_jabatan_penerbit = 71;

		if($this->id_jabatan_penandatangan == 4)
			$this->id_jabatan_penerbit = 72;

		if($this->id_jabatan_penandatangan == 5)
			$this->id_jabatan_penerbit = 73;

		if($this->id_jabatan_penandatangan == 6)
			$this->id_jabatan_penerbit = 73;

		return true;
	}

	public function draf()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('ringkasan',$this->ringkasan,true);
		$criteria->compare('id_surat_jenis',$this->id_surat_jenis);
		$criteria->compare('id_surat_status',$this->id_surat_status);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		$criteria->compare('username_pembuat',Yii::app()->user->id);

		$criteria->order = 'waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SuratKeluar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function findAllParaf()
	{
		$model = Paraf::model()->findAllByAttributes(array('id_surat'=>$this->id),array('order'=>'urutan_tampil ASC'));
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function findAllDistribusi()
	{
		$model = Distribusi::model()->findAllByAttributes(array('id_surat'=>$this->id),array('order'=>'waktu_dibuat ASC'));
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function findAllLampiran()
	{
		$model = Lampiran::model()->findAllByAttributes(array('id_model'=>$this->id,'model'=>'Surat'),array('order'=>'waktu_dibuat ASC'));
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function findAllDistribusiByJenis($jenis)
	{
		$model = Distribusi::model()->findAllByAttributes(array('id_surat'=>$this->id,'id_distribusi_jenis'=>$jenis),array('order'=>'waktu_dibuat ASC'));
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function findParafByIdJabatan($id_jabatan)
	{
		$model = Paraf::model()->findByAttributes(array('id_surat'=>$this->id,'id_jabatan'=>$id_jabatan));
		return $model;
	}

	public function findParafByStatus($status)
	{
		$model = Paraf::model()->findByAttributes(array('id_surat'=>$this->id,'id_paraf_status'=>$status),array('order'=>'urutan ASC'));
		return $model;
	}

	public function findAllParafByStatus($status)
	{
		$model = Paraf::model()->findAllByAttributes(array('id_surat'=>$this->id,'id_paraf_status'=>$status),array('order'=>'urutan ASC'));
		return $model;
	}

	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function setUsernamePenerbit()
	{
		$jabatan = Jabatan::model()->findByPk($this->id_jabatan_penandatangan);

		if($jabatan!==null AND $jabatan->id_jabatan_jenis!=1)
		{
			$this->username_penandatangan = Yii::app()->user->id;
		}

		return true;
	}

	public function getSuratStatus()
	{
		if(!empty($this->surat_status->nama))
			return $this->surat_status->nama;
		else
			return null;
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function updateStatus()
	{
		//Status Disetujui
		if($this->id_surat_status == 1 )
		{
			if($this->waktu_disetujui == null)
			{
				date_default_timezone_set('Asia/Jakarta');
				$this->waktu_disetujui = date('Y-m-d H:i:s');
				$this->save();
			}
		}

		//Status Drafting
		if($this->id_surat_status == 2) 
		{
			if($this->waktu_dibuat == null)
			{
				date_default_timezone_set('Asia/Jakarta');
				$this->waktu_dibuat = date('Y-m-d H:i:s');
				$this->save();
			}
		}

		//Status Review
		if($this->id_surat_status == 3)
		{
			if($this->waktu_diajukan == null)
			{
				date_default_timezone_set('Asia/Jakarta');
				$this->waktu_diajukan = date('Y-m-d H:i:s');
				$this->save();	
			}

			if($this->countParafByStatus(4)!=0)
			{
				$perbaikan = $this->findParafByStatus(4);
				$perbaikan->id_paraf_status = 3;
				$perbaikan->save();
				
				$perbaikan->createPemberitahuanPemaraf();

			} else {
				$paraf = $this->findParafByStatus(2);
					
				if($paraf!==null)
				{
					$paraf->id_paraf_status = 3;
					$paraf->save();

					$paraf->createPemberitahuanPemaraf();
				}
			}

			$jumlah_paraf = $this->countParaf();
			$jumlah_disetujui = $this->countParafByStatus(1);
			$jumlah_tunggu = $this->countParafByStatus(2);
			$jumlah_review = $this->countParafByStatus(3); 
			$jumlah_perbaikan = $this->countParafByStatus(4);
			$jumlah_dilewati = $this->countParafByStatus(5);

			if($jumlah_disetujui == $jumlah_paraf-$jumlah_dilewati)
			{
				$this->id_surat_status = 5;

				date_default_timezone_set('Asia/Jakarta');
				$this->waktu_disetujui = date('Y-m-d H:i:s');
				$this->save();
			}
		}

		if($this->id_surat_status == 4)
		{

		}

		if($this->id_surat_status == 5)
		{
			
		} 

		return true;
	}

	public function getNamaPembuat()
	{
		$model = Pegawai::model()->findByAttributes(array('username'=>$this->username_pembuat));
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getNomor()
	{
		if($this->nomor!=null)
		{
			return $this->nomor;
		} else {
			$this->setNomor();
			return $this->nomor;
		}
	}

	public function getTanggal()
	{
		if($this->tanggal!=null)
		{
			return $this->tanggal;
		} else {
			date_default_timezone_set('Asia/Jakarta');
			return date('Y-m-d');
		}
	}

	public function getNamaPengaju()
	{
		$model = Pegawai::model()->findByAttributes(array('username'=>$this->username_pengaju));
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getPengaju()
	{
		$output = Jabatan::getNamaById($this->id_jabatan_pengaju);
		$output .= ' ('. Pegawai::getNamaByUsername($this->username_pengaju).')';

		return $output;
	}

	public function countParaf($status=null)
	{
		if($status==null)
			return Paraf::model()->countByAttributes(array('id_surat'=>$this->id));
		else
			return Paraf::model()->countByAttributes(array('id_surat'=>$this->id,'id_paraf_status'=>$status));
	}

	public function countParafByStatus($status)
	{
		return Paraf::model()->countByAttributes(array('id_surat'=>$this->id,'id_paraf_status'=>$status));
	}

	public function getJabatanPembuat()
	{
		$model = Jabatan::model()->findByAttributes(array('id'=>$this->id_jabatan_pembuat));
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getJabatanPengaju()
	{
		$model = Jabatan::model()->findByAttributes(array('id'=>$this->id_jabatan_pengaju));
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getContohSurat()
	{
		$model = SuratKeluarJenis::model()->findByPk($this->id_surat_keluar_jenis);
		if($model!==null)
			return $model->contoh;
		else
			return null;
	}

	public function createAllParaf()
	{
		$id_surat = $this->id;
		$id_jabatan_penandatangan = $this->id_jabatan_penandatangan;
		$id_jabatan_pengaju = $this->id_jabatan_pengaju;

		$jabatan = Pegawai::findJabatanByUserId();

		if($jabatan->id!=$id_jabatan_pengaju)
		{
			$jabatanPengaju = Jabatan::model()->findByPk($id_jabatan_pengaju);
			$jabatanPengaju->createParaf($id_surat);
		}
		
		if($id_jabatan_penandatangan!=$jabatan->id)
		{
			$jabatan->createParafAtasan($id_surat,$id_jabatan_penandatangan);
		}
		
		if($id_jabatan_penandatangan==1 OR $id_jabatan_penandatangan==2)
		{
			$jabatan->createParafPersuratan($id_surat,$id_jabatan_penandatangan);
		}

		return true;	
	}

	public function setParafUmum()
	{
		$jabatan = Pegawai::findJabatanByUserId();

		if($jabatan->id_induk!=0)
		{
			$id_surat = $this->id;
			$jabatan->setParafAtasan($id_surat);
		}

		$jabatan->setParafPersuratan($id_surat);

		return true;
	}

	public function getSuratKop($bagian)
	{
		if($this->id_surat_kop==null)
			$this->id_surat_kop = 3;

		$kop = SuratKop::model()->findByPk($this->id_surat_kop);
		$output = $kop->$bagian;

		$output = str_replace('[base_url]',Yii::app()->baseUrl,$output);

		return $output;
	}

	public function setParafMemo()
	{
		return true;
	}

	public function findSuratKeluarJenis()
	{
		$model = SuratKeluarJenis::model()->findByPk($this->id_surat_keluar_jenis);
		return $model;
	}

	public function findAllByStatus($status)
	{
		return SuratKeluar::model()->findAllByAttributes(array('id_surat_keluar_status'=>$status));
	}

	public static function getdataChartSuratKeluarColumn()
	{
	$list = array();
 		foreach(SuratKeluarJenis::model()->findAll() as $data) 
  		{ 
			$list[] = array('type'=>'column','name'=>$data->nama,'data'=>array((int)$data->getCountData()));
  		}
  		return $list;
	}

	public static function getdataChartSuratKeluarPie()
	{
	$list = array();
 		foreach(SuratKeluarJenis::model()->findAll() as $data) 
  		{ 
			$list[] = array($data->nama,(int)$data->getCountData());
  		}
  		return $list;
	}

	public static function getChartSuratKeluarPerBulan()
	{
		$list = array();
		$year = date('Y');

		for($i=1;$i<=12;$i++)
		{
   		 	$criteria = new CDbCriteria;

    		$bulan = $i;
    		if($i<=10) $bulan = '0'.$i;

   			$awal = $year.'-'.$bulan.'-01';
    		$akhir = $year.'-'.$bulan.'-31';
    
	    	$criteria->condition = 'waktu_dibuat >= :awal AND waktu_dibuat <= :akhir';
	    	$criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);
    
	    	$jumlah = SuratKeluar::model()->count($criteria);

	    	$nama_bulan = '';
	    	if($i==1) $nama_bulan = 'Jan';
	    	if($i==2) $nama_bulan = 'Feb';
	    	if($i==3) $nama_bulan = 'Mar';
	    	if($i==4) $nama_bulan = 'Apr';
	    	if($i==5) $nama_bulan = 'Mei';
	    	if($i==6) $nama_bulan = 'Jun';
	    	if($i==7) $nama_bulan = 'Jul';
	    	if($i==8) $nama_bulan = 'Aug';
	    	if($i==9) $nama_bulan = 'Sep';
	    	if($i==10) $nama_bulan = 'Oct';
	    	if($i==11) $nama_bulan = 'Nov';
	    	if($i==12) $nama_bulan = 'Des';

    		$list[] = array('type'=>'column','name'=>$nama_bulan,'data'=>array((int)$jumlah));
		}
		
		return $list;
	}

	public static function getChartSuratPerBulan()
	{
		$list = array();
		$year = date('Y');

		for($i=1;$i<=12;$i++)
		{
   		 	$criteria = new CDbCriteria;

    		$bulan = $i;
    		if($i<=10) $bulan = '0'.$i;

   			$awal = $year.'-'.$bulan.'-01';
    		$akhir = $year.'-'.$bulan.'-31';
    
	    	$criteria->condition = 'waktu_dibuat >= :awal AND waktu_dibuat <= :akhir';
	    	$criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);
    
	    	$jumlah = Surat::model()->count($criteria);

	    	$nama_bulan = '';
	    	if($i==1) $nama_bulan = 'Jan';
	    	if($i==2) $nama_bulan = 'Feb';
	    	if($i==3) $nama_bulan = 'Mar';
	    	if($i==4) $nama_bulan = 'Apr';
	    	if($i==5) $nama_bulan = 'Mei';
	    	if($i==6) $nama_bulan = 'Jun';
	    	if($i==7) $nama_bulan = 'Jul';
	    	if($i==8) $nama_bulan = 'Aug';
	    	if($i==9) $nama_bulan = 'Sep';
	    	if($i==10) $nama_bulan = 'Oct';
	    	if($i==11) $nama_bulan = 'Nov';
	    	if($i==12) $nama_bulan = 'Des';

    		$list[] = array('type'=>'column','name'=>$nama_bulan,'data'=>array((int)$jumlah));
		}
		
		return $list;
	}

	public function getAtribut($nama)
	{
		$model = SuratAtribut::model()->findByAttributes(array('id_surat'=>$this->id,'nama'=>$nama));
		if($model!==null)
			return $model->nilai; 
		else
			return null;
	}

	public function findAllDisposisi()
	{
		$model = Disposisi::model()->findAllByAttributes(array('model'=>'SuratKeluar','id_model'=>$this->id));
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function getUrlLampiran()
	{
		if($this->lampiran!=null)
			return Yii::app()->baseUrl."/uploads/surat/".$this->lampiran;
		else
			return Yii::app()->controller->createUrl("surat/cetakPdf",array("id"=>$this->id));
	}

	public function sendSurat()
	{	
		$tanda = Tanda::getTanda('Distribusi');

		$distribusi = new Distribusi;
		$distribusi->id_surat = $this->id;
		$distribusi->tanda = $tanda;
		$distribusi->id_jabatan_pengirim = Pegawai::getIdJabatanByUserId();
		$distribusi->username_pengirim = Yii::app()->user->id;
		$distribusi->setPengirim();

		if($this->tujuan!='')
		{
			
			$list_tujuan = explode(';',$this->tujuan);

			foreach($list_tujuan as $penerima) 
			{
				$distribusi->id_distribusi_jenis = 1;

				$jabatan = Jabatan::findByNama($penerima);
				
				if($jabatan!==null)
				{
					$jabatan->createDistribusi($distribusi);
				}

				$pegawai = Pegawai::findByNama($penerima);
				if($pegawai!==null)
				{				
					$pegawai->createDistribusi($distribusi);
				}

				if($penerima=='Semua Pegawai') 
				{
					$criteria = new CDbCriteria;
					$criteria->addCondition("username <> ''");

					foreach(Pegawai::model()->findAll($criteria) as $pegawai)
					{
						$pegawai->createDistribusi($distribusi);
					}
				}

				$grup = DistribusiGrup::model()->findByAttributes(array('nama'=>$penerima));

				if($grup !== null)
				{
					foreach($grup->findAllDistribusiGrupDetail() as $data)
					{
						if($data->model=="Jabatan")
						{
							$jabatan = Jabatan::model()->findByPk($data->id_model);
							$jabatan->createDistribusi($distribusi);
						}

						if($data->model=="Pegawai")
						{
							$pegawai = Pegawai::model()->findByPk($data->id_model);
							$pegawai->createDistribusi($distribusi);
						}
					}
				}
			}
		}

		if($this->tembusan!='')
		{
			$list_tembusan = explode(';',$this->tembusan);

			foreach($list_tembusan as $penerima) 
			{
				$distribusi->id_distribusi_jenis = 2;

				$jabatan = Jabatan::findByNama($penerima);
				if($jabatan!==null)
					$jabatan->createDistribusi($distribusi);

				$pegawai = Pegawai::findByNama($penerima);
				if($pegawai!==null)
					$pegawai->createDistribusi($distribusi);

				if($penerima=='Semua Pegawai') 
				{
					$criteria = new CDbCriteria;
					$criteria->addCondition("username <> ''");
					
					foreach(Pegawai::model()->findAll($criteria) as $pegawai)
					{
						$pegawai->createDistribusi($distribusi);
					}
				}

				$grup = DistribusiGrup::model()->findByAttributes(array('nama'=>$penerima));

				if($grup !== null)
				{
					foreach($grup->findAllDistribusiGrupDetail() as $data)
					{
						if($data->model=="Jabatan")
						{
							$jabatan = Jabatan::model()->findByPk($data->id_model);
							$jabatan->createDistribusi($distribusi);
						}

						if($data->model=="Pegawai")
						{
							$pegawai = Pegawai::model()->findByPk($data->id_model);
							$pegawai->createDistribusi($distribusi);
						}
					}
				}

			}
			
		}

		if($this->tembusan_laporan!='')
		{
			$list_tembusan_laporan = explode(';',$this->tembusan_laporan);

			foreach($list_tembusan_laporan as $penerima) 
			{
				$distribusi->id_distribusi_jenis = 2;

				$jabatan = Jabatan::findByNama($penerima);
				if($jabatan!==null)
					$jabatan->createDistribusi($distribusi);

				$pegawai = Pegawai::findByNama($penerima);
				if($pegawai!==null)
					$pegawai->createDistribusi($distribusi);
			}
			
		}
		
	}

	public function setNomor()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$jenis = $this->id_surat_jenis;

		$criteria->addCondition('id_surat_jenis = :id_surat_jenis');
		$params[':id_surat_jenis'] = $this->id_surat_jenis;

		$criteria->addCondition('id_jabatan_penandatangan = :id_jabatan_penandatangan');
		$params[':id_jabatan_penandatangan'] = $this->id_jabatan_penandatangan;

		$criteria->addCondition('tanggal >= :awal AND tanggal <= :akhir');
		date_default_timezone_set('Asia/Jakarta');
		$params[':awal']=date('Y').'-01-01';
		$params[':akhir']=date('Y').'-12-31';

		$criteria->order = 'urutan DESC';

		$criteria->params = $params;

		$model = Surat::model()->find($criteria);

		if($model!==null) {
			$urutan = $model->urutan+1;
		} else {
			$urutan = 1;
		}

		while(strlen($urutan)<4)
		{
			$urutan = '0'.$urutan;
		}

		$bulan = date('n');
		$romawi = '';

		if($bulan==1) $romawi = 'I';
		if($bulan==2) $romawi = 'II';
		if($bulan==3) $romawi = 'III';
		if($bulan==4) $romawi = 'IV';
		if($bulan==5) $romawi = 'V';
		if($bulan==6) $romawi = 'VI';
		if($bulan==7) $romawi = 'VII';
		if($bulan==8) $romawi = 'VIII';
		if($bulan==9) $romawi = 'IX';
		if($bulan==10) $romawi = 'X';
		if($bulan==11) $romawi = 'XI';
		if($bulan==12) $romawi = 'XII';

		$jabatan_penandatangan = $this->findJabatanPenandatangan();
		
		if($jabatan_penandatangan->id_jabatan_eselon == 1)
		{
			$belakang = "/".$this->getJabatanPenandatanganSingkat()."/Bakamla/".$romawi."/".date('Y');
		} else {
			$belakang = "/".$this->getJabatanPenandatanganSingkat()."/".$this->getJabatanEselonISingkat()."/Bakamla/".$romawi."/".date('Y');
		}

		if($jenis==1) $depan = "Skep-";
		if($jenis==2) $depan = "Sprin-";
		if($jenis==3) $depan = "ST-";
		if($jenis==4) $depan = "SD-";
		if($jenis==5) $depan = "SPLN-";
		if($jenis==6) $depan = "SE-";
		if($jenis==7) $depan = "SI-";
		if($jenis==8) $depan = "Sket-";
		if($jenis==9) $depan = "SKJ-";
		if($jenis==10) $depan = "SK-";
		if($jenis==11) $depan = "Sper-";
		if($jenis==12) $depan = "SC-";
		if($jenis==13) $depan = "Un-";
		if($jenis==14) $depan = "Skdt-";
		if($jenis==15) $depan = "SKPSW-";
		if($jenis==16) $depan = "SKTM-";
		if($jenis==17) $depan = "M-";
		if($jenis==18) $depan = "AGNO-";
		if($jenis==19) $depan = "M-";
		if($jenis==20) $depan = "M-";

		$this->nomor =  $depan.$urutan.$belakang;
	}

	public function setUrutan()
	{
		$nomor = explode("/",$this->nomor);
		$depan = explode("-",$nomor[0]);
		$urutan = $depan[1];
		$urutan = ltrim($urutan);
		
		$this->urutan = $urutan;

	}

	public function findJabatanPenandatangan()
	{
		$model =  Jabatan::model()->findByPk($this->id_jabatan_penandatangan);

		if($model!==null)
			return $model;
		else
			return null;
	}

	public function getJabatanPenandatanganSingkat()
	{
		$model = Jabatan::model()->findByPk($this->id_jabatan_penandatangan);
		if($model!==null)
		{
			return $model->getSingkatan();
		} else {
			return null;
		}

	}

	public function getJabatanEselonISingkat()
	{
		$model = Jabatan::model()->findByPk($this->id_jabatan_penandatangan);
		if($model!==null)
		{
			$jabatan = $model->findEselon(1);
			if($jabatan!==null)
				return $jabatan->getSingkatan();
			else
				return null;
		} else {
			return null;
		}

	}

	public static function isViewPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPenerbit())
			return true;

		if(User::isPegawai())
		{
			if(!empty($_GET['id'])) 
				$id = $_GET['id'];

			$pembuat = Surat::isPembuatPermitted($id);

			$distribusi = Surat::isDistribusiPermitted($id);

			$paraf = Surat::isParafPermitted($id);

			$penerbit = Surat::isPublishPermitted($id);
			
			if($pembuat==1 OR $distribusi=1 OR $paraf==1 OR $penerbit==1)
				return true;
			else
				return false;
		}
	}

	public static function isPembuatPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;
			
		if(User::isPegawai())
		{
			$criteria = new CDbCriteria;

			$criteria->addCondition('id = :id');
			$params[':id']=$id;

			$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username OR id_jabatan_pengaju = :id_jabatan OR username_pengaju = :username');
		
			$params[':username'] = Yii::app()->user->id;
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

			$criteria->params = $params;

			$jumlah = Surat::model()->count($criteria);

			if($jumlah==1)
				return true;
			else
				return false;
		}	
	}

	public static function isPublishPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			if(!empty($_GET['id']))
				$id = $_GET['id'];

			$jumlah = Pegawai::countSuratSiapTerbit($id);

			if($jumlah==1)
				return true;
			else
				return false;
		}
	}

	public static function isParafPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			if(!empty($_GET['id']))
				$id = $_GET['id'];

			$criteria_paraf = new CDbCriteria;
			$params_paraf = array();
			
			$criteria_paraf->addCondition('id_surat = :id_surat');
			$params_paraf[':id_surat']=$id;

			$criteria_paraf->addCondition('id_paraf_status = :id_paraf_status');
			$params_paraf[':id_paraf_status']=3;

			$criteria_paraf->addCondition('id_jabatan = :id_jabatan  OR username_pemaraf = :username');
			$params_paraf[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params_paraf[':username'] = Yii::app()->user->id;

			$criteria_paraf->params = $params_paraf;
			$jumlah_paraf = Paraf::model()->count($criteria_paraf);

			if($jumlah_paraf>0)
				return true;
			else
				return false;
		}
	}

	public static function isDistribusiPermitted($id=null)
	{
		if(empty($_GET['id']))
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			$criteria_internal = new CDbCriteria;

			$criteria_internal->addCondition('id = :id');
			$params_internal[':id']=$_GET['id'];

			$criteria_internal->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username OR id_jabatan_pengaju = :id_jabatan OR username_pengaju = :username');
		
			$params_internal[':username'] = Yii::app()->user->id;
			$params_internal[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

			$criteria_internal->params = $params_internal;

			$jumlah_internal = Surat::model()->count($criteria_internal);

			$criteria_distribusi = new CDbCriteria;
			$criteria_distribusi->addCondition('id_surat = :id_surat');
			$criteria_distribusi->addCondition('id_jabatan_penerima = :id_jabatan OR username_penerima = :username');

			$params_distribusi[':id_surat']=$_GET['id'];
			$params_distribusi[':username'] = Yii::app()->user->id;
			$params_distribusi[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

			$criteria_distribusi->params = $params_distribusi;

			$jumlah_distribusi = Distribusi::model()->count($criteria_distribusi);

			$criteria_paraf = new CDbCriteria;
			$params_paraf = array();
			
			$criteria_paraf->addCondition('id_surat = :id_surat');
			$params_paraf[':id_surat']=$_GET['id'];

			$criteria_paraf->addCondition('id_paraf_status = :id_paraf_status');
			$params_paraf[':id_paraf_status']=3;

			$criteria_paraf->addCondition('id_jabatan = :id_jabatan  OR username_pemaraf = :username');
			$params_paraf[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params_paraf[':username'] = Yii::app()->user->id;

			$criteria_paraf->params = $params_paraf;
			$jumlah_paraf = Paraf::model()->count($criteria_paraf);

			if($jumlah_internal>0 OR $jumlah_distribusi>0)
				return true;
			else
				return false;
		}
	}

	public static function isDirectDeletePermitted()
	{
		if(empty($_GET['id']))
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			$criteria = new CDbCriteria;
			$params = array();

			$criteria->addCondition('id = :id');
			$params[':id']=$_GET['id'];

			$criteria->addCondition('id_surat_status = :id_surat_status');
			$params[':id_surat_status']=2;

			$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username OR id_jabatan_pengaju = :id_jabatan OR username_pengaju = :username');
		
			$params[':username'] = Yii::app()->user->id;
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

			$criteria->params = $params;

			$jumlah = Surat::model()->count($criteria);
			if($jumlah==1)	
				return true;
			else
				return false;
		}
				
	}

	public function countDistribusi($id_distribusi_jenis=null)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_surat = :id_surat');
		$params[':id_surat'] = $this->id;

		if($id_distribusi_jenis!=null)
		{
			$criteria->addCondition('id_distribusi_jenis = :id_distribusi_jenis');
			$params[':id_distribusi_jenis'] = $id_distribusi_jenis;
		}

		$criteria->params = $params;

		return Distribusi::model()->count($criteria);
	}

	//grafik

	public function getDataGrafikSuratPerBulan()
	{
		$grafik = '';

		for($i=1;$i<=12;$i++)
		{
			$bulan = $i;
			
			if($bulan<10) 
				$bulan = '0'.$bulan;

			$criteria = new CDbCriteria;
			$criteria->condition = 'waktu_diterbitkan >= :awal AND waktu_diterbitkan <= :akhir AND waktu_diterbitkan IS NOT NULL';
			$criteria->params = array(
				':awal'=>date('Y').'-'.$bulan.'-01',
				':akhir'=>date('Y').'-'.$bulan.'-31',
			);

			$jumlah = Surat::model()->count($criteria);

			$grafik .= '{"label":"'.$bulan.'","value":"'.$jumlah.'"},';
		}

		return $grafik;

	}	

	public static function getDataGrafikSuratPerJenis()
	{
		$grafik = '';

		foreach(SuratJenis::model()->findall() as $data)
		{

			$criteria = new CDbCriteria;
			$criteria->condition = 'id_surat_status = :status AND id_surat_jenis = :jenis';
			$criteria->params = array(
				':status'=>1,
				':jenis'=>$data->id,
			);

			$jumlah = Surat::model()->count($criteria);

			$grafik .= '{"label":"'.$data->nama.'","value":"'.$jumlah.'"},';
		}

		return $grafik;

	}

	public static function countTerbitHariIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d').' 00:00:00';
		$akhir = date('Y-m-d').' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_diterbitkan >= :awal AND waktu_diterbitkan <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countTerbitBulanIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m').'-01 00:00:00';
		$akhir = date('Y-m').'-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_diterbitkan >= :awal AND waktu_diterbitkan <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countTerbitTahunIni()
	{
		date_default_timezone_set('Asia/Jakarta');

		$awal = date('Y').'-01-01 00:00:00';
		$akhir = date('Y').'-12-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_diterbitkan >= :awal AND waktu_diterbitkan <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countTerbitMingguIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d', strtotime('monday this week')).' 00:00:00';
		$akhir = date('Y-m-d', strtotime('sunday this week')) .' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_diterbitkan >= :awal AND waktu_diterbitkan <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	protected function beforeDelete()
	{
  		Distribusi::model()->deleteAllByAttributes(array('id_surat'=>$this->id));
		Paraf::model()->deleteAllByAttributes(array('id_surat'=>$this->id));
		SuratAtribut::model()->deleteAllByAttributes(array('id_surat'=>$this->id));  		
  		
  		return parent::beforeDelete();
	}

	public function captureBerkas()
	{
		$sumber = Yii::app()->controller->createAbsoluteUrl("surat/cetakSuratUndangan",array("id"=>42));
		
		$path = Yii::app()->basePath.'/../uploads/surat/berkas/';
		$filename = time().'_'.$this->urutan.$this->id_jabatan_penandatangan.'.pdf';

 		$target = $path.$filename;

 		/*
 		$opts = array('http' => array('method'=>'GET', 'header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
		$context = stream_context_create($opts);
		$data = file_get_contents($sumber,false,$context);
		
 		$file = fopen($target, "w+");
 		fwrite($file, $data);
 		fclose($file);
 		*/
 		print $sumber;
 		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL,$sumber);
		$useragent = $_SERVER['HTTP_USER_AGENT'];
 		$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
		//curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		//curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
		//curl_setopt($ch,CURLOPT_USERAGENT, $useragent);
		//curl_setopt( $ch, CURLOPT_COOKIE, $strCookie );
		$data = curl_exec($ch);
		curl_close($ch);
		print "<br>";
		print $data;

		$file = fopen($target, "w+");
 		fwrite($file, $data);
 		fclose($file);
	}

}
