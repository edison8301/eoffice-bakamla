<?php

/**
 * This is the model class for table "paraf".
 *
 * The followings are the available columns in table 'paraf':
 * @property integer $id
 * @property integer $id_surat
 * @property integer $urutan
 * @property integer $id_jabatan
 * @property integer $id_paraf_status
 * @property string $waktu_dilihat
 * @property string $waktu_disetujui
 */
class Paraf extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paraf';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_surat, urutan, urutan_tampil, id_jabatan, id_paraf_status', 'numerical', 'integerOnly'=>true),
			array('waktu_dilihat, waktu_disetujui', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_surat, urutan, id_jabatan, id_paraf_status, waktu_dilihat, waktu_disetujui', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paraf_status'=>array(self::BELONGS_TO,'ParafStatus','id_paraf_status'),
			'surat'=>array(self::BELONGS_TO,'Surat','id_surat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_surat' => 'Id Surat',
			'urutan' => 'Urutan',
			'id_jabatan' => 'Id Jabatan',
			'id_paraf_status' => 'Id Paraf Status',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_disetujui' => 'Waktu Disetujui',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_surat',$this->id_surat);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('id_jabatan',$this->id_jabatan);
		$criteria->compare('id_paraf_status',$this->id_paraf_status);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_disetujui',$this->waktu_disetujui,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function pegawai()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_paraf_status = 3');
		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;
		
		$criteria->with = 	array('surat');
		$criteria->addCondition('surat.id IS NOT NULL');	
		$criteria->order = 'surat.waktu_diajukan DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*
	public function paraf()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_surat',$this->id_surat);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('id_jabatan',$this->id_jabatan);
		$criteria->compare('id_paraf_status',$this->id_paraf_status);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_disetujui',$this->waktu_disetujui,true);

		$criteria->compare('id_jabatan',Pegawai::getIdJabatanByUserId());
		$criteria->compare('id_paraf_status',3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	*/

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paraf the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function countKomentar()
	{
		$model = ParafKomentar::model()->countByAttributes(array('id_paraf'=>$this->id));
		return $model;
	}
	public function findAllCatatan()
	{
		$model = Catatan::model()->findAllByAttributes(array('id_model'=>$this->id,'model'=>'Paraf'));
		return $model;
	}

	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function findSurat()
	{
		$model = Surat::model()->findByAttributes(array('id'=>$this->id_surat));

		if($model!==null)
			return $model;
		else
			return null;

	}

	public function getNamaJabatan()
	{
		$model = Jabatan::model()->findByPk($this->id_jabatan);
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getNamaPemangkuJabatan()
	{
		return null;
	}

	public function updateParaf()
	{
		if($this->id_paraf_status == 1)
		{
			if($this->waktu_disetujui==null)
			{
				date_default_timezone_set('Asia/Jakarta');
				$this->waktu_disetujui = date('Y-m-d H:i:s');
				$this->save();
			}

			$this->createPemberitahuanPengajuParafDisetujui();

			$surat = $this->findSurat();

			if($surat!==null)
				$surat->updateStatus();
		}

		if($this->id_paraf_status == 4)
		{
			$surat = $this->findSurat();
			
			if($surat!==null)
			{
				$surat->id_surat_status = 4;
				$surat->save();
				$surat->updateStatus();

			}

			$this->createPemberitahuanPengajuParafDiperbaiki();
		}

		return true; 
	}

	public function setUrutan()
	{
		$paraf = Paraf::model()->findByAttributes(array('id_surat'=>$this->id_surat),array('order'=>'urutan DESC'));

		if($paraf==null)
		{
			$this->urutan = 1;
		} else {
			$this->urutan = $paraf->urutan+1;
		}

		return true;
	}

	public function countByUserId()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_paraf_status = 3');
		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;
		
		$criteria->with = array('surat');
		$criteria->addCondition('surat.id IS NOT NULL');

		$jumlah = Paraf::model()->count($criteria);

		return $jumlah;

	}

	public function createPemberitahuanPemaraf()
	{

		$model = new Pemberitahuan;
		$model->isi = "Anda memiliki surat yang harus direview";
		
		$surat = $this->findSurat();
		
		if($surat!==null)
		{
			$model->isi = "<b>".$surat->getPengaju()."</b> mengajukan <b>".$surat->getRelation("surat_jenis","nama")."</b>. ";
			$model->id_pemberitahuan_sifat = $surat->id_surat_sifat;
		}

		$model->url = Yii::app()->controller->createUrl("surat/paraf",array("id"=>$this->id_surat));
		$model->id_jabatan = $this->id_jabatan;
		$model->username = $this->username_pemaraf;
		

		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dibuat = date('Y-m-d H:i:s');
		$model->save();
	}

	public function getPemaraf()
	{
		$output = Jabatan::getNamaById($this->id_jabatan);
		$output .= ' ('. Pegawai::getNamaByUsername($this->username_pemaraf).')';

		return $output;
	}

	public function createPemberitahuanPengajuParafDisetujui()
	{

		$model = new Pemberitahuan;
		$model->isi = "";
		
		$surat = $this->findSurat();
		
		if($surat!==null)
		{
			$model->isi = "Selamat, <b>".$this->getPemaraf()."</b> menyetujui pengajuan<b>".$surat->getRelation("surat_jenis","nama")."</b> Anda.";
			$model->url = Yii::app()->controller->createUrl("surat/view",array("id"=>$this->id_surat));
			$model->id_jabatan = $surat->id_jabatan_pembuat;
			$model->username = $surat->username_pembuat;

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');
			$model->save();
		}

		
	}

	public function createPemberitahuanPengajuParafDiperbaiki()
	{

		$model = new Pemberitahuan;
		$model->isi = "";
		
		$surat = $this->findSurat();
		
		if($surat!==null)
		{
			$model->isi = "<b>".$this->getPemaraf()."</b> meminta Anda untuk memperbaiki <b>".$surat->getRelation("surat_jenis","nama")."</b>. ";
		
			$model->url = Yii::app()->controller->createUrl("surat/view",array("id"=>$this->id_surat));
			$model->id_jabatan = $surat->id_jabatan_pembuat;
			$model->username = $surat->username_pembuat;
			$model->id_pemberitahuan_sifat = $surat->id_surat_sifat;

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');
			$model->save();
		}

		
	}

	protected function afterSave()
	{
		return true;
	}

	public static function isSetStatusPermitted()
	{
		if(empty($_GET['id']))
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			$criteria = new CDbCriteria;
			$params = array();

			$criteria->addCondition('id = :id');
			$params[':id']=$_GET['id'];

			$criteria->addCondition('id_paraf_status = :id_paraf_status');
			$params[':id_paraf_status']=3;

			$criteria->addCondition('id_jabatan = :id_jabatan  OR username_pemaraf = :username');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;

			$criteria->params = $params;
			$jumlah = Paraf::model()->count($criteria);

			if($jumlah == 1)
				return true;
			else
				return false;
		}
	}

	public static function isLewatiPermitted()
	{
		return true;
	}

	public static function isHapusPermitted()
	{
		return true;
	}
}
