<?php

/**
 * This is the model class for table "memo".
 *
 * The followings are the available columns in table 'memo':
 * @property integer $id
 * @property integer $isi
 * @property integer $id_jabatan_pembuat
 * @property string $username_pembuat
 * @property string $waktu_dibuat
 */
class Memo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'memo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isi, perihal', 'required'),
			array('id_jabatan_pembuat', 'numerical', 'integerOnly'=>true),
			array('perihal, username_pembuat', 'length', 'max'=>255),
			array('isi','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, isi, id_jabatan_pembuat, username_pembuat, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jabatan_pembuat'=>array(self::BELONGS_TO,'Jabatan','id_jabatan_pembuat')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'isi' => 'Isi',
			'id_jabatan_pembuat' => 'Id Jabatan Pembuat',
			'username_pembuat' => 'Username Pembuat',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('isi',$this->isi);
		$criteria->compare('id_jabatan_pembuat',$this->id_jabatan_pembuat);
		$criteria->compare('username_pembuat',$this->username_pembuat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		if(!User::isAdmin())
		{
			$criteria->compare('id_jabatan_pembuat',Pegawai::getIdJabatanByUserId());
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Memo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getNamaPembuat()
	{
		$model = Pegawai::model()->findByAttributes(array('username'=>$this->username_pembuat));
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getNamaJabatanPembuat()
	{
		$model = Jabatan::model()->findByAttributes(array('id'=>$this->id_jabatan_pembuat));
		if($model!==null)
			return $model->nama_jabatan;
		else
			return null;
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return nulll;
	}

	public function findAllDisposisi()
	{
		$model = Disposisi::model()->findAllByAttributes(array('model'=>'Memo','id_model'=>$this->id));
		if($model!==null)
			return $model;
		else
			return false;
	}

}
