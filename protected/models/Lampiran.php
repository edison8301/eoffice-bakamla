<?php

/**
 * This is the model class for table "lampiran".
 *
 * The followings are the available columns in table 'lampiran':
 * @property integer $id
 * @property integer $id_model
 * @property string $model
 * @property string $berkas
 * @property string $waktu_dibuat
 */
class Lampiran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lampiran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_model, model, berkas, waktu_dibuat', 'required'),
			array('id_model, id_jabatan_pembuat', 'numerical', 'integerOnly'=>true),
			array('model, berkas, username_pembuat', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_model, model, berkas, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_model' => 'Id Model',
			'model' => 'Model',
			'berkas' => 'Berkas',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_model',$this->id_model);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('berkas',$this->berkas,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lampiran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getUrl()
	{
		if($this->model == 'Surat')
			return Yii::app()->baseUrl.'/uploads/surat/lampiran/'.$this->berkas;

		if($this->model == 'Catatan')
			return Yii::app()->baseUrl.'/uploads/catatan/lampiran/'.$this->berkas;
	}

	protected function beforeDelete()
	{
		$path = Yii::app()->basePath.'/../uploads/surat/lampiran/';

		if(file_exists($path.$this->berkas) AND $this->berkas!='')
			unlink($path.$this->berkas);

		return true;
	}

	public function isHapusPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			if(!empty($_GET['id'])) $id = $_GET['id'];

			$criteria = new CDbCriteria;
			$criteria->addCondition('id = :id');
			$params[':id'] = $id;

			$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;

			$criteria->params = $params;

			$jumlah = Lampiran::model()->count($criteria);

			if($jumlah==1)
				return true;
			else
				return false;

		}
	}
}
