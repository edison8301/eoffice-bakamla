<?php

/**
 * This is the model class for table "surat_agno".
 *
 * The followings are the available columns in table 'surat_agno':
 * @property integer $id
 * @property integer $id_surat
 * @property string $nomor
 * @property string $tanggal
 * @property string $asal_surat
 * @property string $nomor_surat
 * @property string $tanggal_surat
 * @property string $tujuan_surat
 * @property string $tembusan_surat
 * @property string $perihal
 * @property string $sehubungan
 * @property string $catatan
 * @property integer $id_jabatan_pembuat
 * @property string $username_pembuat
 * @property string $waktu_dibuat
 */
class SuratAgno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'surat_agno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_surat, id_jabatan_pembuat', 'numerical', 'integerOnly'=>true),
			array('nomor_agenda, asal_surat, nomor_surat, perihal, username_pembuat', 'length', 'max'=>255),
			array('tanggal, tanggal_surat, tujuan_surat, tembusan_surat, sehubungan, catatan, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_surat, nomor, tanggal, asal_surat, nomor_surat, tanggal_surat, tujuan_surat, tembusan_surat, perihal, sehubungan, catatan, id_jabatan_pembuat, username_pembuat, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_surat' => 'Id Surat',
			'nomor' => 'Nomor',
			'tanggal' => 'Tanggal',
			'asal_surat' => 'Asal Surat',
			'nomor_surat' => 'Nomor Surat',
			'tanggal_surat' => 'Tanggal Surat',
			'tujuan_surat' => 'Tujuan Surat',
			'tembusan_surat' => 'Tembusan Surat',
			'perihal' => 'Perihal',
			'sehubungan' => 'Sehubungan',
			'catatan' => 'Catatan',
			'id_jabatan_pembuat' => 'Id Jabatan Pembuat',
			'username_pembuat' => 'Username Pembuat',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_surat',$this->id_surat);
		$criteria->compare('nomor_agenda',$this->nomor_agenda,true);
		$criteria->compare('tanggal_surat',$this->tanggal_surat,true);
		$criteria->compare('asal_surat',$this->asal_surat,true);
		$criteria->compare('nomor_surat',$this->nomor_surat,true);
		$criteria->compare('tanggal_surat',$this->tanggal_surat,true);
		$criteria->compare('tujuan_surat',$this->tujuan_surat,true);
		$criteria->compare('tembusan_surat',$this->tembusan_surat,true);
		$criteria->compare('perihal',$this->perihal,true);
		$criteria->compare('sehubungan',$this->sehubungan,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('id_jabatan_pembuat',$this->id_jabatan_pembuat);
		$criteria->compare('username_pembuat',$this->username_pembuat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SuratAgno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
