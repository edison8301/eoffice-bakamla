<?php

/**
 * This is the model class for table "surat_keluar_jenis".
 *
 * The followings are the available columns in table 'surat_keluar_jenis':
 * @property integer $id
 * @property string $nama
 * @property string $contoh
 */
class SuratJenis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'surat_jenis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('nama, penandatangan, contoh', 'length', 'max'=>255),
			array('keterangan','safe'),	
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, contoh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'contoh' => 'Contoh',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('contoh',$this->contoh,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SuratKeluarJenis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getCountData()
	{
		return SuratKeluar::model()->countByAttributes(array('id_surat_keluar_jenis'=>$this->id));
	}

	public static function getNamaById($id)
	{
		$model = SuratJenis::model()->findByPk($id);
		if($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getListPenandatangan()
	{
		$list = array();

		$daftar = explode(';',$this->penandatangan);

		foreach($daftar as $value)
		{
			if($value=='Kepala')
			{
				$list['1']='Kepala Bakamla';
			}

			if($value=='Sestama')
			{
				$list['2']='Sestama Bakamla';
			}

			if($value=='Deputi')
			{
				$jabatan = Pegawai::findJabatanByUserId();
				$eselon_1 = $jabatan->findEselon(1);

				if($eselon_1->id!=1 AND $eselon_1->id!=2 AND $eselon_1->id!=3)
					$list[$eselon_1->id]=$eselon_1->nama;
			}
		}
	
		return $list;
	}
}
