<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class SetPasswordForm extends CFormModel
{
	public $password;
	public $password_konfirmasi;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('password, password_konfirmasi', 'required','message'=>'{attribute} tidak boleh kosong'),
			array('password_konfirmasi','sama')

			// email has to be a valid email address
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'password'=>'Password',
			'password_konfirmasi'=>'Password (Konfirmasi)',
		);
	}

	public function sama()
	{
		if($this->password!=$this->password_konfirmasi)
		{
			$this->addError('password_baru_konfirmasi','Password (konfirmasi) tidak sesuai');
		}

		return true;
	}
}