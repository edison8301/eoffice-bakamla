<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property integer $id
 */

require_once(Yii::app()->basePath.'/models/Jabatan.php');

class Pegawai extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('id_jabatan, id_jabatan_induk', 'numerical', 'integerOnly'=>true),
			array('id_jabatan','jabatanGanda'),
			array('nip, email, username, password', 'length', 'max'=>255),
			array('login_terkahir','safe')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jabatan'=>array(self::BELONGS_TO,'jabatan','id_jabatan'),
			'jabatan_induk'=>array(self::BELONGS_TO,'jabatan','id_jabatan_induk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'nip' => 'NIP',
			'username' => 'Username',
			'password' => 'Password',
			'struktural' => 'Tipe Jabatan',
			'id_jabatan_induk'=>'Atasan',
			'id_golongan' => 'Id Golongan',
			'tmt_golongan' => 'Tmt Golongan',
			'id_jabatan' => 'Jabatan',
			'id_jabatan_induk' => 'Atasan',


	
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$params = array();

		if(!empty($this->id_jabatan))
		{
			$criteria->addCondition('id_jabatan IN (SELECT id FROM jabatan WHERE nama LIKE :id_jabatan)');
			$params[':id_jabatan'] = '%'.$this->id_jabatan.'%';
		}

		$criteria->params = $params;

		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('username',$this->username,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function jabatanGanda()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->with = array('jabatan');
		$criteria->together = true;

		$criteria->addCondition('jabatan.id_jabatan_jenis = 1');
		
		$criteria->addCondition('id_jabatan = :id_jabatan');
		$criteria->addCondition('username <> :username');
		
		$params[':id_jabatan'] = $this->id_jabatan;
		$params[':username'] = $this->username;

		$criteria->params = $params;
		
		$jumlah = Pegawai::model()->count($criteria);

		if($jumlah!=0)
		{
			$this->addError('id_jabatan','Jabatan sudah ada yang menduduki.');
		}

		return true;

	}

	public function dialog()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($_GET['Pegawai']))
			$this->attributes = $_GET['Pegawai'];
		
		$criteria=new CDbCriteria;
		$params = array();

		if(!empty($this->id_jabatan))
		{
			$criteria->addCondition('id_jabatan IN (SELECT id FROM jabatan WHERE nama LIKE :id_jabatan)');
			$params[':id_jabatan'] = '%'.$this->id_jabatan.'%';
		}

		$criteria->params = $params;

		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('username',$this->username,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
        		'pageSize'=>5,
    		),
		));
	}

	public function dialogStaf()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		if(isset($_GET['Pegawai']))
			$this->attributes = $_GET['Pegawai'];
		
		$criteria=new CDbCriteria;
		$params = array();

		//$criteria->with = array('jabatan');
		//$criteria->together = true;

		$criteria->addCondition('id_jabatan IS NULL');

		//$criteria->params = $params;

		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('username',$this->username,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
        		'pageSize'=>5,
    		),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
		
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
		
	}

	public function hashPassword()
	{
		if(strlen($this->password)!=60 AND $this->password!=null)
			$this->password = CPasswordHelper::hashPassword($this->password);

		return true;
	}

	public static function getListAutoComplete()
	{
		$list = array();

		foreach(Pegawai::model()->findAllByAttributes(array()) as $data) {
			$list[]=$data->nama;
		}

		return $list;
	}

	public static function getIdJabatanByUsername($username)
	{
		$model = Pegawai::model()->findByAttributes(array('username'=>$username));
		if(!empty($model->id_jabatan))
			return $model->id_jabatan;
		else
			return null;
	}

	public static function findByNama($nama)
	{
		$model = Pegawai::model()->findByAttributes(array('nama'=>$nama));
		if($model!==null)
			return $model;
		else
			return null;
	}

	public static function countSuratSiapTerbit($id_surat=null)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_surat_status = 5');

		if($id_surat!=null)
		{
			$criteria->addCondition('id = :id_surat');
			$params[':id_surat'] = $id_surat;
		}

		if(Pegawai::isStruktural()) 
		{
			$criteria->addCondition('id_jabatan_penandatangan = :id_jabatan OR id_jabatan_penandatangan IN (SELECT id_jabatan_penandatangan FROM penerbit WHERE id_jabatan_penerbit = :id_jabatan)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
		} else {
			$criteria->addCondition('username_penandatangan = :username OR id_jabatan_penandatangan IN (SELECT id_jabatan_penandatangan FROM penerbit WHERE username_penerbit = :username)');
			$params[':username'] = Yii::app()->user->id;
		}

		$criteria->params = $params;

		return Surat::model()->count($criteria);
		
	}

	public function createDistribusi(Distribusi $distribusi)
	{
		$model = new Distribusi;
		$model->attributes = $distribusi->attributes;

		$model->id_jabatan_penerima = $this->id_jabatan;
		$model->username_penerima = $this->username;
		
		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dibuat = date('Y-m-d H:i:s');

		$model->save();

		return true;
	}

	public static function getIdJabatanByUserId()
	{
		$username = Yii::app()->user->id;
		$model = Pegawai::model()->findByAttributes(array('username'=>$username));
		if(!empty($model->id_jabatan))
			return $model->id_jabatan;
		else
			return null;
	}

	public static function getIdJabatanIndukByUserId()
	{
		$username = Yii::app()->user->id;
		$model = Pegawai::model()->findByAttributes(array('username'=>$username));
		if(!empty($model->id_jabatan_induk))
			return $model->id_jabatan_induk;
		else
			return null;
	}

	public static function findJabatanIndukByUserId()
	{
		$username = Yii::app()->user->id;
		if(Pegawai::getIdJabatanIndukByUserId()!=null)
		{
			$model = Jabatan::model()->findByPk(Pegawai::getIdJabatanIndukByUserId());
			if($model!==null)
				return $model;
			else
				return null;
		} else {
			return null;
		}

	}

	public static function getNamaByUserId()
	{
		$username = Yii::app()->user->id;
		$model = Pegawai::model()->findByAttributes(array('username'=>$username));
		if(!empty($model->nama))
			return $model->nama;
		else
			return null;
	}

	public static function getNipByUserId()
	{
		$username = Yii::app()->user->id;
		$model = Pegawai::model()->findByAttributes(array('username'=>$username));
		if(!empty($model->nip))
			return $model->nip;
		else
			return null;
	}



	public static function findJabatanByUserId()
	{
		$username = Yii::app()->user->id;
		
		$pegawai = Pegawai::model()->findByAttributes(array('username'=>$username));
		
		if($pegawai!==null)
		{
			$jabatan = Jabatan::model()->findByPk($pegawai->id_jabatan);
			if($jabatan!==null)
				return $jabatan;
			else
				return null;
		} else {
			return null;
		}
		
	}



	public static function getIdJabatanByNama($nama)
	{
		$model = Pegawai::model()->findByAttributes(array('nama'=>$nama));
		if(!empty($model->id_jabatan))
			return $model->id_jabatan;
		else
			return null;
	}

	public function getUsernameByIdJabatan($id_jabatan)
	{
		$model = Pegawai::model()->findByAttributes(array('id_jabatan'=>$id_jabatan));
		if(!empty($model->username))
			return $model->username;
		else
			return null;
	}

	public static function getUsernameByNama($nama)
	{
		$model = Pegawai::model()->findByAttributes(array('nama'=>$nama));
		if(!empty($model->username))
			return $model->username;
		else
			return null;
	}

	public static function getUsernameById($id)
	{
		$model = Pegawai::model()->findByAttributes(array('id'=>$id));
		if(!empty($model->username))
			return $model->username;
		else
			return null;
	}

	public static function getNamaJabatanByNama($nama)
	{
		$id_jabatan = Pegawai::getIdJabatanByNama($nama);
		return Jabatan::getNamaJabatanById($id_jabatan);
	}

	public static function getJabatanByUserId()
	{
		$id_jabatan = Pegawai::getIdJabatanByUserId();
		$jabatan = Jabatan::model()->findByPk($id_jabatan);
		if($jabatan!==null)
			return $jabatan->nama;
		else
			return null;
		//return Jabatan::getNamaById($id_jabatan);
	}

	public static function getNamaByUsername($username)
	{
		if($username!=null)
		{
			$model = Pegawai::model()->findByAttributes(array('username'=>$username));
			if(!empty($model->nama))
				return $model->nama;
			else
				return $username;
		} else
			return null;

	}

	public static function getEmailByUsername($username)
	{
		if($username!=null)
		{
			$model = Pegawai::model()->findByAttributes(array('username'=>$username));
			if(!empty($model->email))
				return $model->email;
			else
				return $username;
		} else
			return null;

	}

	public static function getNamaByIdJabatan($id_jabatan)
	{
		
		$model = Pegawai::model()->findByAttributes(array('id_jabatan'=>$id_jabatan));
			if(!empty($model->nama))
				return $model->nama;
			else
				return null;
	}

	public static function findAllSuratByStatus($status)
	{
		$model = Surat::model()->findAllByAttributes(array('id_jabatan_pembuat'=>Pegawai::getIdJabatanByUserId(),'id_surat_status'=>$status));

		return $model;
	}

	public static function findAllParafByStatus($status,$model)
	{
		$data = Paraf::model()->findAllByAttributes(array('id_jabatan'=>Pegawai::getIdJabatanByUserId(),'id_paraf_status'=>$status,'model'=>$model));

		return $data;
	}

	public static function countDisposisiMemoByUserId()
	{
		$id_jabatan = Pegawai::getIdJabatanByUserId();
		return Disposisi::model()->countByAttributes(array('model'=>'Memo','id_jabatan_penerima'=>$id_jabatan));
	}

	public static function countDisposisiMemoBaruByUserId()
	{
		$id_jabatan = Pegawai::getIdJabatanByUserId();
		$criteria = new CDbCriteria;
		$criteria->condition = 'model = :model AND id_jabatan_penerima = :id_jabatan_penerima AND waktu_dilihat IS NULL';
		$criteria->params = array('model'=>'Memo','id_jabatan_penerima'=>$id_jabatan);

		return Disposisi::model()->count($criteria);
	}

	public static function countDisposisiSuratKeluarBaruByUserId()
	{
		$id_jabatan = Pegawai::getIdJabatanByUserId();
		$criteria = new CDbCriteria;
		$criteria->condition = 'model = :model AND id_jabatan_penerima = :id_jabatan_penerima AND waktu_dilihat IS NULL';
		$criteria->params = array('model'=>'SuratKeluar','id_jabatan_penerima'=>$id_jabatan);

		return Disposisi::model()->count($criteria);
	}

	public static function countDisposisiSuratBaruByUserId()
	{
		$id_jabatan = Pegawai::getIdJabatanByUserId();
		$criteria = new CDbCriteria;
		$criteria->condition = 'model = :model AND id_jabatan_penerima = :id_jabatan_penerima AND waktu_dilihat IS NULL';
		$criteria->params = array('model'=>'Surat','id_jabatan_penerima'=>$id_jabatan);

		return Disposisi::model()->count($criteria);
	}

	public static function countSuratByStatus($status)
	{
		$criteria = new CDbCriteria;
		$params = array();

		if(Pegawai::isStruktural())
		{
			$criteria->addCondition('id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;
		} else {
			$criteria->addCondition('username_pembuat = :username');
			$params[':username'] = Yii::app()->user->id;
		}

		$criteria->addCondition('id_surat_status = :id_surat_status');
		$params[':id_surat_status'] = $status;

		$criteria->params = $params;

		return Surat::model()->count($criteria);
	}

	public static function hasDraf()
	{
		$draf = false;
		
		if(Pegawai::countSuratByStatus(2)>0)
			$draf = true;

		if(Pegawai::countSuratByStatus(3)>0)
			$draf = true;

		if(Pegawai::countSuratByStatus(4)>0)
			$draf = true;

		if(Pegawai::countSuratByStatus(5)>0)
			$draf = true;

		return $draf;
	}

	public static function hasAksi()
	{
		$output = false;
		
		if(Paraf::countByUserId()>0)
			$output = true;

		if(Pegawai::countSuratSiapTerbit()>0)
			$output = true;

		return $output;
	}

	public static function isStruktural()
	{
		$model = Pegawai::findJabatanByUserId();
		if($model!==null)
		{
			if($model->id_jabatan_jenis ==1)
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

	public static function countSuratTerbitBySumber($sumber)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_surat_status = 1');

		if($sumber=="dibuat")
		{
			$criteria->addCondition('(id_jabatan_pembuat = :id_jabatan OR username_pembuat = :username OR id_jabatan_pengaju = :id_jabatan OR username_pengaju = :username)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;
		}

		if($sumber=="tembusan")
		{
			$criteria->addCondition('id IN (SELECT id_surat FROM distribusi WHERE id_jabatan_penerima = :id_jabatan AND id_distribusi_jenis = 1)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
		}

		if($sumber=="disposisi")
		{
			$criteria->addCondition('id IN (SELECT id_surat FROM distribusi WHERE id_jabatan_penerima = :id_jabatan AND id_distribusi_jenis = 2)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
		}

		if($sumber=="terusan")
		{
			$criteria->addCondition('id IN (SELECT id_surat FROM distribusi WHERE id_jabatan_penerima = :id_jabatan AND id_distribusi_jenis = 3)');
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
		}

		$criteria->params = $params;

		return Surat::model()->count($criteria);
	}

	public static function findAllDistribusiByJenis($jenis)
	{
		$criteria = new CDbCriteria;
		$params = array();

		/*
		$criteria->addCondition('username_pembuat = :username_pembuat');
		$params[':username_pembuat'] = Yii::app()->user->id;
		*/
		$criteria->addCondition('id_surat IS NOT NULL');
		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;

		return Distribusi::model()->findAll($criteria);
	}

	public static function countPemberitahuanBaru()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('waktu_dilihat IS NULL');

		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();


		$criteria->params = $params;
		
		return Pemberitahuan::model()->count($criteria);
	}

	public function findAllPemberitahuan($limit)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->limit = $limit;

		$criteria->params = $params;

		$criteria->order = 'waktu_dibuat DESC';
		
		return Pemberitahuan::model()->findAll($criteria);
	}

	public static function findAllPemberitahuanBaru($limit=null)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('waktu_dilihat IS NULL');

		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;

		$criteria->order = 'waktu_dibuat DESC';

		if($limit!=null)
			$criteria->limit = $limit;
		
		return Pemberitahuan::model()->findAll($criteria);
	}

	public static function getMenuPemberitahuanBaru()
	{
		$list = array();
		foreach(Pegawai::findAllPemberitahuanBaru(15) as $data)
		{
			$class = '';
			if($data->id_pemberitahuan_sifat==1) $class='alert-success';
			if($data->id_pemberitahuan_sifat==2) $class='alert-warning';
			if($data->id_pemberitahuan_sifat==3) $class='alert-danger';

			$list[] = array('label'=>$data->isi,'url'=>array('pemberitahuan/view','id'=>$data->id),'linkOptions'=>array('class'=>$class));
		}
		$list[] = '---';
		$list[] = array('label'=>'Lihat semua pemberitahuan','url'=>array('pegawai/pemberitahuan'));

		return $list;
	}

	public static function countDistribusiBaruByJenis($jenis)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('waktu_dilihat IS NULL');

		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->addCondition('id_distribusi_jenis = :jenis');
		$params[':jenis'] = $jenis;

		$criteria->params = $params;
		
		return Distribusi::model()->count($criteria);
	}

	public function countDistribusiByJenis($jenis)
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->addCondition('id_distribusi_jenis = :jenis');
		$params[':jenis'] = $jenis;

		$criteria->params = $params;
		
		return Distribusi::model()->count($criteria);
	}

	public function getDataGrafikSuratPerBulan()
	{
		$grafik = '';

		for($i=1;$i<=12;$i++)
		{
			$bulan = $i;
			
			if($bulan<10) 
				$bulan = '0'.$bulan;

			$criteria = new CDbCriteria;
			$criteria->condition = 'waktu_dibuat >= :awal AND waktu_dibuat <= :akhir AND waktu_dibuat IS NOT NULL AND (id_jabatan_penerima = :id_jabatan OR username_penerima = :username)';
			$criteria->params = array(
				':awal'=>date('Y').'-'.$bulan.'-01',
				':akhir'=>date('Y').'-'.$bulan.'-31',
				':id_jabatan'=>Pegawai::getIdJabatanByUserId(),
				':username'=>Yii::app()->user->id
			);

			$jumlah = Distribusi::model()->count($criteria);

			$grafik .= '{"label":"'.$bulan.'","value":"'.$jumlah.'"},';
		}

		return $grafik;

	}

	public static function getDataGrafikSuratPerJenis()
	{
		$grafik = '';

		foreach(SuratJenis::model()->findall() as $data)
		{

			$criteria = new CDbCriteria;
			$criteria->condition = 'id_surat_status = :status AND id_surat_jenis = :jenis AND id_jabatan_pembuat = :jabatan OR id_jabatan_pengaju = :jabatan OR id IN (SELECT id_surat FROM distribusi WHERE id_jabatan_penerima = :jabatan)';
			$criteria->params = array(
				':status'=>1,
				':jenis'=>$data->id,
				':jabatan'=>Pegawai::getIdJabatanByUserId(),
			);

			$jumlah = Surat::model()->count($criteria);

			$grafik .= '{"label":"'.$data->nama.'","value":"'.$jumlah.'"},';
		}

		return $grafik;

	}

	public static function findByUserId()
	{
		return Pegawai::model()->findByAttributes(array('username'=>Yii::app()->user->id));
	}

	public static function getListAtasan()
	{
		$list = array();

		$jabatan = Pegawai::findJabatanByUserId();

		if($jabatan===null)
		{
			$jabatan = Jabatan::model()->findByPk(Pegawai::getIdJabatanIndukByUserId());
		} 

		$list['1']='Kepala Bakamla';
		$list['3']='Sekretariat Utama';
		$list[$eselon_1->id]=$eselon_1->nama;
		$list['15']='Biro Umum';

		return $list;
	}

	public function findEselon($eselon)
	{

	}

	public static function getListPenandatangan()
	{
		$list = array();

		$list['1']='Kepala Bakamla';
		$list['2']='Sekretaris Utama';

		$jabatan = Pegawai::findJabatanByUserId();
		$atasan = $jabatan->findAtasan();

		if($atasan!==null)
		{
			$i=1;
			while($i<=$atasan->id_jabatan_eselon)
			{
				$eselon = $atasan->findEselon($i);
				if($eselon!==null)
				{
					$list[$eselon->id]=$eselon->nama;
				}

				$i++;
			}
		}

		if($jabatan->id_jabatan_jenis==1)
			$list[$jabatan->id] = $jabatan->nama;
		else
			$list[$jabatan->id] = Pegawai::getNamaByUserId();

		return $list;
	}

	public static function countLoginHariIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d').' 00:00:00';
		$akhir = date('Y-m-d').' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->count($criteria);
	}

	public static function findAllLoginHariIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d').' 00:00:00';
		$akhir = date('Y-m-d').' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->order = 'login_terakhir DESC';
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->findAll($criteria);
	}

	public static function countLoginMingguIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d', strtotime('monday this week')).' 00:00:00';
		$akhir = date('Y-m-d', strtotime('sunday this week')) .' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->count($criteria);
	}

	public static function findAllLoginMingguIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d', strtotime('monday this week')).' 00:00:00';
		$akhir = date('Y-m-d', strtotime('sunday this week')) .' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->order = 'login_terakhir DESC';
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->findAll($criteria);
	}

	public static function countLoginBulanIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m').'-01 00:00:00';
		$akhir = date('Y-m').'-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->count($criteria);
	}

	public static function findAllLoginBulanIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m').'-01 00:00:00';
		$akhir = date('Y-m').'-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->order = 'login_terakhir DESC';
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);


		return Pegawai::model()->findAll($criteria);
	}

	public static function countLoginTahunIni()
	{
		date_default_timezone_set('Asia/Jakarta');

		$awal = date('Y').'-01-01 00:00:00';
		$akhir = date('Y').'-12-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->count($criteria);
	}

	public static function findAllLoginTahunIni()
	{
		date_default_timezone_set('Asia/Jakarta');

		$awal = date('Y').'-01-01 00:00:00';
		$akhir = date('Y').'-12-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('login_terakhir >= :awal AND login_terakhir <= :akhir');
		$criteria->order = 'login_terakhir DESC';
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Pegawai::model()->findAll($criteria);
	}

	


}
