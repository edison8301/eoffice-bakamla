<?php

/**
* Form Laporan
* Lorem Ipsum DOlor
*/
class LaporanForm extends CFormModel
{
	public $tanggal_awal;
	public $tanggal_akhir;
	public $jenis_surat;
	
	public function rules()
	{
		return array(
			array('tanggal_awal, tanggal_akhir, jenis_surat', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array();
	}
}