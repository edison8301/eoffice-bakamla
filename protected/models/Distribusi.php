<?php

/**
 * This is the model class for table "disposisi".
 *
 * The followings are the available columns in table 'disposisi':
 * @property integer $id
 * @property integer $id_surat
 * @property string $tanggal
 * @property string $no_agenda
 * @property string $perihal
 * @property integer $id_disposisi_asal
 * @property integer $id_disposisi_tujuan
 * @property integer $id_disposisi_sifat
 * @property integer $id_disposisi_tindakan
 * @property string $catatan
 * @property string $dari
 * @property string $ke
 * @property string $waktu_dilihat
 * @property string $waktu_dibuat
 */
class Distribusi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $list_penerima;

	public $pengirim;

	public function tableName()
	{
		return 'distribusi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_surat, id_jabatan_penerima, id_distribusi_jenis, tanda, id_jabatan_pengirim', 'numerical', 'integerOnly'=>true),
			array('username_penerima, pengirim, username_pengirim', 'length', 'max'=>255),
			array('catatan, waktu_dilihat, tindakan, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_surat, tanggal, no_agenda, pengirim, perihal, id_disposisi_asal, id_disposisi_tujuan, id_disposisi_sifat, id_disposisi_tindakan, catatan, dari, ke, waktu_dilihat, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'disposisi_sifat'=>array(self::BELONGS_TO,'DisposisiSifat','id_disposisi_sifat'),
			//'disposisi_tindakan'=>array(self::BELONGS_TO,'DisposisiTindakan','id_disposisi_tindakan'),
			'surat'=>array(self::BELONGS_TO,'Surat','id_surat'),
			'jabatan_pengirim'=>array(self::BELONGS_TO,'Jabatan','id_jabatan_pengirim'),
			'distribusi_jenis'=>array(self::BELONGS_TO,'DistribusiJenis','id_distribusi_jenis')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_surat' => 'Id Surat',
			'tanggal' => 'Tanggal',
			'no_agenda' => 'No Agenda',
			'perihal' => 'Perihal',
			'list_penerima' => 'Penerima Disposisi',
			'list_pegawai_penerima' => 'Nama Pegawai',
			'disposisi_nama'=>'Nama Pegawai',
			'disposisi_jabatan'=>'Jabatan',
			'id_distribusi_jenis'=>'Jenis',
			'id_disposisi_asal' => 'Id Disposisi Asal',
			'id_disposisi_tujuan' => 'Id Disposisi Tujuan',
			'id_disposisi_sifat' => 'Sifat',
			'id_disposisi_tindakan' => 'Tindakan',
			'catatan' => 'Catatan',
			'dari' => 'Dari',
			'ke' => 'Ke',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_dibuat' => 'Waktu Dikirim',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_surat',$this->id_surat);
		$criteria->compare('id_jabatan_pengirim',$this->id_jabatan_pengirim);
		$criteria->compare('id_distribusi_jenis',$this->id_distribusi_jenis);
		$criteria->compare('username_pengirim',$this->username_pengirim);
		$criteria->compare('id_jabatan_penerima',$this->id_jabatan_penerima);
		$criteria->compare('username_penerima',$this->username_penerima);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		$criteria->order = 'waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function pegawai()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$params = array();
		
		if(Pegawai::isStruktural())
		{		
			$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima OR username_penerima = :username');
			$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;
		} else {
			$criteria->addCondition('username_penerima = :username');
			$params[':username'] = Yii::app()->user->id;
		}

		$criteria->params = $params;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_surat',$this->id_surat);
		$criteria->compare('id_distribusi_jenis',$this->id_distribusi_jenis);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);
		
		$criteria->order = 'waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Disposisi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getDisposisiNama()
	{
		if($this->ke!=null)
			return Pegawai::getNamaByUsername($this->ke);
		else
			return null;
	}

	public function getDisposisiEmail()
	{
		if($this->ke!=null)
			return Pegawai::getEmailByUsername($this->ke);
		else
			return null;
	}

	public function getAsalDisposisi()
	{
		return Pegawai::getNamaByUsername($this->dari).' ('.Jabatan::getNamaJabatanById($this->id_disposisi_asal).')';
	}

	public function getTujuanDisposisi()
	{
		return Pegawai::getNamaByUsername($this->ke).' ('.Jabatan::getNamaJabatanById($this->id_disposisi_tujuan).')';
	}

	public function findSurat()
	{
		return Surat::model()->findByAttributes(array('id'=>$this->id_surat));
	}

	public function getTindakan()
	{
		$output = '';
		$tindakan = json_decode($this->id_disposisi_tindakan);
		if(is_array($tindakan))
		{
			foreach($tindakan as $value)
			{
				$output .= DisposisiTindakan::getNamaById($value).', ';
			}
		}
		return $output;
	}

	public function emailCreation()
	{
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$to = str_replace('/',',',$this->getDisposisiEmail());
		// Additional headers
		$headers .= 'To: '.$this->getDisposisiNama().' <'.$this->getDisposisiEmail().'>' . "\r\n";
		$headers .= 'From: Surat Masuk PKP2A I LAN <suratmasuk@bandung.lan.go.id>' . "\r\n";
		//$headers .= 'Cc: p2ipk@lan.go.id, p2ipk.inovasi@gmail.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		$subject = 'Disposisi Surat Masuk #'.$this->no_agenda;
	
		$message = '';
		$message .= 'Kepada Yth. <br>';
		$message .= $this->getDisposisiNama().'<br>';
		$message .= 'di tempat <br>';
		$message .= '<br>';
		$message .= 'Dengan hormat, <br>';
		$message .= '<br>';
		$message .= 'Melalui surat elektronik ini kami informasikan bahwa Saudara/i mendapatkan disposisi surat masuk. <br>';
		$message .= 'Silahkan periksa akun surat masuk Saudara/i. <br>';
		$message .= '<br>';
		$message .= '<br>';
		$message .= 'Keterangan: <br>';
		$message .= 'Email ini dikirim secara otomatis oleh aplikasi Surat Masuk PKP2A I LAN.<br>';

		date_default_timezone_set('Asia/Jakarta');
		$message .= 'Waktu disposisi: '.Helper::getCreatedTime(date('Y-m-d H:i:s'));
		
		mail($to,$subject,$message,$headers);

		return true;
	}

	public function getMemoPerihal()
	{
		$model = Memo::model()->findByAttributes(array('id'=>$this->id_model));
		if($model->perihal!==null)
			return $model->perihal;
		else
			return null;
	}

	public function findMemo()
	{
		return Memo::model()->findByPk($this->id_model);
	}

	protected function afterSave()
	{
		$this->createPemberitahuan();

		return true;	
	}

	public function createPemberitahuan()
	{
		$model = new Pemberitahuan;

		if($this->id_distribusi_jenis==1)
		{
			$model->isi = 'Anda mendapatkan <b>surat</b> baru dari <b>'.$this->getPengirim().'</b>';
			$model->url = Yii::app()->controller->createUrl("distribusi/view",array("id"=>$this->id));
		}

		if($this->id_distribusi_jenis==2)
		{
			$model->isi = 'Anda mendapatkan <b>tembusan</b> surat baru dari <b>'.$this->getPengirim().'</b>';
			$model->url = Yii::app()->controller->createUrl("distribusi/view",array("id"=>$this->id));
		}

		if($this->id_distribusi_jenis==3)
		{
			$model->isi = 'Anda mendapatkan <b>disposisi</b> surat baru dari <b>'.$this->getPengirim().'</b>';
			$model->url = Yii::app()->controller->createUrl("distribusi/view",array("id"=>$this->id));
		}

		if($this->id_distribusi_jenis==4)
		{
			$model->isi = 'Anda mendapatkan <b>terusan</b> surat baru dari <b>'.$this->getPengirim().'</b>';
			$model->url = Yii::app()->controller->createUrl("distribusi/view",array("id"=>$this->id));
		}

		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dibuat = date('Y-m-d H:i:s');
		$model->id_jabatan = $this->id_jabatan_penerima;
		$model->username = $this->username_penerima;

		$model->save();

		return true;
	}

	public function getPengirim()
	{
		if($this->pengirim!=null)
			return $this->pengirim;
		else {
			$this->setPengirim();
			return $this->pengirim; 
		}

		return $output;
	}

	public function setPengirim()
	{
		$output = Jabatan::getNamaById($this->id_jabatan_pengirim);
		$output .= ' ('. Pegawai::getNamaByUsername($this->username_pengirim).')';

		$this->pengirim = $output;
	}

	public function getPenerima()
	{
		$output = Jabatan::getNamaById($this->id_jabatan_penerima);
		$output .= ' ('. Pegawai::getNamaByUsername($this->username_penerima).')';

		return $output;
	}

	public function getJenis()
	{
		return $this->getRelation("distribusi_jenis","nama");
	}

	public function countCatatan()
	{
		return Catatan::model()->countByAttributes(array('model'=>'Distribusi','id_model'=>$this->id));
	}

	public function findAllCatatan()
	{
		return Catatan::model()->findAllByAttributes(array('model'=>'Distribusi','id_model'=>$this->id),array('order'=>'waktu_dibuat ASC'));
	}

	public static function isCreatePermitted()
	{
		if(empty($_GET['id_surat']) OR empty($_GET['id_distribusi_jenis']))
			return false;

		if($_GET['id_distribusi_jenis']!=2 AND $_GET['id_distribusi_jenis']!=3 AND $_GET['id_distribusi_jenis']!=4)
			return false;

		if(Surat::isViewPermitted($_GET['id_surat']))
			return true;
	}

	public static function isViewPermitted($id=null)
	{
		if(empty($_GET['id']) AND $id==null)
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			if(!empty($_GET['id']))
				$id = $_GET['id'];
			
			$criteria = new CDbCriteria;
			$params = array();

			$criteria->condition = '(id_jabatan_penerima = :id_jabatan OR id_jabatan_pengirim = :id_jabatan OR username_pengirim = :username OR username_penerima = :username)';
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;

			$criteria->addCondition('id = :id');
			$params[':id']=$id;

			$criteria->params = $params;

			$jumlah = Distribusi::model()->count($criteria);

			if($jumlah==1)
				return true;
			else
				return false;

		}

	}

	public static function countHariIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d').' 00:00:00';
		$akhir = date('Y-m-d').' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countBulanIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m').'-01 00:00:00';
		$akhir = date('Y-m').'-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countTahunIni()
	{
		date_default_timezone_set('Asia/Jakarta');

		$awal = date('Y').'-01-01 00:00:00';
		$akhir = date('Y').'-12-31 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}

	public static function countMingguIni()
	{
		date_default_timezone_set('Asia/Jakarta');
		$awal = date('Y-m-d', strtotime('monday this week')).' 00:00:00';
		$akhir = date('Y-m-d', strtotime('sunday this week')) .' 23:59:59';

		$criteria = new CDbCriteria;
		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$criteria->params=array(':awal'=>$awal,':akhir'=>$akhir);

		return Surat::model()->count($criteria);
	}
}
