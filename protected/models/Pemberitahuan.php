<?php

/**
 * This is the model class for table "pemberitahuan".
 *
 * The followings are the available columns in table 'pemberitahuan':
 * @property integer $id
 * @property string $isi
 * @property string $tautan
 * @property integer $id_jabatan
 * @property string $username
 * @property string $waktu_dilihat
 * @property string $waktu_dibuat
 */
class Pemberitahuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pemberitahuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jabatan, id_pemberitahuan_sifat', 'numerical', 'integerOnly'=>true),
			array('url, username', 'length', 'max'=>255),
			array('isi, waktu_dilihat, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, isi, tautan, id_jabatan, username, waktu_dilihat, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'isi' => 'Isi',
			'url' => 'URL',
			'id_pemberitahuan_sifat'=>'Sifat',
			'id_jabatan' => 'Id Jabatan',
			'username' => 'Username',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pemberitahuan_sifat',$this->id_pemberitahuan_sifat);
		$criteria->compare('isi',$this->isi,true);
		$criteria->compare('tautan',$this->tautan,true);
		$criteria->compare('id_jabatan',$this->id_jabatan);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pemberitahuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function isViewPermitted()
	{
		if(empty($_GET['id']))
			return false;

		if(User::isAdmin())
			return true;

		if(User::isPegawai())
		{
			$criteria = new CDbCriteria;
			$params = array();

			$criteria->condition = '(id_jabatan = :id_jabatan OR username = :username)';
			$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();
			$params[':username'] = Yii::app()->user->id;

			$criteria->addCondition('id = :id');
			$params[':id'] = $_GET['id'];

			$criteria->params = $params;

			$jumlah = Pemberitahuan::model()->count($criteria);
			
			if($jumlah==1)
				return true;
			else
				return false;
		}
	}
}
