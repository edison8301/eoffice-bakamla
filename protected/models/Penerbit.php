<?php

/**
 * This is the model class for table "penerbit".
 *
 * The followings are the available columns in table 'penerbit':
 * @property integer $id
 * @property integer $id_jabatan_penandatangan
 * @property integer $id_jabatan_penerbit
 * @property string $username_penerbit
 */
class Penerbit extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'penerbit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jabatan_penandatangan','required','message'=>'{attribute} harus diisi'),
			array('id_jabatan_penandatangan, id_jabatan_penerbit', 'numerical', 'integerOnly'=>true),
			array('username_penerbit', 'length', 'max'=>255),
			array('id_jabatan_penerbit,username_penerbit','dobel'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_jabatan_penandatangan, id_jabatan_penerbit, username_penerbit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jabatan_penandatangan'=>array(self::BELONGS_TO,'Jabatan','id_jabatan_penandatangan'),
			'jabatan_penerbit'=>array(self::BELONGS_TO,'Jabatan','id_jabatan_penerbit'),
			'pegawai_penerbit'=>array(self::BELONGS_TO,'Pegawai','username_penerbit'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_jabatan_penandatangan' => 'Jabatan Penandatangan',
			'id_jabatan_penerbit' => 'Jabatan Penerbit',
			'username_penerbit' => 'Pegawai',
		);
	}

	public function dobel()
	{
		if($this->id_jabatan_penerbit!=null AND $this->username_penerbit!=null)
		{
			$this->addError('id_jabatan_penerbit','Jabatan penerbit tidak boleh diisi bersamaan dengan pegawai');
			$this->addError('username_penerbit','Pegawai tidak boleh diisi bersamaan dengan jabatan penerbit');
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_jabatan_penandatangan',$this->id_jabatan_penandatangan);
		$criteria->compare('id_jabatan_penerbit',$this->id_jabatan_penerbit);
		$criteria->compare('username_penerbit',$this->username_penerbit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Penerbit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getPegawaiPenerbit()
	{
		if($this->username_penerbit!=null)
		{
			return Pegawai::getNamaByUsername($this->username_penerbit);
		}
	}
}
