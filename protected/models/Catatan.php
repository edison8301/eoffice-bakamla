<?php

/**
 * This is the model class for table "komentar".
 *
 * The followings are the available columns in table 'komentar':
 * @property integer $id
 * @property string $model
 * @property integer $id_model
 * @property string $komentar
 * @property string $lampiran
 * @property integer $id_jabatan
 * @property string $username
 * @property string $waktu_dilihat
 * @property string $waktu_dibuat
 */
class Catatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_model, catatan, waktu_dibuat', 'required'),
			array('id_model, id_jabatan', 'numerical', 'integerOnly'=>true),
			array('model, lampiran, username', 'length', 'max'=>255),
			array('waktu_dilihat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, model, id_model, komentar, lampiran, id_jabatan, username, waktu_dilihat, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'model' => 'Model',
			'id_model' => 'Id Model',
			'komentar' => 'Komentar',
			'lampiran' => 'Lampiran',
			'id_jabatan' => 'Id Jabatan',
			'username' => 'Username',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('id_model',$this->id_model);
		$criteria->compare('komentar',$this->komentar,true);
		$criteria->compare('lampiran',$this->lampiran,true);
		$criteria->compare('id_jabatan',$this->id_jabatan);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('waktu_dilihat',$this->waktu_dilihat,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Komentar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getPembuat()
	{
		$output = Jabatan::getNamaJabatanById($this->id_jabatan);
		$output .= ' ('.Pegawai::getNamaByUsername($this->username).')';
		return $output;

	}

	public function findAllLampiran()
	{
		return Lampiran::model()->findAllByAttributes(array('id_model'=>$this->id,'model'=>'Catatan'),array('order'=>'waktu_dibuat ASC'));
	}
}

