<?php

class ParafController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('setStatus','setujui','perbaiki'),
				'expression'=>'Paraf::isSetStatusPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('lewati','updateEditable','hapus'),
				'expression'=>'Paraf::isLewatiPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('hapus'),
				'expression'=>'Paraf::isHapusPermitted()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'User::isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$surat = $model->findSurat();

		$paraf_komentar = new ParafKomentar;
		$paraf_komentar->id_paraf = $model->id;
		$paraf_komentar->pembuat = Yii::app()->user->id;

		if(isset($_POST['ParafKomentar']))
		{
			$paraf_komentar->attributes = $_POST['ParafKomentar'];

			date_default_timezone_set('Asia/Jakarta');
			$paraf_komentar->waktu_dibuat = date('Y-m-d H:i:s');
			
			if($paraf_komentar->save())
				$this->redirect(array('paraf/view','id'=>$id));

		}

		$this->render('view',array(
			'model'=>$model,
			'surat'=>$surat,
			'paraf_komentar'=>$paraf_komentar

		));
	}

	public function actionUpdateEditable()
	{
		Yii::import('booster.components.TbEditableSaver');
		
		$es = new TbEditableSaver('Paraf'); 
		$es->update();
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Paraf;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_GET['id_surat'])) $model->id_surat = $_GET['id_surat'];

		if(isset($_POST['Paraf']))
		{
			$model->attributes=$_POST['Paraf'];
			$model->id_paraf_status = 2;

			if($model->save())
			{
				$this->redirect(array('suratKeluar/view','id'=>$model->id_surat));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paraf']))
		{
			$model->attributes=$_POST['Paraf'];
			if($model->save())
				$this->redirect(array('surat/view','id'=>$model->id_surat));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionSetStatus($id,$status)
	{
		$model = $this->loadModel($id);
		$model->id_paraf_status = $status;

		if($model->save())
		{
			if($status==1)
				Yii::app()->user->setFlash('success','Draf berhasil dikonfirmasi');

			$model->updateParaf();
			
			$this->redirect(array('pegawai/paraf'));
		}
	}

	public function actionSetujui($id)
	{
		$this->redirect(array('paraf/setStatus','id'=>$id,'status'=>1));
	}

	public function actionPerbaiki($id)
	{
		$this->redirect(array('paraf/setStatus','id'=>$id,'status'=>4));
	}

	public function actionLewati($id)
	{
		$model = $this->loadModel($id);
		$model->id_paraf_status = 5;

		if($model->save())
		{
			$this->redirect(array('surat/view','id'=>$model->id_surat));
		}
	}

	public function actionHapus($id)
	{
		$model = $this->loadModel($id);
		$id_surat = $model->id_surat;

		if($model->delete())
		{
			$this->redirect(array('surat/view','id'=>$id_surat));
		}
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Paraf');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Paraf('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Paraf']))
$model->attributes=$_GET['Paraf'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Paraf::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='paraf-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
