<?php

class PegawaiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/column1';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','paraf','draf','pemberitahuan','gantiPassword','distribusi',
					'tembusan','terusan','disposisi','profil','suntingProfil','surat','suratDisetujui'
				),
				'expression'=>'User::isPegawai()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','delete','create','update','view','import','setPassword',
					'log'
				),
				'expression'=>'User::isAdmin()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('siapTerbit'),
				'expression'=>'User::isPegawai()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionLog($id)
	{
		$model = $this->loadModel($id); 
		$log=new Log('search');
		$log->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Log']))
			$log->attributes=$_GET['Log'];

		$this->render('log',array(
			'model'=>$model,
			'log'=>$log
		));
	}

	public function actionProfil()
	{
		$model = Pegawai::findByUserId();

		$this->render('profil',array(
			'model'=>$model,
		));
	}

	public function actionGantiPassword()
	{
		$model = Pegawai::findByUserId();

		$gantiPasswordForm = new GantiPasswordForm;

		if(isset($_POST['GantiPasswordForm']))
		{
			$gantiPasswordForm->attributes = $_POST['GantiPasswordForm'];

			if($gantiPasswordForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($gantiPasswordForm->password_baru);
				$model->save();
				Yii::app()->user->setFlash('success','Password pegawai berhasil diperbarui');
				$this->redirect(array('pegawai/gantiPassword'));
			}
		}

		$this->render('gantiPassword',array(
			'model'=>$model,
			'gantiPasswordForm'=>$gantiPasswordForm
		));
	}

	public function actionSetPassword($id)
	{
		$model = $this->loadModel($id);

		$setPasswordForm = new SetPasswordForm;

		if(isset($_POST['SetPasswordForm']))
		{
			$setPasswordForm->attributes = $_POST['SetPasswordForm'];

			if($setPasswordForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($setPasswordForm->password);
				$model->save();
				Yii::app()->user->setFlash('success','Password pegawai berhasil diperbarui');
				$this->redirect(array('pegawai/admin'));
			}
		}

		$this->render('setPassword',array(
			'model'=>$model,
			'setPasswordForm'=>$setPasswordForm
		));
	}

	public function actionPemberitahuan()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_jabatan = :id_jabatan');
		$params[':id_jabatan'] = Pegawai::getIdJabatanByUserId();

		$criteria->params = $params;

		$criteria->order = 'waktu_dibuat DESC';

		$dataProvider=new CActiveDataProvider('Pemberitahuan',array(
			'criteria'=>$criteria
		));
		
		$this->render('pemberitahuan',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionDistribusi($id_distribusi_jenis)
	{
		$model=new Distribusi('pegawai');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Distribusi']))
			$model->attributes=$_GET['Distribusi'];

		if(isset($_GET['id_distribusi_jenis']))
			$model->id_distribusi_jenis = $_GET['id_distribusi_jenis'];

		$this->render('distribusi',array(
			'id_distribusi_jenis'=>$id_distribusi_jenis,
			'model'=>$model,
		));
	}

	public function actionTembusan()
	{
		$criteria = new CDbCriteria;

		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->addCondition('id_distribusi_jenis=1');

		$criteria->params = $params;
		
		$criteria->order = 'waktu_dibuat DESC';

		$dataProvider=new CActiveDataProvider('Distribusi',array(
			'criteria'=>$criteria
		));
		
		$this->render('tembusan',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionTerusan()
	{
		$criteria = new CDbCriteria;

		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->addCondition('id_distribusi_jenis=3');

		$criteria->params = $params;
		
		$criteria->order = 'waktu_dibuat DESC';

		$dataProvider=new CActiveDataProvider('Distribusi',array(
			'criteria'=>$criteria
		));
		
		$this->render('terusan',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionDisposisi()
	{
		$criteria = new CDbCriteria;

		$criteria->addCondition('id_jabatan_penerima = :id_jabatan_penerima');
		$params[':id_jabatan_penerima'] = Pegawai::getIdJabatanByUserId();

		$criteria->addCondition('id_distribusi_jenis=2');

		$criteria->params = $params;
		
		$criteria->order = 'waktu_dibuat DESC';

		$model=new Distribusi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Distribusi']))
			$model->attributes=$_GET['Distribusi'];

		$this->render('disposisi',array(
			'model'=>$model,
		));
	}

	public function actionMemo()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'waktu_dibuat DESC';
		
		$criteria->condition = 'model = :model AND id_jabatan_penerima = :id_jabatan_penerima';
		$criteria->params = array(':model'=>'Memo','id_jabatan_penerima'=>Pegawai::getIdJabatanByUserId());
		
		$dataProvider=new CActiveDataProvider('Disposisi',array(
			'criteria'=>$criteria
		));

		$this->render('memo',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionSurat()
	{
		if(empty($_GET['sumber']))
			$this->redirect(array('pegawai/surat','sumber'=>'dibuat'));

		$model=new Surat('terbit');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Surat']))
			$model->attributes=$_GET['Surat'];

		$this->render('surat',array(
			'model'=>$model,
		));
	}

	public function actionSiapTerbit()
	{
		$model=new Surat('siap');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Surat']))
			$model->attributes=$_GET['Surat'];

		$this->render('siapTerbit',array(
			'model'=>$model,
		));
	}

	public function actionSuratKeluar()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'waktu_dibuat DESC';
		
		$criteria->condition = 'model = :model AND id_jabatan_penerima = :id_jabatan_penerima';
		$criteria->params = array(':model'=>'SuratKeluar','id_jabatan_penerima'=>Pegawai::getIdJabatanByUserId());
		
		$dataProvider=new CActiveDataProvider('Disposisi',array(
			'criteria'=>$criteria
		));

		$this->render('suratKeluar',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionParaf()
	{
		
		$model=new Paraf('pegawai');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Paraf']))
			$model->attributes=$_GET['Paraf'];

		$this->render('paraf',array(
			'model'=>$model,
		));
	}

	public function actionDraf()
	{
		if(empty($_GET['status']))
			$this->redirect(array('pegawai/draf','status'=>'konsep'));

		if($_GET['status']=='konsep') $id_surat_status = 2;
		elseif($_GET['status']=='pemeriksaan') $id_surat_status = 3;
		elseif($_GET['status']=='perbaikan') $id_surat_status = 4;
		elseif($_GET['status']=='disetujui') $id_surat_status = 5;
		elseif($_GET['status']=='terbit') $id_surat_status = 1;
		else throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

		$model=new Surat('draf');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Surat']))
			$model->attributes=$_GET['Surat'];

		$model->id_surat_status = $id_surat_status;

		$this->render('draf',array(
			'model'=>$model,
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Pegawai;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			$model->hashPassword();

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			$model->hashPassword();
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionImport()
	{
		
		spl_autoload_unregister(array('YiiBase','autoload'));
		
		Yii::import('application.extensions.PHPExcel',true);
		
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		$filename = Yii::app()->basePath.'/../import/eoffice.xls';
		
		$objReader = PHPExcel_IOFactory::createReader("Excel5");
		$objPHPExcel = $objReader->load($filename);
		
		$highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
		
		for($i=3;$i<=$highestRow;$i++)
		{
			$nip = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
			$nama = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
			$username = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
			$password = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();

			

			$pegawai = new Pegawai;
			$pegawai->nama = $nama;
			$pegawai->nip = $nip;
			$pegawai->username = $username;
			$pegawai->password = $password;
			$pegawai->save();
		}
		
			
	}

	public function actionSuntingProfil()
	{
		$model=Pegawai::findByUserId();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];

			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('profil'));
			}
		}

		$this->render('suntingProfil',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		
		if(Yii::app()->user->isGuest) {
			$this->redirect(array('site/login'));
		}

		$criteria = new CDbCriteria;
		$criteria->order = 'waktu_dibuat DESC';
		
		if(!User::isAdmin() AND !User::isKepala())
		{
			$criteria->condition = 'id IN (SELECT id_surat FROM disposisi WHERE ke = :ke)';
			$criteria->params = array(':ke'=>Yii::app()->user->id);
		}

		$dataProvider=new CActiveDataProvider('Surat',array(
			'criteria'=>$criteria
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Pegawai('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pegawai']))
			$model->attributes=$_GET['Pegawai'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Pegawai::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='pegawai-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
