<?php

class DistribusiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/column1';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  
				'actions'=>array('create',),
				'expression'=>'Distribusi::isCreatePermitted()',
			),
			array('allow', 
				'actions'=>array('view'),
				'expression'=>'Distribusi::isViewPermitted()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','update','admin','delete','directDelete'),
				'expression'=>'User::isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		
		$catatan = new Catatan;
		$catatan->model = 'Distribusi';
		$catatan->id_model = $model->id;

		if($model->waktu_dilihat==null AND $model->id_jabatan_penerima == Pegawai::getIdJabatanByUserId())
		{
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dilihat = date('Y-m-d H:i:s');
			$model->save();
		}

		if(isset($_POST['Catatan']))
		{
			$catatan->attributes = $_POST['Catatan'];

			date_default_timezone_set('Asia/Jakarta');
			$catatan->waktu_dibuat = date('Y-m-d H:i:s');
			$catatan->id_jabatan = Pegawai::getIdJabatanByUserId();
			$catatan->username = Yii::app()->user->id;

			$lampiran = CUploadedFile::getInstance($model,'lampiran');
			
			if($catatan->save())
			{
				$lampirans = CUploadedFile::getInstancesByName('lampirans');

				if (isset($lampirans) && count($lampirans) > 0) {
 
                	foreach ($lampirans as $key => $lampiran) 
                	{
                		$model_lampiran = new Lampiran;
                		$model_lampiran->id_model = $catatan->id;
                		$model_lampiran->model = 'Catatan';
                		$model_lampiran->berkas = str_replace(' ', '_', time().'_'.$lampiran->name);
                		$model_lampiran->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();
                		$model_lampiran->username_pembuat = Yii::app()->user->id;

                		date_default_timezone_set('Asia/Jakarta');
                		$model_lampiran->waktu_dibuat = date('Y-m-d H:i:s');

                		if($model_lampiran->save())
                		{
                			$path = Yii::app()->basePath.'/../uploads/catatan/lampiran/';
                			$lampiran->saveAs($path.$model_lampiran->berkas);
                		}
                	}
           		}

				Yii::app()->user->setFlash('success','Catatan berhasil ditambahkan');
				$this->redirect(array('distribusi/view','id'=>$id));
			}
		}
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'catatan'=>$catatan
		));
	}

	public function actionMemo($id_model)
	{
		$model = $this->loadModel($id);


		if($model->waktu_dilihat==null AND $model->id_jabatan_penerima == Pegawai::getIdJabatanByUserId())
		{	
			print "OK";
			$model->waktu_dilihat = date('Y-m-d H:i:s');
			$model->save();
		}
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionDetail($id)
	{
		$this->render('detail',array(
			'model'=>$this->loadModel($id),
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model = new Distribusi;

		if(isset($_GET['id_surat']))
			$model->id_surat = $_GET['id_surat'];

		if(isset($_GET['id_distribusi_jenis']))
			$model->id_distribusi_jenis = $_GET['id_distribusi_jenis'];
		
		if(isset($_POST['Distribusi']))
		{
			$tanda = Tanda::getTanda('Distribusi');

			if($_POST['Distribusi']['list_penerima']!=null) 
			{
				$list_penerima = $_POST['Distribusi']['list_penerima'];
				$penerimas = explode(';',$list_penerima);
				foreach($penerimas as $penerima) 
				{
					$model = new Distribusi;
					$model->attributes=$_POST['Distribusi'];

					$model->tanda = $tanda;

					$model->id_jabatan_pengirim = Pegawai::getIdJabatanByUsername(Yii::app()->user->id);
					$model->username_pengirim = Yii::app()->user->id;
					$model->setPengirim();

					$jabatan = Jabatan::findByNama($penerima);
					if($jabatan!==null)
						$jabatan->createDistribusi($model);
					
					$pegawai = Pegawai::findByNama($penerima);
					if($pegawai!==null)				
						$pegawai->createDistribusi($model);

				}
			} //END IF

			Yii::app()->user->setFlash('success','Surat berhasil dikirimkan');
			
			$this->redirect(array('surat/view','id'=>$model->id_surat));

		}// END IF ISSET $_POST['Disposisi']
		
		$this->render('create',array(
			'model'=>$model,
		));
		
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$model->id_disposisi_tindakan = json_decode($model->id_disposisi_tindakan);

		if(isset($_POST['Disposisi']))
		{
			$model->attributes=$_POST['Disposisi'];
			$model->id_disposisi_tindakan = json_encode($_POST['Disposisi']['id_disposisi_tindakan']);

			if($_POST['disposisi_nama']!=null)
			{
				$model->ke = Pegawai::getUsernameByNama($_POST['disposisi_nama']);
				$model->id_disposisi_tujuan = Pegawai::getIdJabatanByNama($_POST['disposisi_nama']);
			} else {
				$model->ke = Pegawai::getUsernameByIdJabatan($model->id_disposisi_tujuan);
			}

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionDirectDelete($id)
	{
		$model = $this->loadModel($id);
		
		$id_surat = $model->id_surat;
		
		if($model->delete())
		{
			Yii::app()->user->setFlash('success','Data berhasil dihapus');
			$this->redirect(array('surat/view','id'=>$id_surat));
		}
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		
		$criteria = new CDbCriteria;
		$criteria->order = 'waktu_dibuat DESC';
		
		if(!User::isAdmin() AND !User::isKepalaPusat())
		{
			$criteria->condition = 'id IN (SELECT id_surat FROM disposisi WHERE ke = :ke)';
			$criteria->params = array(':ke'=>Yii::app()->user->id);
		}

		$dataProvider=new CActiveDataProvider('Surat',array(
			'criteria'=>$criteria
		));

		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Distribusi('search');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Distribusi']))
			$model->attributes=$_GET['Distribusi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Distribusi::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='disposisi-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
