<?php

class SuratController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('konsep','pemeriksaan','disetujui','perbaikan','view',
					'review','setStatus','drafting','cetakAuto','cetakPdf','cetakSuratIzinPdf','cetakKeteranganPdf',
					'cetakKeteranganJalanPdf','cetakSuratKuasa','cetakSuratPernyataan','cetakSuratCuti',
					'cetakSuratUndangan','cetakSuratDatangTerlambat','cetakSuratPulang','cetakSuratTidakMasuk','cetakMemo','cetakSuratEdaran',
					'cetakPerjalananLuarNegeri','cetakSuratDinas','cetakSuratTugas','cetakSuratPerintah','cetakSuratKeputusan',
					'cetakMemoKepala','createParaf','cetakMemoSestama','kirimUntukDiperiksa','cetakSuratAgno','ubahMetode'
				),
				'expression'=>'Surat::isViewPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('createRevisi',
				),
				'expression'=>'Surat::isCreateRevisiPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('publish',
				),
				'expression'=>'Surat::isPublishPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('paraf',
				),
				'expression'=>'Surat::isParafPermitted()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('directDelete',
				),
				'expression'=>'Surat::isDirectDeletePermitted()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','ajaxListPenandatangan','latihan'
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','update','laporan','exportExcel'),
				'expression'=>'User::isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),		
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionLatihan()
	{
		$this->render('latihan');

	}

	public function actionCreateRevisi($id)
	{
		$model = $this->loadModel($id);

		$revisi = new Surat;
		$revisi->attributes = $model->attributes;

		date_default_timezone_set('Asia/Jakarta');
		$revisi->waktu_dibuat = date('Y-m-d H:i:s');

	}

	public function actionCreateParaf($id,$id_jabatan)
	{
		$jabatan = Jabatan::model()->findByPk($id_jabatan);
		$jabatan->createParaf($id);

		Yii::app()->user->setFlash('success','Otentifikasi berhasil ditambahkan');
		$this->redirect(array('surat/konsep','id'=>$id));
	}
	
	public function actionView($id)
	{

		$model = $this->loadModel($id);

		if($model->id_surat_status == 2)
			$this->redirect(array('surat/konsep','id'=>$model->id));

		if($model->id_surat_status == 3)
			$this->redirect(array('surat/pemeriksaan','id'=>$model->id));

		if($model->id_surat_status == 4)
			$this->redirect(array('surat/perbaikan','id'=>$model->id));

		if($model->id_surat_status == 5)
			$this->redirect(array('surat/disetujui','id'=>$model->id));

		if(isset($_POST['Surat']))
		{
			$draft = CUploadedFile::getInstance($model,'draft');

			if($draft !== null)
			{
				$model->draft = str_replace(' ','-', time().'_'.$draft->name);
				if($model->save())
				{
					$path = Yii::app()->basePath.'/../uploads/suratKeluar/draft/';
					$draft->saveAs($path.$model->draft);
					Yii::app()->user->setFlash('success','Draft Berhasil Diupload');
					$this->redirect(array('suratKeluar/view','id'=>$id));
				}
			}
		}

		if(isset($_POST['SuratAtribut']))
		{
			
			foreach($_POST['SuratAtribut'] as $key => $value)
			{
				
				$atribut = SuratAtribut::model()->findByAttributes(array('id_surat'=>$model->id,'nama'=>$key));
				
				if($atribut===null)
				{
					$atribut = new SuratAtribut;
					$atribut->id_surat = $model->id;
					$atribut->nama = $key;
				}

				$atribut->nilai=$value;
				$atribut->save();
			}

				
			Yii::app()->user->setFlash('success','Draft Berhasil Diinput');
			$this->redirect(array('view','id'=>$id));	
	
		}

		$this->render('view',array(
			'model'=>$model,
		));
	}

	public function actionAjaxListPenandatangan($id_surat_jenis=null)
	{
		$model = SuratJenis::model()->findByPk($id_surat_jenis);

		$output = '';

		if($model!==null)
		{
			foreach($model->getListPenandatangan() as $key => $value)
			{
				$output .= '<option value="'.$key.'">'.$value.'</option>';
			}
		}

		print $output;
	}

	public function actionKonsep($id)
	{
		$model = $this->loadModel($id);

		$draf_lama = $model->draf;
		$lampiran_lama = $model->lampiran;

		if($model->id_surat_status != 2)
			$this->redirect(array('surat/view','id'=>$model->id));

		if(isset($_POST['Surat']))
		{
			$model->attributes = $_POST['Surat'];

            $draf = CUploadedFile::getInstance($model,'draf');
			
			if($draf !== null)
			{
				$model->draf = str_replace(' ','-', time().'_'.$draf->name);
			} else {
				$model->draf = $draf_lama;
			}

			if($model->save())
			{
				if($draf !== null)
				{
					$path = Yii::app()->basePath.'/../uploads/surat/draf/';
					$draf->saveAs($path.$model->draf);
				}

				$lampirans = CUploadedFile::getInstancesByName('lampirans');

				if (isset($lampirans) && count($lampirans) > 0) {
 
                	foreach ($lampirans as $key => $lampiran) 
                	{
                		$model_lampiran = new Lampiran;
                		$model_lampiran->id_model = $model->id;
                		$model_lampiran->model = 'Surat';
                		$model_lampiran->berkas = str_replace(' ', '_', time().'_'.$lampiran->name);
                		$model_lampiran->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();
                		$model_lampiran->username_pembuat = Yii::app()->user->id;

                		date_default_timezone_set('Asia/Jakarta');
                		$model_lampiran->waktu_dibuat = date('Y-m-d H:i:s');

                		if($model_lampiran->save())
                		{
                			$path = Yii::app()->basePath.'/../uploads/surat/lampiran/';
                			$lampiran->saveAs($path.$model_lampiran->berkas);
                		}
                	}
           		}
			}
		}

		if(isset($_POST['SuratAtribut']))
		{
			
			foreach($_POST['SuratAtribut'] as $key => $value)
			{
				
				$atribut = SuratAtribut::model()->findByAttributes(array('id_surat'=>$model->id,'nama'=>$key));
				
				if($atribut===null)
				{
					$atribut = new SuratAtribut;
					$atribut->id_surat = $model->id;
					$atribut->nama = $key;
				}

				$atribut->nilai=$value;
				$atribut->save();
			}
		}


		if(isset($_POST['Surat']) OR isset($_POST['SuratAtribut']))
		{
			Yii::app()->user->setFlash('success','Konsep Surat Berhasil Disimpan');
			$this->redirect(array('konsep','id'=>$id));
		}

		$this->render('konsep',array(
			'model'=>$model,
		));
	}

	public function actionPemeriksaan($id)
	{
		$model = $this->loadModel($id);

		if($model->id_surat_status!=3)
			$this->redirect(array('surat/view','id'=>$id));

		$jumlah_paraf_review = $model->countParafByStatus(3);
		//$paraf = $model->findParafByStatus(3);
		if($jumlah_paraf_review>1)
		{
			foreach($model->findAllParafByStatus(3) as $paraf)
			{
				$paraf->id_paraf_status = 2;
				$paraf->save();
			}
		
		}

		$jumlah_paraf_review = $model->countParafByStatus(3);
		
		if($jumlah_paraf_review==0)
		{
			$model->updateStatus();
			$this->redirect(array('surat/view','id'=>$id));
		}

		$this->render('pemeriksaan',array(
			'model'=>$model,
		));
	}

	public function actionDisetujui($id)
	{
		$model = $this->loadModel($id);

		if($model->id_surat_status!=5)
			$this->redirect(array('surat/view','id'=>$id));

		$this->render('disetujui',array(
			'model'=>$model,
		));
	}

	public function actionSetStatus($id,$status)
	{
		$model = $this->loadModel($id);
		$model->id_surat_status = $status;

		if($model->save())
		{
			$model->updateStatus();
			$this->redirect(array('surat/view','id'=>$model->id));
		}	
	}

	public function actionKirimUntukDiperiksa($id)
	{
		$this->redirect(array('surat/setStatus','id'=>$id,'status'=>3));
	}

	public function actionParaf($id)
	{
		$model = $this->loadModel($id);
		
		$paraf = $model->findParafByIdJabatan(Pegawai::getIdJabatanByUserId());

		$catatan = new Catatan;
		
		$catatan->model = 'Paraf';
		$catatan->id_model = $paraf->id;
		
		$catatan->id_jabatan = Pegawai::getIdJabatanByUserId();
		$catatan->username = Yii::app()->user->id;

		
		if(isset($_POST['Catatan']))
		{
			$catatan->attributes = $_POST['Catatan'];

			date_default_timezone_set('Asia/Jakarta');
			$catatan->waktu_dibuat = date('Y-m-d H:i:s');
			
			if($catatan->save())
			{
				$lampirans = CUploadedFile::getInstancesByName('lampirans');

				if (isset($lampirans) && count($lampirans) > 0) {
 
                	foreach ($lampirans as $key => $lampiran) 
                	{
                		$model_lampiran = new Lampiran;
                		$model_lampiran->id_model = $catatan->id;
                		$model_lampiran->model = 'Catatan';
                		$model_lampiran->berkas = str_replace(' ', '_', time().'_'.$lampiran->name);
                		$model_lampiran->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();
                		$model_lampiran->username_pembuat = Yii::app()->user->id;

                		date_default_timezone_set('Asia/Jakarta');
                		$model_lampiran->waktu_dibuat = date('Y-m-d H:i:s');

                		if($model_lampiran->save())
                		{
                			$path = Yii::app()->basePath.'/../uploads/catatan/lampiran/';
                			$lampiran->saveAs($path.$model_lampiran->berkas);
                		}
                	}
           		}

				Yii::app()->user->setFlash('success','Catatan berhasil ditambahkan');
				$this->redirect(array('surat/paraf','id'=>$id));
			}

		}

		if($paraf->waktu_dilihat == null)
		{	
			date_default_timezone_set('Asia/Jakarta');
			$paraf->waktu_dilihat = date('Y-m-d H:i:s');
			$paraf->save();
		}

		
		$this->render('paraf',array(
			'model'=>$model,
			'paraf'=>$paraf,
			'catatan'=>$catatan
		));
	
	
		
	}

	public function actionDrafting($id) 
	{
		$model = $this->loadModel($id);

		$this->render('drafting',array(
			'model'=>$model,
		));
	}

	public function actionPerbaikan($id)
	{
		$model = $this->loadModel($id);
		$draf_lama = $model->draf;

		$paraf = $model->findParafByStatus(4);

		if($paraf===null)
		{
			$model->id_surat_status = 3;
			$model->save();
			$model->updateStatus();

			$this->redirect(array('surat/view','id'=>$model->id));
		}


		if(isset($_POST['Surat']))
		{
			$model->tembusan = $_POST['Surat']['tembusan'];

            $draf = CUploadedFile::getInstance($model,'draf');
			
			if($draf !== null)
			{
				$model->draf = str_replace(' ','-', time().'_'.$draf->name);
			} else {
				$model->draf = $draf_lama;
			}

			if($model->save())
			{
				if($draf !== null)
				{
					$path = Yii::app()->basePath.'/../uploads/surat/draf/';
					$draf->saveAs($path.$model->draf);
				}

				$lampirans = CUploadedFile::getInstancesByName('lampirans');

				if (isset($lampirans) && count($lampirans) > 0) {
 
                	foreach ($lampirans as $key => $lampiran) 
                	{
                		$model_lampiran = new Lampiran;
                		$model_lampiran->id_model = $model->id;
                		$model_lampiran->model = 'Surat';
                		$model_lampiran->berkas = str_replace(' ', '_', time().'_'.$lampiran->name);
                		$model_lampiran->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();
                		$model_lampiran->username_pembuat = Yii::app()->user->id;

                		date_default_timezone_set('Asia/Jakarta');
                		$model_lampiran->waktu_dibuat = date('Y-m-d H:i:s');

                		if($model_lampiran->save())
                		{
                			$path = Yii::app()->basePath.'/../uploads/surat/lampiran/';
                			$lampiran->saveAs($path.$model_lampiran->berkas);
                		}
                	}
           		}
			}
		}

		if(isset($_POST['SuratAtribut']))
		{
			
			foreach($_POST['SuratAtribut'] as $key => $value)
			{
				
				$atribut = SuratAtribut::model()->findByAttributes(array('id_surat'=>$model->id,'nama'=>$key));
				
				if($atribut===null)
				{
					$atribut = new SuratAtribut;
					$atribut->id_surat = $model->id;
					$atribut->nama = $key;
				}

				$atribut->nilai=$value;
				$atribut->save();
			}
		}

		$catatan = new Catatan;
		$catatan->id_model = $paraf->id;
		$catatan->model = 'Paraf';
		$catatan->id_jabatan = Pegawai::getIdJabatanByUserId();
		$catatan->username = Yii::app()->user->id;

		if(isset($_POST['Catatan']))
		{
			$catatan->attributes = $_POST['Catatan'];

			date_default_timezone_set('Asia/Jakarta');
			$catatan->waktu_dibuat = date('Y-m-d H:i:s');

			if($catatan->save())
			{
				$lampirans = CUploadedFile::getInstancesByName('lampirans');

				if (isset($lampirans) && count($lampirans) > 0) {
 
                	foreach ($lampirans as $key => $lampiran) 
                	{
                		$model_lampiran = new Lampiran;
                		$model_lampiran->id_model = $catatan->id;
                		$model_lampiran->model = 'Catatan';
                		$model_lampiran->berkas = str_replace(' ', '_', time().'_'.$lampiran->name);
                		$model_lampiran->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();
                		$model_lampiran->username_pembuat = Yii::app()->user->id;

                		date_default_timezone_set('Asia/Jakarta');
                		$model_lampiran->waktu_dibuat = date('Y-m-d H:i:s');

                		if($model_lampiran->save())
                		{
                			$path = Yii::app()->basePath.'/../uploads/catatan/lampiran/';
                			$lampiran->saveAs($path.$model_lampiran->berkas);
                		}
                	}
           		}
			}

		}

		if($paraf->waktu_dilihat == null)
		{	
			date_default_timezone_set('Asia/Jakarta');
			$paraf->waktu_dilihat = date('Y-m-d H:i:s');
			$paraf->save();
		}

		if(isset($_POST['Surat']) OR isset($_POST['SuratAtribut']) OR isset($_POST['Catatan']))
		{
			Yii::app()->user->setFlash('success','Perbaikan Surat Berhasil Diperbarui');
			$this->redirect(array('perbaikan','id'=>$id));
		}


		$this->render('perbaikan',array(
			'model'=>$model,
			'paraf'=>$paraf,
			'catatan'=>$catatan
		));
	}

	public function actionUbahMetode($id)
	{
		$model = $this->loadModel($id);
		if($model->id_surat_metode == 1)
		{
			$model->id_surat_metode = 2;
			$model->save();
		} elseif($model->id_surat_metode == 2) {
			$model->id_surat_metode = 1;
			$model->save();
		}

		Yii::app()->user->setFlash('success','Metode pengisian draf/surat berhasil diubah');
		$this->redirect(array('surat/konsep','id'=>$id));
		
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$pegawai = Pegawai::findByUserId();

		if($pegawai->id_jabatan==null)
		{
			Yii::app()->user->setFlash('danger','Anda belum ditempatkan dalam sistem struktur organisasi. Silahkan hubungi admin');
			$this->redirect(array('pegawai/profil'));
		}

		$model=new Surat;

		if(isset($_POST['Surat']))
		{
			$model->attributes=$_POST['Surat'];
			$model->paraf = $_POST['Surat']['paraf'];

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');

			$model->setUsernamePenerbit();

			$model->username_pembuat = Yii::app()->user->id;
			$model->id_jabatan_pembuat = Pegawai::getIdJabatanByUserId();

			$model->id_surat_kop = 3;

			if($model->username_pengaju!=null)
				$model->id_jabatan_pengaju = Pegawai::getIdJabatanByUsername($model->username_pengaju);
			else {
				$model->username_pengaju = Yii::app()->user->id;
				$model->id_jabatan_pengaju = Pegawai::getIdJabatanByUserId();
			}

			$model->id_surat_status = 2;

			if($model->save())
			{
				if($model->paraf==1)
					$model->createAllParaf();

				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionPublish($id)
	{
		$model=$this->loadModel($id);

		if($model->nomor==null)
			$model->setNomor();

		if($model->tanggal==null)
		{
			date_default_timezone_set('Asia/Jakarta');
			$model->tanggal = date('Y-m-d');
		}

		if(isset($_POST['Surat']))
		{
			$model->attributes=$_POST['Surat'];
			$model->id_surat_status = 1;
			$model->setUrutan();

			$model->id_jabatan_penerbit = Pegawai::getIdJabatanByUserId();
			$model->username_penerbit = Yii::app()->user->id;

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_diterbitkan = date('Y-m-d H:i:s');

			$berkas = CUploadedFile::getInstance($model,'berkas');
			
			if($berkas!==null)
				$model->berkas = str_replace(' ', '-', time().'_'.$berkas->name);

			if($model->save())
			{
				if($berkas!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/surat/berkas/';
					$berkas->saveAs($path.$model->berkas);
				}

				$model->sendSurat();

				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('publish',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDirectDelete($id)
	{
		$model = $this->loadModel($id);

		if($model->delete())
		{
			Yii::app()->user->setFlash('success','Draf berhasil dihapus');
			$this->redirect(array('pegawai/draf'));
		}
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('SuratKeluar');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Surat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Surat']))
			$model->attributes=$_GET['Surat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Surat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

//===========================================PDF//===========================================
//===========================================PDF//===========================================	

	public function actionCetakPdf($id) 
	{
		$model = $this->loadModel($id);
		
		if($model->id_surat_jenis==7)
			$this->redirect(array('cetakSuratIzinPdf','id'=>$id));
		elseif ($model->id_surat_jenis==8)
			$this->redirect(array('cetakKeteranganPdf','id'=>$id));
		elseif ($model->id_surat_jenis==9)
			$this->redirect(array('cetakKeteranganJalanPdf','id'=>$id));
		elseif ($model->id_surat_jenis==10)
			$this->redirect(array('cetakSuratKuasa','id'=>$id));	
		elseif ($model->id_surat_jenis==11)
			$this->redirect(array('cetakSuratPernyataan','id'=>$id));		
		elseif ($model->id_surat_jenis==12)
			$this->redirect(array('cetakSuratCuti','id'=>$id));	
		elseif ($model->id_surat_jenis==13)
			$this->redirect(array('cetakSuratUndangan','id'=>$id));		
		elseif ($model->id_surat_jenis==14)
			$this->redirect(array('cetakSuratDatangTerlambat','id'=>$id));	
		elseif ($model->id_surat_jenis==15)
			$this->redirect(array('cetakSuratPulang','id'=>$id));	
		elseif ($model->id_surat_jenis==16)
			$this->redirect(array('cetakSuratTidakMasuk','id'=>$id));	
		elseif ($model->id_surat_jenis==17)
			$this->redirect(array('cetakMemo','id'=>$id));
		elseif ($model->id_surat_jenis==18)
			$this->redirect(array('cetakSuratAgno','id'=>$id));		
		elseif ($model->id_surat_jenis==19)
			$this->redirect(array('cetakMemoKepala','id'=>$id));
		elseif ($model->id_surat_jenis==20)
			$this->redirect(array('cetakMemoSestama','id'=>$id));																											
		elseif ($model->id_surat_jenis==6)
			$this->redirect(array('cetakSuratEdaran','id'=>$id));	
		elseif ($model->id_surat_jenis==5)
			$this->redirect(array('cetakPerjalananLuarNegeri','id'=>$id));		
		elseif ($model->id_surat_jenis==4)
			$this->redirect(array('cetakSuratDinas','id'=>$id));	
		elseif ($model->id_surat_jenis==3)
			$this->redirect(array('cetakSuratTugas','id'=>$id));
		elseif ($model->id_surat_jenis==2)
			$this->redirect(array('cetakSuratPerintah','id'=>$id));	
		elseif ($model->id_surat_jenis==1)
			$this->redirect(array('cetakSuratKeputusan','id'=>$id));											
	}

	public function actionCetakSuratIzinPdf($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_izin',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}


public function actionCetakKeteranganPdf($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_keterangan',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

public function actionCetakKeteranganJalanPdf($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_keterangan_jalan',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

public function actionCetakSuratKuasa($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_kuasa',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakSuratPernyataan($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_pernyataan',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakSuratCuti($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_cuti',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakSuratUndangan($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_undangan',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$lampiran = explode(PHP_EOL,$model->getAtribut('lampiran_undangan'));	
		
		if(count($lampiran)!==null) 
		{ 
			$pdf->AddPage();
			$html = $this->renderPartial('cetak_surat_undangan_lampiran',array('model'=>$model),true);

			$pdf->WriteHTML($html);
		}

		print $pdf->Output('tes.pdf');			
	}

	public function actionCetakSuratDatangTerlambat($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_datang_terlambat',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakSuratPulang($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_pulang',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakSuratTidakMasuk($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_tidak_masuk',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}	

	public function actionCetakMemo($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_memo',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');
		
		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}	

	public function actionCetakMemoKepala($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_memo_kepala',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');
		
		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}	

	public function actionCetakMemoSestama($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('helvetica', '', 11);
		
		$model = $this->loadModel($id);
		
		$html = '';
	
		$html .= $this->renderPartial('//layouts/kop',array(),true);
		
		//$html .= '<div style="width:50%;margin-left:auto;margin-right:auto;border:1px solid #000">';
		
		$html .= '<h1 style="font-weight:bold;text-align:center;text-decoration:none;font-size:38px">MEMO<br>';
		$html .= '<span style="font-weight:bold;text-align:center;text-decoration:underline;font-size:32px">';
		$html .= 'Nomor: M-'.$model->getAtribut('no_surat').'/'.$model->getAtribut('jabatan').'/'.$model->getAtribut('bag_organisasi').'/Sestama/Bakamla/'.$model->getAtribut('bulan_romawi').'/'.$model->getAtribut('tahun');
		$html .= '</span>';
		$html .= '</h1>';

		$html .= '<div>&nbsp;</div>';

		$html .= '<table class="surat">
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Kepada Yth.</td>
		<td width="65%">: '.$model->getAtribut('penerima_memo').'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: '.$model->getAtribut('pengirim_memo').'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: '.$model->getAtribut('perihal').'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: '.$model->getAtribut('lampiran').'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: '.$model->getAtribut('tanggal_memo').'</td>
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"> Dengan Hormat,</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Bersama Ini Kami Sampaikan '.$model->getAtribut('isi').'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Demikian disampaikan, mohon arahan lebih lanjut dan terima kasih.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">'.$model->getAtribut('jabatan').'</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">'.$model->getAtribut('pengirim_memo').'</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. '.$model->getAtribut('nip').'</td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td style="font-weight:bold;text-decoration:underline">Tembusan Yth.:</td>
	</tr>
	<tr>
		<td>
			'.$model->getAtribut('tembusan').'
		</td>
	</tr>
	
	</table>';

		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
	}

	public function actionCetakSuratEdaran($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_edaran',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}

	public function actionCetakPerjalananLuarNegeri($id)
{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_perjalanan_luar_negeri',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}


	public function actionCetakSuratDinas($id)
{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_dinas',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}


	public function actionCetakSuratTugas($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_tugas',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));	
		
		if(count($daftar_pegawai)>3) 
		{ 
			$pdf->AddPage();
			$html = $this->renderPartial('cetak_surat_perintah_lampiran',array('model'=>$model),true);

			$pdf->WriteHTML($html);
		}

		$pdf->Output();			
	}

	public function actionCetakSuratPerintah($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_perintah',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));	
		
		if(count($daftar_pegawai)>3) 
		{ 
			$pdf->AddPage();
			$html = $this->renderPartial('cetak_surat_perintah_lampiran',array('model'=>$model),true);

			$pdf->WriteHTML($html);
		}

		$pdf->Output();			
	}


	public function actionCetakSuratKeputusan($id)
	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_keputusan',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));	
		
		if(count($daftar_pegawai)>3) 
		{ 
			$pdf->AddPage();
			$html = $this->renderPartial('cetak_surat_perintah_lampiran',array('model'=>$model),true);

			$pdf->WriteHTML($html);
		}

		$pdf->Output();			
	}

	public function actionCetakSuratAgno($id)
	{
		$model = $this->loadModel($id);
		 $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 40;
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','A4',9,'Arial',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$model = $this->loadModel($id);

		$html = $this->renderPartial('cetak_surat_agno',array('model'=>$model),true);
		$header = $model->getSuratKop('header');
		$footer = $model->getSuratKop('footer');

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
	}	



/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Surat']))
		{
			$model->attributes=$_POST['Surat'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='surat-keluar-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

/*public function actionLaporan()
{
	$laporanform = new LaporanForm;
	if(isset($_POST['LaporanForm']))
	{
	$laporanform->attributes=$_POST['LaporanForm'];
	if($laporanform->validate())

		{
					$this->redirect(array(
						'surat/ExportExcel',
						'tanggal_awal'=>$_POST['LaporanForm']['tanggal_awal'],
						'tanggal_akhir'=>$_POST['LaporanForm']['tanggal_akhir'],
						'jenis_surat' => $_POST['LaporanForm']['jenis_surat'],
					));
		}
	}
	$this->render('laporan',array(
		'laporanform'=>$laporanform
	));
}

	public function actionExportExcel()
	{

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$criteria = new CDbCriteria;
			$params = array();

			if(!empty($tanggal_awal)) {
				$criteria->addCondition('waktu_diterbitkan >= :waktu_awal');
				$params[':waktu_awal'] = $tanggal_awal;
			}

			if(!empty($tanggal_akhir)) {
				$criteria->addCondition('waktu_diterbitkan <= :waktu_akhir');
				$params[':waktu_akhir'] = $tanggal_akhir;
			}			

			if(!empty($jenis_surat)) {
				$criteria->addCondition('id_surat_jenis=:jenis_surat');
				$params[':jenis_surat'] = $jenis_surat;
			}

			$criteria->params = $params;
			$criteria->order = 'waktu_diterbitkan ASC';



			$PHPExcel = new PHPExcel();

			$PHPExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle("A1:H1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:H1');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "LAPORAN SURAT TERBIT");
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nomor Surat');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Tanggal');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Jenis Surat');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Pengaju');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Pembuat');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Penandatangan');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Waktu Diterbitkan');

				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

			$i = 1;
			$kolom = 4;

			foreach(Surat::model()->findAll($criteria) as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nomor);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, Helper::getTanggal($data->tanggal));
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->getRelationField('surat_jenis','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->getRelationField('jabatan_pengaju','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->getRelationField('jabatan_pembuat','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->getRelationField('jabatan_penandatangan','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, Helper::getTanggalWaktu($data->waktu_diterbitkan));

				$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:H'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
									
				$i++; $kolom++;
			}

			$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setWrapText(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	
			$filename = time().'_LaporanSurat.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}*/

	public function actionLaporan()
	{
		$laporanform = new LaporanForm;
		if(isset($_POST['LaporanForm']))
		{

			$laporanform->attributes=$_POST['LaporanForm'];
			if($laporanform->validate())
			{

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$criteria = new CDbCriteria;
			$params = array();

			if(!empty($_POST['LaporanForm']['tanggal_awal'])) {
				$criteria->addCondition('waktu_diterbitkan >= :waktu_awal');
				$params[':waktu_awal'] =$_POST['LaporanForm']['tanggal_awal'];
			}

			if(!empty($_POST['LaporanForm']['tanggal_akhir'])) {
				$criteria->addCondition('waktu_diterbitkan <= :waktu_akhir');
				$params[':waktu_akhir'] = $_POST['LaporanForm']['tanggal_akhir'];
			}			

			if(!empty($_POST['LaporanForm']['jenis_surat'])) {
				$criteria->addCondition('id_surat_jenis=:jenis_surat');
				$params[':jenis_surat'] = $_POST['LaporanForm']['jenis_surat'];
			}


			$criteria->params = $params;
			$criteria->order = 'waktu_diterbitkan ASC';



			$PHPExcel = new PHPExcel();

			$PHPExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle("A1:H1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:H1');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "LAPORAN SURAT TERBIT");
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Nomor Surat');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Tanggal');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Jenis Surat');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Pengaju');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Pembuat');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Penandatangan');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Waktu Diterbitkan');

				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

			$i = 1;
			$kolom = 4;

			foreach(Surat::model()->findAll($criteria) as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->nomor);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, Helper::getTanggal($data->tanggal));
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->getRelationField('surat_jenis','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->getRelationField('jabatan_pengaju','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->getRelationField('jabatan_pembuat','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->getRelationField('jabatan_penandatangan','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, Helper::getTanggalWaktu($data->waktu_diterbitkan));

				$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:H'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
									
				$i++; $kolom++;
			}

			$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setWrapText(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:H'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		
			$filename = time().'_LaporanSurat.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');

			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
		}
	}
		$this->render('laporan',array(
		'laporanform'=>$laporanform
	));
}

}
