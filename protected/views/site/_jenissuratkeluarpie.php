
    <?php $this->widget('booster.widgets.TbHighCharts', array(
      'options' => array(
        'title' => array(
          'text' => 'Grafik Surat Keluar',
        ),
        'xAxis' => array(
          'categories' => ['Lembaga']
        ),
        'yAxis' => array(
          'title' => array(
            'text' => 'Total',
          ),
        ),
        'series' => array(
          array(
            'type' => 'pie',
            'name' => 'Jenis Surat',
            'data' => SuratKeluar::model()->getdataChartSuratKeluarPie()
          ),
        ),
      ),
    ));?>