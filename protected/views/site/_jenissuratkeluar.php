    <?php $this->widget('booster.widgets.TbHighCharts', array(
      'options' => array(
        'title' => array(
          'text' => 'Grafik Surat Keluar Berdasarkan Jenis',
        ),
        'xAxis' => array(
          'categories' => ['Jenis Surat']
        ),
        'yAxis' => array(
          'title' => array(
            'text' => 'Total',
          ),
        ),
        'series' => SuratKeluar:: getdataChartSuratKeluarColumn()
      ),
    ));?>
