
<h3>Surat Keluar</h3>

<table class="table" width="100%">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="20%">Jenis Surat</th>
	<th width="30%" style="text-align:center">Ringkasan</th>
	<th width="20%">Pembuat</th>
	<th width="20%" style="text-align:center">Status</th>
	<th width="5%" style="text-align:center">Action</th>
</tr>
</thead>
<?php $i=1; foreach(Pegawai::model()->findAllParafByStatus(3,'Surat') as $paraf) { ?>
<?php $surat = $paraf->findSurat(); ?>
<?php if($surat!==null) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $surat->getRelationField("surat_jenis","nama"); ?></td>
	<td><?php print $surat->ringkasan; ?></td>
	<td>
		<?php $this->widget('booster.widgets.TbLabel',array(
				'context' => 'success', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label' => $surat->username_pembuat
		)); ?>
	</td>
	<td style="text-align:center">
		<?php $this->widget('booster.widgets.TbLabel',array(
				'context' => 'primary', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label' => $surat->getRelationField("surat_status","nama")
		)); ?>
	</td>
	<td style="text-align:center">
		<?php print CHtml::link("<i class='glyphicon glyphicon-search'></i>",array("surat/review","id"=>$surat->id)); ?>
	</td>
</tr>
<?php } ?>
<?php $i++; } ?>
</table>	