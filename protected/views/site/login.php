<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<?php /*
<div id="logo">
	<?php print CHtml::image(Yii::app()->baseUrl."/images/logo-bakamla.png"); ?>
</div>
*/ ?>

<?php $this->beginWidget('booster.widgets.TbPanel',array(
        'title' => 'Aplikasi Persuratan Bakamla',
        'headerIcon' => 'lock',
		'context'=>'primary',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'bootstrap-widget-table','style'=>'margin-bottom:0px;')
));?>

<div id="site-login" style="margin:0px;">

	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'type'=>'horizontal',
		'htmlOptions'=>array('class'=>'wells'),
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
			
		<?php echo $form->textFieldGroup($model,'username',array(
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 
			'prepend'=>'<i class="glyphicon glyphicon-user"></i>'
		)); ?>

		<?php echo $form->passwordFieldGroup($model,'password',array(
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>
		
		
		<hr>

		<div class="form-actions" style="text-align:right">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'primary',
					'label'=>'Login',
					'icon'=>'lock white',
			)); ?>
		</div>

	<?php $this->endWidget(); ?>

</div>

<?php $this->endWidget(); ?>