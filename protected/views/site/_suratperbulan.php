  <div class="form-group">
    <?php Print CHtml:: dropDownList('waktu_dibuat','',CHtml::listData(Surat::model()->findAll(),'id','waktu_dibuat'),array('class'=>'form-control', 'placeholder' => 'Kategori', 'empty' => '-- Semua Waktu --')); ?>
  </div>

    <?php $this->widget('booster.widgets.TbHighCharts', array(
      'options' => array(
        'title' => array(
          'text' => 'Grafik Surat Per Bulan',
        ),
        'xAxis' => array(
          'categories' => ['Bulan']
        ),
        'yAxis' => array(
          'title' => array(
            'text' => 'Total',
          ),
        ),
        'series' => SuratKeluar:: getChartSuratPerBulan()
      ),
    ));?>
