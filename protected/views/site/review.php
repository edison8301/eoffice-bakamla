<?php
	$this->breadcrumbs = array(
		'home'=>array('site/index'),
		'Berkas Surat'
	)
?>

<h1>Daftar Surat Yang Harus Direview</h1>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Surat Keluar','content'=>$this->renderPartial('_review_surat_keluar',array(),true),'active'=>true),
			array('label'=>'Surat Masuk','content'=>$this->renderPartial('_review_surat_masuk',array(),true)),
		),
)); ?>