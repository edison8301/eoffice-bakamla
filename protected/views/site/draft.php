<?php
/* @var $this SiteController */



$this->pageTitle=Yii::app()->name;
?>

<?php
	$this->breadcrumbs = array(
		'home'=>array('site/index'),
		'Berkas Surat'
	)

?>

<h1>Berkas Surat</h1>

<hr>




<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Draft','content'=>$this->renderPartial('_draft',array(),true),'active'=>true),
			array('label'=>'Review','content'=>$this->renderPartial('_review',array(),true)),
			array('label'=>'Perbaikan','content'=>$this->renderPartial('_perbaikan',array(),true)),
			array('label'=>'Disetujui','content'=>$this->renderPartial('_disetujui',array(),true)),
			array('label'=>'Ditolak','content'=>$this->renderPartial('_ditolak',array(),true)),
		),
)); ?>