<?php
/* @var $this SiteController */



$this->pageTitle=Yii::app()->name;
?>

<?php
	$this->breadcrumbs = array(
		'home'=>array('site/index')
	)

?>

<h1>Dashboard</h1>

<hr>

<div>&nbsp;</div>

<div class="row">
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/memo"); ?>">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-tag"></i></div>
			<div class="title"> Memo Baru</div>
			<div class="content"><?php print Pegawai::countDisposisiMemoBaruByUserId(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/suratKeluar"); ?>">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Surat Baru</div>
			<div class="content"><?php print Pegawai::countDisposisiSuratKeluarBaruByUserId(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/surat"); ?>">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-share-alt"></i></div>
			<div class="title"> Disposisi Baru</div>
			<div class="content"><?php print Pegawai::countDisposisiSuratBaruByUserId(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("site/review"); ?>">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-search"></i></div>
			<div class="title"> Paraf</div>
			<div class="content"><?php print Paraf::countByUserId(); ?></div>
		</div>
		</a>
	</div>
</div>

<div>&nbsp;</div>

<div>&nbsp;</div>

<?php /*
<div class="row">
	<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jumlah Surat Per Bulan',
        		'context'=>'primary',
        		'headerIcon' => 'envelope',
        		'content' => $this->renderPartial('_suratkeluarperbulan',array(),true)
    	)); ?>
    </div>
    <div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jumlah Memo Per Bulan',
        		'context' => 'primary',
        		'headerIcon' => 'file',
        		'content' => $this->renderPartial('_suratperbulan',array(),true)
    	)); ?>
    </div>
</div>


<div class="row">
	<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jenis Surat Keluar',
        		'headerIcon' => 'bullhorn',
        		'context'=>'primary',
        		'content' => $this->renderPartial('_jenissuratkeluar',array(),true)
    	)); ?>
    </div>
    <div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jenis Surat Keluar',
        		'context'=>'primary',
        		'headerIcon' => 'share-alt',
        		'content' => $this->renderPartial('_jenissuratkeluarpie',array(),true)
    	)); ?>
    </div>
</div>



<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Draft','content'=>$this->renderPartial('_draft',array(),true),'active'=>true),
			array('label'=>'Review','content'=>$this->renderPartial('_review',array(),true)),
			array('label'=>'Perbaikan','content'=>$this->renderPartial('_perbaikan',array(),true)),
			array('label'=>'Disetujui','content'=>$this->renderPartial('_disetujui',array(),true)),
			array('label'=>'Ditolak','content'=>$this->renderPartial('_ditolak',array(),true)),
		),
)); ?>

*/ ?>