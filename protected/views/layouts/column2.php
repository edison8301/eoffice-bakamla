<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div id="sidebar">
	<?php if(User::isAdmin()) { ?>
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'stacked'=>true,
			'items' => array(
				array('label'=>'Dashboard','url'=>array('site/index'),'icon'=>'home'),
				array('label'=>'Surat', 'url'=>array('/surat/admin'),'icon'=>'envelope'),
				array('label'=>'Jenis Surat', 'url'=>array('/suratJenis/admin'),'icon'=>'envelope'),
				array('label'=>'Laporan', 'url'=>array('/surat/report'),'icon'=>'th-list','visible'=>User::isAdmin()),
				array('label'=>'User', 'url'=>array('/user/admin'),'icon'=>'user','visible'=>User::isAdmin()),
				array('label'=>'Jabatan', 'url'=>array('/jabatan/index'),'icon'=>'asterisk','visible'=>User::isAdmin()),
				array('label'=>'Pegawai', 'url'=>array('/pegawai/admin'),'icon'=>'user','visible'=>User::isAdmin()),
				array('label'=>'Setting', 'url'=>array('/historiGaji/index'),'icon'=>'wrench','items'=>array(
					array('label'=>'Sifat Disposisi','url'=>array('disposisiSifat/admin')),
					array('label'=>'Tindakan Disposisi','url'=>array('disposisiTindakan/admin')),
					array('label'=>'Jenis Surat Keluar','url'=>array('suratKeluarJenis/admin')),
				),'visible'=>User::isAdmin()),
				array('label'=>'Log Out ('.Yii::app()->user->id.')','url'=>array('site/logout'),'icon'=>'off','visible'=>!Yii::app()->user->isGuest),
			)
	)); ?>
	<?php } ?>
	<?php if(User::isPegawai()) { ?>
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'stacked'=>true,
			'items' => array(
				array('label'=>'Home','url'=>array('pegawai/index'),'icon'=>'home'),
				array('label'=>'Surat', 'url'=>array('/pegawai/surat'),'icon'=>'envelope'),
				array('label'=>'Draf Surat','url'=>array('pegawai/draf'),'icon'=>'pencil'),
				array('label'=>'Disposisi','url'=>array('pegawai/disposisi'),'icon'=>'envelope'),
				array('label'=>'Tembusan','url'=>array('pegawai/tembusan'),'icon'=>'envelope'),
				array('label'=>'Terusan','url'=>array('pegawai/terusan'),'icon'=>'envelope'),
				array('label'=>'Paraf Surat','url'=>array('pegawai/paraf'),'icon'=>'envelope'),
				array('label'=>'Pemberitahuan','url'=>array('pegawai/pemberitahuan'),'icon'=>'bullhorn'),
				array('label'=>'Logout ('.Yii::app()->user->id.')','url'=>array('site/logout'),'icon'=>'off','visible'=>!Yii::app()->user->isGuest),
			)
	)); ?>

	<?php } ?>
</div>

<div id="mainbar">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="col-xs-12">
				<?php if(isset($this->breadcrumbs)):?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
					)); ?><!-- breadcrumbs -->
				<?php endif?>
			</div>
		</div>
		
		<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
		
		<div>&nbsp;</div>
		
		<div class="row-fluid">
			<div class="col-xs-12">
				<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<?php } ?>

		<div class="row-fluid">
			<div class="col-xs-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div><!-- content -->

<?php $this->endContent(); ?>