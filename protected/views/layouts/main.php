<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/vendors/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/style.css" />
		
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- Smartsupp Live Chat script -->
	<script type="text/javascript">
		var _smartsupp = _smartsupp || {};
		_smartsupp.key = 'fadc72b47e60b58e840384906cfe9eb2b6df6b6b';
		window.smartsupp||(function(d) {
			var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
			s=d.getElementsByTagName('script')[0];c=d.createElement('script');
			c.type='text/javascript';c.charset='utf-8';c.async=true;
			c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
		})(document);
	</script>
</head>

<body>

<div id="header">
	<div id="logo"><?php print CHtml::image(Yii::app()->baseUrl."/images/logo.png"); ?></div>
	<?php if(!Yii::app()->user->isGuest AND User::isPegawai()) { ?>
	<div id="profil"><a href="<?php print Yii::app()->controller->createUrl('pegawai/profil'); ?>">
		<strong><?php print Pegawai::getNamaByUserId(); ?></strong> (<?php print Pegawai::getJabatanByUserId(); ?>)
	</a></div>
	<?php } ?>
</div>

<div id="mainnav">
<?php if(User::isAdmin()) { ?>
<?php $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'items' => array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'encodeLabel'=>false,
					'htmlOptions'=>array('class'=>'pull-left'),
					'items' => array(
						array('label'=>'Home','icon'=>'home', 'url' => array('site/index')),
						array('label'=>'Surat','icon'=>'envelope', 'url' => array('surat/admin'),'items'=>array(
							array('label'=>'Data Surat','url'=>array('surat/admin')),
							array('label'=>'Jenis Surat','url'=>array('suratJenis/index')),
							array('label'=>'Kop Surat','url'=>array('suratKop/admin')),
						)),
						array('label'=>'Jabatan', 'url'=>array('/jabatan/index'),'icon'=>'asterisk','items'=>array(
							array('label'=>'Struktur Jabatan','url'=>array('jabatan/index')),
							array('label'=>'Tata Usaha','url'=>array('tataUsaha/index')),
							array('label'=>'Penerbit Surat','url'=>array('penerbit/index')),
						)),
						array('label'=>'Pegawai', 'url'=>array('/pegawai/admin'),'icon'=>'user'),
						array('label'=>'Distribusi','icon'=>'share-alt','items'=>array(
							array('label'=>'Data Distribusi','url'=>array('distribusi/admin')),
							array('label'=>'Grup Distribusi','url'=>array('distribusiGrup/admin')),
						)),
						array('label'=>'Laporan', 'url'=>array('/surat/laporan'),'icon'=>'file'),
						array('label'=>'Log', 'url'=>array('/log/admin'),'icon'=>'list'),
					)
				),
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'encodeLabel' => false,
					'htmlOptions'=>array('class'=>'pull-right navbar-right'),
					'items' => array(
						array('label' => 'User','icon'=>'user','items'=>array(
							array('label'=>'<i class="glyphicon glyphicon-user"></i> Profil','url'=>array('user/profil')),
							array('label'=>'<i class="glyphicon glyphicon-lock"></i> Ganti Password','url'=>array('user/gantiPassword')),
							array('label'=>'<i class="glyphicon glyphicon-off"></i> Logout','url'=>array('site/logout')),
						)),
					)
				)
			)	
)); ?>
<?php } ?>

<?php if(User::isPegawai()) { ?>
<?php $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'items' => array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'encodeLabel'=>false,
					'htmlOptions'=>array('class'=>'pull-left'),
					'items' => array(
						array('label' => 'Home','icon'=>'home', 'url' => array('site/index')),
						array('label' => 'Surat ('.Pegawai::countDistribusiBaruByJenis(1).')','icon'=>'envelope', 'url' => array('pegawai/distribusi','id_distribusi_jenis'=>1)),
						array('label' => 'Tembusan ('.Pegawai::countDistribusiBaruByJenis(2).')','icon'=>'tags', 'url' => array('pegawai/distribusi','id_distribusi_jenis'=>2)),
						array('label' => 'Disposisi ('.Pegawai::countDistribusiBaruByJenis(3).')','icon'=>'arrow-down', 'url' => array('pegawai/distribusi','id_distribusi_jenis'=>3)),
						array('label' => 'Terusan ('.Pegawai::countDistribusiBaruByJenis(4).')','icon'=>'arrow-right', 'url' => array('pegawai/distribusi','id_distribusi_jenis'=>4)),
						array('label' => 'Konfirmasi ('.(Paraf::countByUserId()+Pegawai::countSuratSiapTerbit()).')','icon'=>'search', 'url' => array('pegawai/paraf'),'items'=>array(
							array('label'=>'<i class="glyphicon glyphicon-search"></i> Paraf Surat ('.Paraf::countByUserId().')','url'=>array('pegawai/paraf')),
							array('label'=>'<i class="glyphicon glyphicon-ok"></i> Terbitkan Surat ('.Pegawai::countSuratSiapTerbit().')','url'=>array('pegawai/siapTerbit')),
						)),
						array('label' => 'Siap Terbit ('.Pegawai::countSuratSiapTerbit().')','icon'=>'ok', 'url' => array('pegawai/siapTerbit'),'visible'=>User::isPenerbit()),
						array('label' => 'Draf','icon'=>'pencil', 'url' => array('pegawai/draf'),'items'=>array(
							array('label'=>'<i class="glyphicon glyphicon-plus"></i> Buat Draf','url'=>array('surat/create')),
							'---',
							array('label'=>'<i class="glyphicon glyphicon-pencil"></i> Konsep ('.Pegawai::countSuratByStatus(2).')','url'=>array('pegawai/draf','status'=>'konsep')),
							array('label'=>'<i class="glyphicon glyphicon-search"></i> Pemeriksaan ('.Pegawai::countSuratByStatus(3).')','url'=>array('pegawai/draf','status'=>'pemeriksaan')),
							array('label'=>'<i class="glyphicon glyphicon-refresh"></i> Perbaikan ('.Pegawai::countSuratByStatus(4).')','url'=>array('pegawai/draf','status'=>'perbaikan')),
							array('label'=>'<i class="glyphicon glyphicon-ok"></i> Disetujui ('.Pegawai::countSuratByStatus(5).')','url'=>array('pegawai/draf','status'=>'disetujui')),
							array('label'=>'<i class="glyphicon glyphicon-envelope"></i> Terbit ('.Pegawai::countSuratByStatus(1).')','url'=>array('pegawai/draf','status'=>'terbit')),
						)),
						array('label' => 'Buat Draf','icon'=>'plus', 'url' => array('surat/create')),
					)
				),
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'encodeLabel' => false,
					'htmlOptions'=>array('class'=>'pull-right navbar-right'),
					'items' => array(
						array('label' => 'User','icon'=>'user','items'=>array(
							array('label'=>'<i class="glyphicon glyphicon-user"></i> Profil','url'=>array('pegawai/profil')),
							array('label'=>'<i class="glyphicon glyphicon-lock"></i> Ganti Password','url'=>array('pegawai/gantiPassword')),
							array('label'=>'<i class="glyphicon glyphicon-off"></i> Logout','url'=>array('site/logout')),
						)),
						array(
							'label' => '('.Pegawai::countPemberitahuanBaru().')',
							'icon'=>'bullhorn',
							'items'=>Pegawai::getMenuPemberitahuanBaru()
						)
					)
				)
			)	
)); ?>
<?php } ?>


</div>
	
<div id="content">

		<?php echo $content; ?>

</div><!-- page -->


<div>&nbsp;</div>

<div id="footer" style="padding:20px;text-align:center">
	Copyright &copy; 2015 Badan Keamanan Laut Republik Indonesia
</div>



</body>
</html>
