<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="container">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="col-xs-12">
				<?php if(isset($this->breadcrumbs)):?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
					)); ?><!-- breadcrumbs -->
				<?php endif?>
			</div>
		</div>
		
		<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>	
		<div class="row-fluid">
			<div class="col-xs-12">
				<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<?php } ?>

		<div class="row-fluid">
			<div class="col-xs-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div><!-- content -->

<?php $this->endContent(); ?>