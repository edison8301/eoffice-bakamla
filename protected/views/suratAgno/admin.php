<?php
$this->breadcrumbs=array(
	'Surat Agnos'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Agno</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-agno-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered condensed',
		'filter'=>$model,
		'columns'=>array(
			'nomor_agenda',
			'tanggal_surat',
			'asal_surat',
			'nomor_surat',
		/*
		'tanggal_surat',
		'tujuan_surat',
		'tembusan_surat',
		'perihal',
		'sehubungan',
		'catatan',
		'id_jabatan_pembuat',
		'username_pembuat',
		'waktu_dibuat',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Agno',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create')
	)); ?>



</div>

