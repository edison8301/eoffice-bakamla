<?php
$this->breadcrumbs=array(
	'Surat Agnos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List SuratAgno','url'=>array('index')),
	array('label'=>'Create SuratAgno','url'=>array('create')),
	array('label'=>'View SuratAgno','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SuratAgno','url'=>array('admin')),
	);
	?>

	<h1>Update SuratAgno <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>