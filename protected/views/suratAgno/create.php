<?php
$this->breadcrumbs=array(
	'Surat Agno'=>array('suratAgno/admin'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List SuratAgno','url'=>array('index')),
array('label'=>'Manage SuratAgno','url'=>array('admin')),
);
?>

<h1>Tambah Agno</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>