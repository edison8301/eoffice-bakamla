<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_surat')); ?>:</b>
	<?php echo CHtml::encode($data->id_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asal_surat')); ?>:</b>
	<?php echo CHtml::encode($data->asal_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_surat')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_surat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_surat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tujuan_surat')); ?>:</b>
	<?php echo CHtml::encode($data->tujuan_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tembusan_surat')); ?>:</b>
	<?php echo CHtml::encode($data->tembusan_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perihal')); ?>:</b>
	<?php echo CHtml::encode($data->perihal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sehubungan')); ?>:</b>
	<?php echo CHtml::encode($data->sehubungan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catatan')); ?>:</b>
	<?php echo CHtml::encode($data->catatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jabatan_pembuat')); ?>:</b>
	<?php echo CHtml::encode($data->id_jabatan_pembuat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username_pembuat')); ?>:</b>
	<?php echo CHtml::encode($data->username_pembuat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_dibuat')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_dibuat); ?>
	<br />

	*/ ?>

</div>