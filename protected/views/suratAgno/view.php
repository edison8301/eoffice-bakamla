<?php
$this->breadcrumbs=array(
	'Surat Agnos'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SuratAgno','url'=>array('index')),
array('label'=>'Create SuratAgno','url'=>array('create')),
array('label'=>'Update SuratAgno','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SuratAgno','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SuratAgno','url'=>array('admin')),
);
?>

<h1>View SuratAgno #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_surat',
		'nomor',
		'tanggal',
		'asal_surat',
		'nomor_surat',
		'tanggal_surat',
		'tujuan_surat',
		'tembusan_surat',
		'perihal',
		'sehubungan',
		'catatan',
		'id_jabatan_pembuat',
		'username_pembuat',
		'waktu_dibuat',
),
)); ?>
