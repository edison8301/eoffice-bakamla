<h2>Paraf</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Paraf',
		'icon'=>'plus',
		'url'=>array('paraf/create','id_surat'=>$model->id)
)); ?>



<div>&nbsp;</div>

<table class='table'>
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th>Jabatan</th>
	<th>Pemangku</th>
	<th>Status</th>
	<th>Waktu Disetujui</th>
	<th>&nbsp;</th>
</tr>
<?php foreach($model->findAllParaf() as $paraf) { ?>
<?php $class = ''; ?>
<?php if($paraf->id_paraf_status == 1) $class = "success"; ?>
<?php if($paraf->id_paraf_status == 3) $class = "info"; ?>
<?php if($paraf->id_paraf_status == 4) $class = "danger"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $paraf->urutan; ?></td>
	<td><?php print $paraf->getNamaJabatan(); ?></td>
	<td><?php print $paraf->getNamaPemangkuJabatan(); ?></div>
	<td>
		<?php print $paraf->getRelationField('paraf_status','nama'); ?>
	</td>
	<td><?php print $paraf->waktu_disetujui; ?></td>
	<td>
		<?php print CHtml::link("<i class='glyphicon glyphicon-search'></i>",array("paraf/view","id"=>$paraf->id)); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil'></i>",array("paraf/update","id"=>$paraf->id)); ?>
	</td>
</tr>
<?php } ?>
</table>
