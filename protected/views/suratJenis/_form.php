<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-jenis-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->select2Group($model,'penandatangan',array(
			'widgetOptions'=>array(
				'asDropDownList'=>false,
				'htmlOptions'=>array('class'=>'span5','maxlength'=>255),
				'options' => array(
					'tags' => array('Kepala', 'Sestama', 'Deputi','Kabag','Kepala UPH'),
					'placeholder' => 'Pilih penandatangan',
					'separator' => array(';')
				)
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'contoh',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textAreaGroup($model,'keterangan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
