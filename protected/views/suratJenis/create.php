<?php
$this->breadcrumbs=array(
	'Surat Jenises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SuratJenis','url'=>array('index')),
array('label'=>'Manage SuratJenis','url'=>array('admin')),
);
?>

<h1>Create SuratJenis</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>