<?php
$this->breadcrumbs=array(
	'Jenis Surat'=>array('index'),
	'Sunting',
);

$this->menu=array(
	array('label'=>'List SuratJenis','url'=>array('index')),
	array('label'=>'Create SuratJenis','url'=>array('create')),
	array('label'=>'View SuratJenis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SuratJenis','url'=>array('admin')),
);

?>

<h1>Sunting Jenis Surat</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>