<?php
$this->breadcrumbs=array(
	'Surat Jenises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SuratJenis','url'=>array('index')),
array('label'=>'Create SuratJenis','url'=>array('create')),
array('label'=>'Update SuratJenis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SuratJenis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SuratJenis','url'=>array('admin')),
);
?>

<h1>View SuratJenis #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
		'contoh',
		'keterangan',
),
)); ?>
