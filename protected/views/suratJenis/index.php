<?php
$this->breadcrumbs=array(
	'Jenis Surat',
);
?>

<h1>Jenis Surat</h1>

<div>&nbsp;</div>

<table class="table table-condensed table-hover">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="95%">Jenis Surat</th>
	<th>Penandantangan</th>
	<th>&nbsp;</th>
</tr>
</thead>
<?php $no=1; foreach(SuratJenis::model()->findAll() as $data) { ?>
<tr>
	<td><?php print $no; ?></td>
	<td><?php print $data->nama; ?></td>
	<td><?php print $data->penandatangan; ?>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('suratJenis/update','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Sunting')); ?>
	</td>
</tr>
<?php $no++; } ?>
</table>