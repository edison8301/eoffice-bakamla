<?php

$this->pageTitle=Yii::app()->name;

$this->breadcrumbs = array(
		'Daftar Login'
	)

?>

<h1>Riwayat Login</h1>

<div>&nbsp;</div>

<table class="table table-condensed">
<thead>
<tr>
    <th width="5%">No</th>
    <th width="25%">Nama</th>
    <th width="40%">Jabatan</th>
    <th width="30%">Terakhir Login</th>
</tr>
</thead>
<?php
	
	if($_GET['waktu']=='hari-ini')
	{
		$model = Pegawai::findAllLoginHariIni();
	}

	if($_GET['waktu']=='minggu-ini')
	{
		$model = Pegawai::findAllLoginMingguIni();
	}

	if($_GET['waktu']=='bulan-ini')
	{
		$model = Pegawai::findAllLoginBulanIni();
	}

	if($_GET['waktu']=='tahun-ini')
	{
		$model = Pegawai::findAllLoginTahunIni();
	}

?>

<?php $i=1; foreach($model as $pegawai) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print CHtml::link($pegawai->nama,array("pegawai/view","id"=>$pegawai->id)); ?></td>
	<td><?php print $pegawai->getRelation('jabatan','nama'); ?></td>
	<td><?php print Helper::getHariTanggalPukul($pegawai->login_terakhir); ?></td>
</tr>
<?php $i++; } ?>
</table>