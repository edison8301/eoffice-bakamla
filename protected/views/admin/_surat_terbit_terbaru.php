<table class="table table-bordered table table-striped">
	<tr>
		<th>Nomor Surat</th>
		<th>Jenis Surat</th>
		<th>Waktu Diterbitkan</th>
	</tr>
	<?php  $i=1; foreach(Surat::model()->findAllByAttributes(array('id_surat_status'=>1),array('order' => 'waktu_diterbitkan DESC')) as $data) {
		if ($i++ == 11) break;
	?>
	<tr>
		<td> <i class="glyphicon glyphicon-envelope"></i> <?= CHtml::link($data->nomor,array('surat/view','id'=>$data->id)); ?> </td>
		<td> <?= $data->getRelationField('surat_jenis','nama'); ?></td>
		<td> <?= Helper::getTanggalWaktu($data->waktu_diterbitkan); ?> </td>
	</tr>
	<?php  } ?>
</table>