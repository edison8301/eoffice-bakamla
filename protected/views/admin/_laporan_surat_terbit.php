<div class="row">
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah draf surat yang terbit hari ini">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Hari Ini</div>
			<div class="content"><?php print Surat::countTerbitHariIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah draf surat terbit minggu ini">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Minggu Ini</div>
			<div class="content"><?php print Surat::countTerbitMingguIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah draf surat yang terbit bulan ini">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Bulan Ini</div>
			<div class="content"><?php print Surat::countTerbitBulanIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah draf surat yang harus terbit tahun ini">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Tahun Ini</div>
			<div class="content"><?php print Surat::countTerbitTahunIni(); ?></div>
		</div>
		</a>
	</div>
</div>