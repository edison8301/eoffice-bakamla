<div class="row">
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah surat yang didistribusikan hari ini">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-share-alt"></i></div>
			<div class="title"> Hari Ini</div>
			<div class="content"><?php print Distribusi::countHariIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah surat yang didistribusikan minggu ini">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-share-alt"></i></div>
			<div class="title"> Minggu Ini</div>
			<div class="content"><?php print Distribusi::countMingguIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah surat yang didistribusikan bulan ini">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-share-alt"></i></div>
			<div class="title"> Bulan Ini</div>
			<div class="content"><?php print Distribusi::countBulanIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="#" data-toggle="tooltip" title="Jumlah surat yang didistribusikan tahun ini">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-share-alt"></i></div>
			<div class="title"> Tahun Ini</div>
			<div class="content"><?php print Distribusi::countTahunIni(); ?></div>
		</div>
		</a>
	</div>
</div>