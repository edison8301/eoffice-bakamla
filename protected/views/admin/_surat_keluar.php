<table class="table" width="100%">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="35%">Jenis Surat</th>
	<th width="20%">Pembuat</th>
	<th width="20%" style="text-align:center">Status</th>
	<th width="15%">Waktu Dibuat</th>
	<th width="5%" style="text-align:center">Action</th>
</tr>
</thead>
<?php $i=1; foreach(Pegawai::model()->findAllSuratKeluarByStatus($status) as $surat) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $surat->getRelationField("surat_keluar_jenis","nama"); ?></td>
	<td>
		<?php $this->widget('booster.widgets.TbLabel',array(
				'context' => 'success', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label' => $surat->username_pembuat
		)); ?>
	</td>
	<td style="text-align:center">
		<?php $this->widget('booster.widgets.TbLabel',array(
				'context' => 'primary', // 'success', 'warning', 'important', 'info' or 'inverse'
				'label' => $surat->getRelationField("surat_keluar_status","nama")
		)); ?>
	</td>
	<td>
		<?php print Helper::getTanggalJam($surat->waktu_dibuat); ?>
	</td>
	<td style="text-align:center">
		<?php print CHtml::link("<i class='glyphicon glyphicon-eye-open'></i>",array("suratKeluar/view","id"=>$surat->id)); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>	