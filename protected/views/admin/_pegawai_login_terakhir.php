<table class="table table-bordered table table-striped">
	<tr>
		<th>Akun</th>
		<th>Waktu Login</th>
	</tr>
	<?php  $i=1; foreach(Pegawai::model()->findAll(array('order' => 'login_terakhir DESC')) as $data) {
		if ($i++ == 11) break;
	?>
	<tr>
		<td> <i class="glyphicon glyphicon-user"></i> <?= CHtml::link($data->nama,array('pegawai/view','id'=>$data->id)); ?> </td>
		<td> <?= Helper::getTanggalWaktu($data->login_terakhir); ?> </td>
	</tr>
	<?php  } ?>
</table>