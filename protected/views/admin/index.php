<?php

$this->pageTitle=Yii::app()->name;

$this->breadcrumbs = array(
		'Dashboard'
	)

?>

<h1>Dashboard Admin</h1>

<hr>

<h3>Pegawai Login</h3>

<?php $this->renderPartial('_laporan_login'); ?>

<hr>

<h3>Surat Terbit</h3>

<?php $this->renderPartial('_laporan_surat_terbit'); ?>

<hr>

<h3>Distribusi Surat</h3>

<?php $this->renderPartial('_laporan_distribusi'); ?>

<hr>

<div>&nbsp;</div>

<div class="row">
	<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Surat Terbit per Jenis',
        		'context'=>'primary',
        		'headerIcon' => 'adjust',
        		'content' => $this->renderPartial('_grafik_surat_per_jenis',array(),true)
    	)); ?>
    </div>
    <div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Surat Terbit per Bulan',
        		'context' => 'primary',
        		'headerIcon' => 'signal',
        		'content' => $this->renderPartial('_grafik_surat_per_bulan',array(),true)
    	)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php $this->widget('booster.widgets.TbPanel',array(
                'title' => 'Surat Terbit Terbaru',
                'context'=>'primary',
                'headerIcon' => 'envelope',
                'content' => $this->renderPartial('_surat_terbit_terbaru',array(),true)
        )); ?>
    </div>
    <div class="col-md-6">
        <?php $this->widget('booster.widgets.TbPanel',array(
                'title' => 'Akun Login Terbaru',
                'context' => 'primary',
                'headerIcon' => 'user',
                'content' => $this->renderPartial('_pegawai_login_terakhir',array(),true)
        )); ?>
    </div>
</div>

