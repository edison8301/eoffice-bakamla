<div class="row">
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("admin/daftarLogin",array("waktu"=>"hari-ini")); ?>" data-toggle="tooltip" title="Jumlah pegawai yang login hari ini">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-user"></i></div>
			<div class="title"> Hari Ini</div>
			<div class="content"><?php print Pegawai::countLoginHariIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("admin/daftarLogin",array("waktu"=>"minggu-ini")); ?>" data-toggle="tooltip" title="Jumlah pegawai yang login minggu ini">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-user"></i></div>
			<div class="title"> Minggu Ini</div>
			<div class="content"><?php print Pegawai::countLoginMingguIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("admin/daftarLogin",array("waktu"=>"bulan-ini")); ?>" data-toggle="tooltip" title="Jumlah pegawai yang login bulan ini">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-user"></i></div>
			<div class="title"> Bulan Ini</div>
			<div class="content"><?php print Pegawai::countLoginBulanIni(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("admin/daftarLogin",array("waktu"=>"tahun-ini")); ?>" data-toggle="tooltip" title="Jumlah pegawai yang login tahun ini">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-user"></i></div>
			<div class="title"> Tahun Ini</div>
			<div class="content"><?php print Pegawai::countLoginTahunIni(); ?></div>
		</div>
		</a>
	</div>
</div>