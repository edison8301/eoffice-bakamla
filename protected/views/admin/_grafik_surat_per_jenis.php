<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>


<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "column3d",
        "renderAt": "grafik-surat-per-jenis",
        "width": "100%",
        "height": "350",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Persentase Surat Per Jenis",
              "xAxisName": "Jenis",
              "yAxisName": "Jumlah Surat",
              "formatNumberScale" : 0,
              "theme": "fint"
           },
          "data":        
                [ <?php print Surat::getDataGrafikSuratPerJenis(); ?> ], 
        }
    });

    revenueChart.render();
})
    
</script>
 
  <div id="grafik-surat-per-jenis" style="text-align:center">Grafik Surat Per Jenis</div>

