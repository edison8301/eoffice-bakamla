<?php
$this->breadcrumbs=array(
	$model->getJenis()=>array('pegawai/distribusi','id_distribusi_jenis'=>$model->id_distribusi_jenis),
	'Detil',
);

?>

<h1 id="detil">Detail <?php print $model->getJenis(); ?></h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'Pengirim',
				'value'=>$model->getPengirim()
			),
			array(
				'label'=>'Penerima',
				'value'=>$model->getPenerima()
			),
			'catatan',
			array(
				'label'=>'Waktu Dilihat',
				'value'=>Helper::getCreatedTime($model->waktu_dilihat)
			),
			array(
				'label'=>'Waktu Pengiriman',
				'value'=>Helper::getCreatedTime($model->waktu_dibuat)
			)
		),
)); ?>

<?php $surat = $model->findSurat(); ?>

<?php if($surat!==null) { ?>

<?php //$this->renderPartial('//surat/_view',array('model'=>$surat)); ?>

<div>&nbsp;</di>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Disposisikan Surat',
		'icon'=>'arrow-down',
		'size'=>'small',
		'url'=>array('distribusi/create','id_surat'=>$surat->id,'id_distribusi_jenis'=>3),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Teruskan Surat',
		'icon'=>'arrow-right',
		'size'=>'small',
		'url'=>array('distribusi/create','id_surat'=>$surat->id,'id_distribusi_jenis'=>4),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Detail Surat',
		'icon'=>'search',
		'size'=>'small',
		'url'=>array('surat/view','id'=>$surat->id),
)); ?>&nbsp;
</div>


<?php $this->renderPartial('//surat/_pratinjau',array('model'=>$surat)); ?>

<?php //$this->renderPartial('//surat/_lampiran',array('model'=>$surat)); ?>

<?php } ?>


<hr>

<h3 id="catatan"><i class="glyphicon glyphicon-bullhorn"></i> Catatan/Respon</h3>

<?php $this->renderPartial('//catatan/_display',array('model'=>$model)); ?>

<div>&nbsp;</div>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>
	
	<?php echo $form->errorSummary($model); ?>

	<div class="well">
		
		<?php echo $form->textAreaGroup($catatan,'catatan',array(
				'widgetOptions'=>array('htmlOptions'=>array(
					'rows'=>'3'
				))
		)); ?>

		<div class="form-group">
			<label class="col-sm-3 control-label">Lampiran</label>
			<div class="col-sm-9">
				<?php $this->widget('CMultiFileUpload', array(
						'name'=>'lampirans',
     					'accept'=>'jpg|gif|pdf|png',
     					'duplicate' => 'Duplicate file!', // useful, i think
                		'denied' => 'Tipe file ditolak',
  				)); ?>
  			</div>
  		</div>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'bullhorn',
			'label'=>'Kirim catatan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>





<div id="shortcut">
	<a class="btn btn-primary btn-sm" href="#detil"><i class="glyphicon glyphicon-file"></i> Detil</a>
    <a class="btn btn-success btn-sm" href="#surat"><i class="glyphicon glyphicon-envelope"></i> Surat</a>
    <a class="btn btn-danger btn-sm" href="#lampiran"><i class="fa fa-paperclip"></i> Lampiran</a>
    <a class="btn btn-warning btn-sm" href="#catatan"><i class="glyphicon glyphicon-bullhorn"></i> catatan</a>
    
</div>

<style>
#shortcut {
 	position: fixed;
    padding: 5px 5px;
    background: #EEEEEE;
    bottom: 20px;
    right: 20px;
    border:1px solid #999999;
}
</style>