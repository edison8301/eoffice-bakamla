<?php
$this->breadcrumbs=array(
	'Disposisis'=>array('index'),
	$model->id,
);

?>

<h1>Distribusi Surat</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'Jabatan Pengirim',
				'value'=>$model->getRelation("jabatan_pengirim","nama_jabatan")
			),
			array(
				'label'=>'Sifat',
				'value'=>$model->getRelationField('disposisi_sifat','nama')
			),
			array(
				'label'=>'Tindakan',
				'value'=>$model->tindakan
			),
			'catatan',
			array(
				'label'=>'Waktu Dilihat',
				'value'=>Helper::getCreatedTime($model->waktu_dilihat)
			),
			array(
				'label'=>'Waktu Dikirim',
				'value'=>Helper::getCreatedTime($model->waktu_dibuat)
			)
		),
)); ?>

<div>&nbsp;</div>

<h3>Isi Memo</h3>

<?php $memo = $model->findMemo(); ?>

<?php if($memo!==null) { ?>

<?php $box = $this->beginWidget('booster.widgets.TbPanel',array(
        'title' => $memo->perihal,
        'context'=>'primary',
        'headerIcon' => 'envelope',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'bootstrap-widget-table')
));?>



<div style="padding:20px">
	<?php print $memo->isi; ?>
</div>

<?php $this->endWidget(); ?>

<?php } ?>


<div>&nbsp;</div>
