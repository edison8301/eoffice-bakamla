<?php
$this->breadcrumbs=array(
	'Distribusi'=>array('admin'),
	'Kelola',
);

?>

<h1>Kelola Distribusi</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'distribusi-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type'=>'striped bordered condensed',
		'columns'=>array(
			array(
				'header'=>'No Surat',
				'name'=>'id_surat',
				'value'=>'$data->getRelation("surat","nomor")',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'header'=>'Pengirim',
				'name'=>'pengirim',
				'value'=>'$data->getPengirim()',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'header'=>'Penerima',
				'name'=>'pengirim',
				'value'=>'$data->getPenerima()',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'header'=>'Penerima',
				'name'=>'username_penerima',
				'value'=>'$data->username_penerima',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'name'=>'waktu_dibuat',
				'header'=>'Waktu Pengiriman',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dibuat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:20%;text-align:center')
			),
			array(
				'name'=>'waktu_dilihat',
				'header'=>'Waktu Dilihat',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dilihat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:20%;text-align:center')
			),
			array(
				'name'=>'id_distribusi_jenis',
				'value'=>'$data->getJenis()',
				'filter'=>CHtml::listData(DistribusiJenis::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'text-align:center;width:5%')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view} {delete}',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:10%;text-align:center')
			),
		),
)); ?>