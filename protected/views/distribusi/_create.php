<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'disposisi-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">

			<?php echo $form->hiddenField($model,'id_surat'); ?>
			<?php echo $form->hiddenField($model,'id_distribusi_jenis'); ?>

			<?php echo $form->select2Group($model,'list_penerima',array(
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-12',
					),
					'widgetOptions' => array(
						'asDropDownList' => false,
						'options' => array(
							'tags' => Surat::model()->getListAutoCompleteDisposisi(),
							'placeholder' => 'Ketik Nama Jabatan/Pegawai',
							'separator' => ';',
							'minimumInputLength'=>2,
						)
					)
			)); ?>

			<?php echo $form->textAreaGroup($model,'catatan',array('widgetOptions'=>array('htmlOptions'=>array('rows'=>'5','maxlength'=>255)))); ?>
	
	</div><!-- .well -->

	<div class="form-actions well" style="text-align:right">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'share-alt',
			'label'=>'Proses',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
