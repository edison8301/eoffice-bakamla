<?php

if($model->id_distribusi_jenis == 2)
{
	$this->breadcrumbs=array(
		'Disposisi'=>array('pegawai/distribusi','id_distribusi_jenis'=>2),
		'Kirim Disposisi',
	);
}

if($model->id_distribusi_jenis == 3)
{
	$this->breadcrumbs=array(
		'Pegawai'=>array('pegawai/profil'),
		'Kirim Disposisi',
	);
}

if($model->id_distribusi_jenis == 4)
{
	$this->breadcrumbs=array(
		'Pegawai'=>array('pegawai/profil'),
		'Kirim Terusan',
	);
}

?>

<h1>Kirim <?php print $model->getJenis(); ?></h1>

<?php echo $this->renderPartial('_create', array('model'=>$model)); ?>