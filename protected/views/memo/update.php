<?php
$this->breadcrumbs=array(
	'Memos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Memo','url'=>array('index')),
	array('label'=>'Create Memo','url'=>array('create')),
	array('label'=>'View Memo','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Memo','url'=>array('admin')),
	);
	?>

<h1>Sunting Memo</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>