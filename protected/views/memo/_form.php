
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'memo-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">

	<?php echo $form->textFieldGroup($model,'perihal',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php $ckeditor = 'js:[
    	  	{ name: "basicstyles", items : [ "Bold","Italic","Underline","Strike"] },
    	  	{ name: "paragraph", items : [ "NumberedList","BulletedList","-","Outdent","Indent" ] },
        	{ name: "clipboard", items : [ "Undo","Redo" ] },
       		{ name: "styles", items : [ "Format" ] }    
 	]'; ?>
	<?php echo $form->ckEditorGroup($model,'isi',array(
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5'),
				'editorOptions'=>array(
					'toolbar'=>$ckeditor
				)
			),
			
	)); ?>

	</div>
	
	<div class="well" style="text-align:right">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'size'=>'small',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>