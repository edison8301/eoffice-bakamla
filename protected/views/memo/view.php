<?php
$this->breadcrumbs=array(
	'Memo'=>array('index'),
	$model->id,
);
?>

<h1>Lihat Memo</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'pembuat',
				'value'=>$model->getNamaPembuat().' ('.$model->getNamaJabatanPembuat().')'
			),
			array(
				'label'=>'Isi Memo',
				'type'=>'raw',
				'value'=>$model->isi
			),
			array(
				'label'=>'Waktu Dibuat',
				'value'=>Helper::getCreatedTime($model->waktu_dibuat)
			)
		),
)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Distribusikan Memo',
		'icon'=>'share-alt',
		'size'=>'small',
		'url'=>array('disposisi/create','id_model'=>$model->id,'model'=>'Memo'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting Memo',
		'icon'=>'pencil',
		'size'=>'small',
		'url'=>array('udpate','id_model'=>$model->id,'model'=>'SuratKeluar'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Memo',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create'),
)); ?>&nbsp;
</div>

<h3>Distribusi Memo</h3>

<table class="table table-hover table-condensed">
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th width="30%">Jabatan Penerima</th>
	<th width="25%">Nama Penerima</th>
	<th width="20%" style="text-align:center">Waktu Pengiriman</th>
	<th width="20%" style="text-align:center">Waktu Dilihat</th>
</tr>
<?php $i=1; foreach($model->findAllDisposisi() as $data) { ?>
<?php $class = ""; ?>
<?php if($data->waktu_dilihat==null) $class = "danger"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $i; ?></td>
	<td><?php print Jabatan::getNamaJabatanById($data->id_jabatan_penerima); ?></td>
	<td><?php print Pegawai::getNamaByUsername($data->username_penerima); ?></td>
	<td style="text-align:center"><?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></td>
	<td style="text-align:center"><?php print $data->waktu_dilihat; ?></td>
</tr>
<?php $i++; } ?>
</table>