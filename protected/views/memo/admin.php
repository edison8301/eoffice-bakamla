<?php
$this->breadcrumbs=array(
	'Memos'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Memo','url'=>array('index')),
array('label'=>'Create Memo','url'=>array('create')),
);

?>

<h1>Kelola Memo</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'memo-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),	
		'filter'=>$model,
		'columns'=>array(
			
			array(
				'name'=>'perihal',
				'type'=>'raw',
				'value'=>'$data->perihal'
			),
			array(
				'name'=>'id_jabatan_pembuat',
				'header'=>'Jabatan Pembuat',
				'type'=>'raw',
				'value'=>'$data->getRelation("jabatan_pembuat","nama_jabatan")'
			),
			array(
				'name'=>'waktu_dibuat',
				'header'=>'Waktu Dibuat',
				'type'=>'raw',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dibuat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'size'=>'small',
		'label'=>'Buat Memo Baru',
		'icon'=>'plus',
		'url'=>array('create')
)); ?>
</div>