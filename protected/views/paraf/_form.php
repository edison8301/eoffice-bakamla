<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'paraf-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'id_surat'); ?>

	<?php echo $form->textFieldGroup($model,'urutan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_jabatan',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama_jabatan'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
	)); ?>
	</div>

<?php $this->endWidget(); ?>
