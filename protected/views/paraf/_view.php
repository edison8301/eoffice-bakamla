<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_surat')); ?>:</b>
	<?php echo CHtml::encode($data->id_surat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('urutan')); ?>:</b>
	<?php echo CHtml::encode($data->urutan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->id_jabatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_paraf_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_paraf_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_dilihat')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_dilihat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('waktu_disetujui')); ?>:</b>
	<?php echo CHtml::encode($data->waktu_disetujui); ?>
	<br />


</div>