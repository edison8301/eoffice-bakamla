<?php
$this->breadcrumbs=array(
	'Parafs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Paraf','url'=>array('index')),
	array('label'=>'Create Paraf','url'=>array('create')),
	array('label'=>'View Paraf','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Paraf','url'=>array('admin')),
	);
	?>

	<h1>Update Paraf <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>