<?php
$this->breadcrumbs=array(
	'Parafs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Paraf','url'=>array('index')),
array('label'=>'Manage Paraf','url'=>array('admin')),
);
?>

<h1>Tambah Paraf</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>