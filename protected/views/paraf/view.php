<?php
$this->breadcrumbs=array(
	'Parafs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Paraf','url'=>array('index')),
array('label'=>'Create Paraf','url'=>array('create')),
array('label'=>'Update Paraf','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Paraf','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Paraf','url'=>array('admin')),
);
?>

<h1>Lihat Paraf</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Lihat Surat',
		'icon'=>'eye-open',
		'url'=>array('suratKeluar/view','id'=>$model->id_surat)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'urutan',
			array(
				'label'=>'Jabatan',
				'value'=>$model->getNamaJabatan()
			),
			array(
				'label'=>'Status',
				'value'=>$model->getRelationField("paraf_status","nama")
			),
			'waktu_dilihat',
			'waktu_disetujui',
	),
)); ?>


<h2>Komentar</h2>

<div>&nbsp;</div>

<?php foreach($model->findAllParafKomentar() as $data) { ?>
<div class="row" style="margin-bottom:20px">
	<div class="col-md-2">
		<div><?php print $data->pembuat; ?></div>
		<div><?php print $data->waktu_dibuat; ?></div>
	</div>
	<div class="col-md-10">
		<?php print $data->komentar; ?>
	</div>
</div>
<?php } ?>

<?php if($model->id_paraf_status != 1) { ?>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<?php echo $form->hiddenField($paraf_komentar,'id_paraf'); ?>

	<?php echo $form->hiddenField($paraf_komentar,'pembuat'); ?>

	<?php echo $form->textAreaGroup($paraf_komentar,'komentar',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Kirim Komentar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php } ?>
