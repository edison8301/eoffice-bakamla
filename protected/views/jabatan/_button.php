<?php $this->widget('booster.widgets.TbButtonGroup',array(
        'context'=>'primary',
        'size'=>'small',
       	'encodeLabel'=>false,
        'buttons' => array(
            array(
                'label' => ' ',
                'items' => array(
                    array('label' =>'<i class="glyphicon glyphicon-arrow-up"></i> Atasan', 'url' => array('jabatan/index','id'=>$jabatan->id_induk)),
                    '---',
                    array('label' =>'<i class="glyphicon glyphicon-pencil"></i> Sunting', 'url' => array('jabatan/update','id'=>$jabatan->id,'id_kembali'=>$_GET['id'])),
                    array('label' => '<i class="glyphicon glyphicon-plus"></i> Subjabatan', 'url' => array('jabatan/create','id_induk'=>$jabatan->id,'id_kembali'=>$_GET['id'])),
                )
            ),
        ),
)); ?>