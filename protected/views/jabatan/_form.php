
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'jabatan-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('style'=>'width:100%')
)); ?>

<?php echo $form->errorSummary($model); ?>
	
	<div class="well">

	<?php echo $form->select2Group($model,'id_induk',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-6'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('class'=>'col-xs-12','empty'=>'-- Pilih Atasan --')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'nama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'singkatan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_jabatan_jenis',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(JabatanJenis::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>

	<?php echo $form->dropDownListGroup($model,'id_jabatan_eselon',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(JabatanEselon::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('empty'=>'-- Pilih Eselon --')
			)
	)); ?>

	</div>
	
	<div class="form-actions well" style="text-align:right">
			<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Save',
			)); ?>
	</div>


<?php $this->endWidget(); ?>
