<?php $level++; ?>
<tr>
	<td style="padding-left:<?php print $level*35; ?>px">
		<?php $this->renderPartial('_button',array('jabatan'=>$jabatan)); ?>
		<?php print CHtml::link($jabatan->nama,array("jabatan/index","id"=>$jabatan->id)); ?>
		(<?php print $jabatan->singkatan; ?>)		
	</td>
	<td style="text-align:center"><?php print $jabatan->getRelationField("jabatan_jenis","nama"); ?></td>
	<td style="text-align:center"><?php print $jabatan->getRelationField("jabatan_eselon","nama"); ?></td>
	<td>
		<ul id="pemangku">
		<?php foreach($jabatan->findAllPegawai() as $data) { ?>
		<li><?php print CHtml::link($data->nama,array('pegawai/view','id'=>$data->id)); ?></li>
		<?php }  ?>
		</ul>
	</td>
	<td style="text-align:center"><?php print $jabatan->countPegawai(); ?></td>
</tr>
<?php foreach($jabatan->findAllSubjabatan() as $subjabatan) { ?>
<?php $this->renderPartial('_subindex',array('jabatan'=>$subjabatan,'level'=>$level)); ?>
<?php } ?>