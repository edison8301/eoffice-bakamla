<?php
$this->breadcrumbs=array(
	'Jabatan',
);

$this->menu=array(
array('label'=>'Create Jabatan','url'=>array('create')),
array('label'=>'Manage Jabatan','url'=>array('admin')),
);
?>

<h1>Kelola Jabatan</h1>

<div>&nbsp;</div>

<div class="row">
	<div class="col-md-12">

<table class="table table-striped table-hover table-condensed" width="100%" style="width:100%">
<thead>
<tr>
	<th width="60%">Jabatan</th>
	<th width="10%" style="text-align:center">Jenis</th>
	<th width="5%" style="text-align:center">Eselon</th>
	<th width="20%">Nama Pemangku</th>
	<th width="5s%" style="text-align:center">Jml</th>
</tr>
</thead>
<?php $level=0; $i=1; ?>
<tr>
	<td>
		<?php $this->renderPartial('_button',array('jabatan'=>$jabatan)); ?>
		<?php print CHtml::link($jabatan->nama,array("jabatan/index","id"=>$jabatan->id)); ?>
	</td>
	<td style="text-align:center"><?php print $jabatan->getRelationField("jabatan_jenis","nama"); ?></td>
	<td style="text-align:center"><?php print $jabatan->getRelationField("jabatan_eselon","nama"); ?></td>
	<td>
		<?php foreach($jabatan->findAllPegawai() as $data) { ?>
		<?php print CHtml::link($data->nama,array('pegawai/view','id'=>$data->id)); ?>
		<?php }  ?>
	</td>
	<td style="text-align:center"><?php print $jabatan->countPegawai(); ?></td>
</tr>
<?php foreach($jabatan->findAllSubjabatan() as $subjabatan) { ?>
<?php $this->renderPartial('_subindex',array('jabatan'=>$subjabatan,'level'=>$level)); ?>
<?php } ?>
<?php $i++; ?>
</table>

</div>
</div>
<div>&nbsp;</div>
