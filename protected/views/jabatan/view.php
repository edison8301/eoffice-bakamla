<?php

$this->breadcrumbs=array(
	'Jabatan'=>array('index'),
	$model->nama,
);

?>

<h1>Lihat Jabatan</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama',
			array(
				'label'=>'Pemangku',
				'value'=>$model->getNamaPemangku()
			),
			array(
				'label'=>'Jenis Jabatan',
				'value'=>$model->getRelationField("jabatan_jenis","nama")
			),
			array(
				'label'=>'Eselon',
				'value'=>$model->getRelationField("jabatan_eselon","nama")
			),
			
		),
)); ?>

<div>&nbsp;</div>

<div class="well">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'url'=>array('jabatan/update','id'=>$model->id)
	)); ?>&nbsp;
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'label'=>'Tambah',
			'icon'=>'plus',
			'url'=>array('jabatan/create')
	)); ?>&nbsp;
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'label'=>'Kelola',
			'icon'=>'list',
			'url'=>array('jabatan/index')
	)); ?>&nbsp;
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'success',
			'label'=>'Tambah Staf',
			'icon'=>'user',
			'htmlOptions'=>array(
				'onclick'=>'$("#dialogStaf").dialog("open"); return false;'
			)
	)); ?>
</div>

<h3>Daftar Staf Langsung</h3>

<table class="table">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="50%">Nama</th>
	<th width="45%">NIP</th>
</tr>
</thead>
<?php $i=1; foreach($model->findAllStaf() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->nama; ?></td>
	<td><?php print $data->nip; ?></td>
</tr>
<?php $i++; } ?>
</table>

<?php $this->id_jabatan = $model->id; ?>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogStaf',
    'options'=>array(
        'title'=>'Daftar Pegawai',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
)); ?>
	
   	<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped hover condensed',
		'dataProvider'=>Pegawai::model()->dialogStaf(),
		'filter'=>Pegawai::model(),
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:left',
				),
			),
			array(
		      	'name'=>'nama',
		      	'value'=>'$data->nama',
			),
			array(
				'name'=>'id_jabatan_induk',
				'header'=>'Atasan',
				'value'=>'$data->getRelationField("jabatan_induk","nama")',
			),	
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::link("+",array("jabatan/createStaf","id"=>'.$this->id_jabatan.',"id_staf"=>$data->id),array(
						"style"=>"color:#ffffff",
						"width"=>"30px",
						"class"=>"btn btn-primary btn-xs",
						"data-toggle"=>"tooltip",
						"title"=>"Pilih"
				))',
				'htmlOptions'=>array('style'=>'text-align:center')
        	),
		),
	)); ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>