<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'distribusi-grup-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->textFieldGroup($model,'nama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-6'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255))
	)); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>


<?php $this->endWidget(); ?>
