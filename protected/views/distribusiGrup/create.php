<?php
$this->breadcrumbs=array(
	'Grup Distribusi'=>array('admin'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List DistribusiGrup','url'=>array('index')),
array('label'=>'Manage DistribusiGrup','url'=>array('admin')),
);
?>

<h1>Tambah Grup Distribusi </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>