<?php
$this->breadcrumbs=array(
	'Grup Distribusi'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Grup Distribusi</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'distribusi-grup-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered condensed',
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Grup',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('distribusiGrup/create')
	)); ?>
</div>
