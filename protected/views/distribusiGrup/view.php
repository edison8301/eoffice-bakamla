<?php
$this->breadcrumbs=array(
	'Grup Distribusi'=>array('admin'),
	$model->nama,
);
?>

<h1>Lihat Grup Distribusi</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			'id',
			'nama',
	),
)); ?>

<div>&nbsp;</div>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'distribusi-grup-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
		<?php echo $form->select2Group($model,'anggota',array(
				'wrapperHtmlOptions' => array('class' => 'col-sm-4'),
				'widgetOptions' => array(
					'asDropDownList' => false,
					'options' => array(
						'tags' => Surat::model()->getListAutoCompleteTujuan(),
						'placeholder' => 'Ketik Nama Jabatan/Pegawai',
						'separator' => ';',
						'minimumInputLength'=>2,
					),
				),
		)); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'size'=>'small',
			'label'=>'Tambahkan Daftar Anggota Grup',
		)); ?>
	</div>


<?php $this->endWidget(); ?>

<h3>Daftar Pegawai/Jabatan Grup</h3>

<div>&nbsp;</div>

<table class="table table-condensed table-hover">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="65%">Nama</th>
	<th width="25%">Jenis</th>
	<th width="5%">&nbsp;</th>
</tr>
</thead>
<?php $i=1; foreach($model->findAllDistribusiGrupDetail() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->getNama(); ?></td>
	<td><?php print $data->getJenis(); ?></td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array('distribusiGrupDetail/directDelete','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Hapus','confirm'=>'Yakin akan menghapus data dari list anggota?')); ?></td>
<?php $i++; } ?>
</table>
