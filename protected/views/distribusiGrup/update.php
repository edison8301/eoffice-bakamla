<?php
$this->breadcrumbs=array(
	'Distribusi Grups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List DistribusiGrup','url'=>array('index')),
	array('label'=>'Create DistribusiGrup','url'=>array('create')),
	array('label'=>'View DistribusiGrup','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage DistribusiGrup','url'=>array('admin')),
	);
	?>

	<h1>Update DistribusiGrup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>