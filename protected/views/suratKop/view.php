<?php
$this->breadcrumbs=array(
	'Kop Surat'=>array('admin'),
	$model->nama,
);
?>

<h1>Lihat Kop Surat</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'condensed striped bordered',
		'attributes'=>array(
			'nama',
			array(
				'label'=>'Header',
				'type'=>'raw',
				'value'=>$model->getKop('header')
			),
			array(
				'label'=>'Footer',
				'type'=>'raw',
				'value'=>$model->getKop('footer')
			)
		),
)); ?>

<?php /*
<h3>Header</h3>
<?php print $model->getKop('header'); ?>

<h3>Footer</h3>
<?php print $model->getKop('footer'); ?>
*/ ?>

<div>&nbsp;</div>

<div class="well">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'size'=>'small',
		'url'=>array('update','id'=>$model->id)
	)); ?>
		<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create')
	)); ?>
		<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Kelola',
		'icon'=>'list',
		'size'=>'small',
		'url'=>array('admin')
	)); ?>
</div>