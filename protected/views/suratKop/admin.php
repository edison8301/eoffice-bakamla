<?php
$this->breadcrumbs=array(
	'Kop Surat'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Kop Surat</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-kop-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered condensed',
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Kop Surat',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create')
	)); ?>
</div>