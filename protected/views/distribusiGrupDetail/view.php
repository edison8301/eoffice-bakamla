<?php
$this->breadcrumbs=array(
	'Distribusi Grup Details'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List DistribusiGrupDetail','url'=>array('index')),
array('label'=>'Create DistribusiGrupDetail','url'=>array('create')),
array('label'=>'Update DistribusiGrupDetail','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete DistribusiGrupDetail','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage DistribusiGrupDetail','url'=>array('admin')),
);
?>

<h1>View DistribusiGrupDetail #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_distribusi_grup',
		'model',
		'id_model',
),
)); ?>
