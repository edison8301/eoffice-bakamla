<?php
$this->breadcrumbs=array(
	'Distribusi Grup Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List DistribusiGrupDetail','url'=>array('index')),
	array('label'=>'Create DistribusiGrupDetail','url'=>array('create')),
	array('label'=>'View DistribusiGrupDetail','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage DistribusiGrupDetail','url'=>array('admin')),
	);
	?>

	<h1>Update DistribusiGrupDetail <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>