<?php
$this->breadcrumbs=array(
	'Distribusi Grup Details'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List DistribusiGrupDetail','url'=>array('index')),
array('label'=>'Manage DistribusiGrupDetail','url'=>array('admin')),
);
?>

<h1>Create DistribusiGrupDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>