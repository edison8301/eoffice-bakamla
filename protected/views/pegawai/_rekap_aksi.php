<div class="row">
	<div class="col-md-6">
		<a href="<?php print $this->createUrl("pegawai/siapTerbit"); ?>" data-toggle="tooltip" title="Jumlah draf surat yang siap diterbitkan">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-ok"></i></div>
			<div class="title"> Draf Siap Diterbitkan</div>
			<div class="content"><?php print Pegawai::countSuratSiapTerbit(); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-6">
		<a href="<?php print $this->createUrl("pegawai/paraf"); ?>" data-toggle="tooltip" title="Jumlah draf surat yang harus diperiksa/dikonfirmasi">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-search"></i></div>
			<div class="title"> Draf Konfirmasi/Paraf</div>
			<div class="content"><?php print Paraf::countByUserId(); ?></div>
		</div>
		</a>
	</div>
</div>