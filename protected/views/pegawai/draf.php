<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Draf',
);
?>
<h1>Draf Surat <?php print ucfirst($model->getSuratStatus()); ?></h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_rekap_draf'); ?>

<hr>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-grid',
		'type'=>'striped bordered condensed hover',
		'dataProvider'=>$model->draf(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			array(
				'header'=>'Jenis',
				'name'=>'id_surat_jenis',
				'value'=>'$data->getRelationField("surat_jenis","nama")',
				'filter'=>CHtml::listData(SuratJenis::model()->findAll(),'id','nama')
			),
			array(
				'header'=>'Ringkasan',
				'name'=>'ringkasan',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:left'),
			),
			
			array(
				'header'=>'Waktu Dibuat',
				'name'=>'waktu_dibuat',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dibuat)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Status',
				'name'=>'id_surat_status',
				'value'=>'$data->getRelationField("surat_status","nama")',
				//'filter'=>CHtml::listData(SuratStatus::model()->findAll(),'id','nama'),
				'filter'=>'',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'viewButtonUrl'=>'Yii::app()->createUrl("surat/view", array("id"=>$data->id))'
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'size'=>'small',
		'label'=>'Buat Draf Surat Baru',
		'icon'=>'plus',
		'url'=>array('surat/create')
)); ?>
</div>
