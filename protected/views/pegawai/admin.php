<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Kelola',
);
?>

<h1>Kelola Pegawai</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			'nip',
			'username',
			array(
				'name'=>'id_jabatan',
				'header'=>'Jabatan',
				'value'=>'$data->getRelationField("jabatan","nama")',
			),
			array(
				'name'=>'login_terakhir',
				'value'=>'Helper::getTanggalWaktu($data->login_terakhir)'
			),
			array(
				'name'=>'id',
				'header'=>'',
				'type'=>'raw',
				'filter'=>'',
				'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-lock\"></i>",array("pegawai/setPassword","id"=>$data->id),array("data-toggle"=>"tooltip","title"=>"Set Password"))',
				'headerHtmlOptions'=>array('style'=>'width:30px'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'name'=>'id',
				'header'=>'',
				'type'=>'raw',
				'filter'=>'',
				'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-list\"></i>",array("pegawai/log","id"=>$data->id),array("data-toggle"=>"tooltip","title"=>"Log Aktivitas"))',
				'headerHtmlOptions'=>array('style'=>'width:30px'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('style'=>'width:50px')
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Pegawai',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('pegawai/create')
	)); ?>
</div>