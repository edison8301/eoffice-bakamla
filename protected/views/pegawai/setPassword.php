<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('pegawai/admin'),
	$model->nama=>array('pegawai/view','id'=>$model->id),
	'Set Password',
);

?>

<h1>Set Password</h1>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'disposisi-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($setPasswordForm); ?>

	<div class="well">
	<?php echo $form->passwordFieldGroup($setPasswordForm,'password',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>
	<?php echo $form->passwordFieldGroup($setPasswordForm,'password_konfirmasi',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>

	</div>

	<div class="form-actions well" style='text-align:right'>
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'label'=>'Set Password',
		'icon'=>'lock',
		'size'=>'small',
	)); ?>&nbsp;
	</div>

	
<?php $this->endWidget(); ?>