<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('profil'),
	'Surat',
);
?>

<h1><?php print DistribusiJenis::getNamaById($id_distribusi_jenis); ?></h1>

<hr>

<?php $this->renderPartial('_rekap_distribusi_baru'); ?>

<hr>

<?php $this->renderPartial('_distribusi',array('model'=>$model)); ?>
