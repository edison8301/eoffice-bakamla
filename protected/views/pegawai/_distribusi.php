

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'disposisi-grid',
		'dataProvider'=>$model->pegawai(),
		'filter'=>$model,
		'type'=>'striped bordered condensed',
		'columns'=>array(
			array(
				'header'=>'No Surat',
				'name'=>'id_surat',
				'value'=>'$data->getRelation("surat","nomor")',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'header'=>'Pengirim',
				'name'=>'pengirim',
				'value'=>'$data->getPengirim()',
				'headerHtmlOptions'=>array('style'=>'width:30%')
			),
			array(
				'name'=>'waktu_dibuat',
				'header'=>'Waktu Pengiriman',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dibuat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:20%;text-align:center')
			),
			array(
				'name'=>'waktu_dilihat',
				'header'=>'Waktu Dilihat',
				'value'=>'Helper::getTanggalWaktu($data->waktu_dilihat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:20%;text-align:center')
			),
			/*
			array(
				'name'=>'id_distribusi_jenis',
				'value'=>'$data->getJenis()',
				'filter'=>'',
				//'filter'=>CHtml::listData(DistribusiJenis::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'text-align:center;width:5%')
			), */
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'viewButtonUrl'=>'array("distribusi/view","id"=>$data->id)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'width:5%')
			),
		),
)); ?>

<?php /*
<div class="item">
	<ul class="media-list">
		<li class="media <?= $surat_class; ?>">
			<a class="pull-left" href="#">
				<img class="media-object" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/envelope.png">
			</a>
			<div class="media-body">
				<h4 class="media-heading">
					<?php print CHtml::link($data->getPengirim(),array('distribusi/view','id'=>$data->id)); ?>
				</h4>
				<span style="font-weight:bold"><?php print $data->catatan; ?> <br>
				<span style="font-weight:bold"><i class="glyphicon glyphicon-calendar"></i> </span><?php print Helper::getCreatedTime($data->waktu_dibuat); ?>
			</div>
		</li>
	</ul>
</div>
<hr>
*/ ?>