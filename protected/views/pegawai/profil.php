<?php
$this->breadcrumbs=array(
	'Pegawai',
	'Profil',
);

?>

<h1>Profil Pegawai</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			'nama',
			'nip',
			'email',
			'username',
			array(
				'label'=>'Jabatan',
				'value'=>$model->getRelationField('jabatan','nama'),
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting Profil',
		'icon'=>'pencil',
		'size'=>'small',
		'url'=>array('suntingProfil'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Ganti Password',
		'icon'=>'lock',
		'size'=>'small',
		'url'=>array('pegawai/gantiPassword'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Logout',
		'icon'=>'off',
		'size'=>'small',
		'url'=>array('site/logout')
)); ?>
</div>
