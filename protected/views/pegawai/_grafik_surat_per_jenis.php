<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>


<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "pie3d",
        "renderAt": "grafik-surat-per-jenis",
        "width": "100%",
        "height": "350",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Persentase Surat Per Jenis",
              "xAxisName": "Bulan",
              "yAxisName": "Jumlah Pengeluaran",
              "formatNumberScale" : 0,
              "theme": "fint"
           },
          "data":        
                [ <?php print Pegawai::getDataGrafikSuratPerJenis(); ?> ], 
        }
    });

    revenueChart.render();
})
		
</script>
<div> &nbsp</div> 

<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
      'title'=>'Grafik Surat Per Bulan',
      'context' => 'primary',
      'headerIcon'=>'signal' 
)); ?>  
  <div id="grafik-surat-per-jenis" style="text-align:center">Grafik Surat Per Jenis</div>

<?php $this->endWidget(); ?>
