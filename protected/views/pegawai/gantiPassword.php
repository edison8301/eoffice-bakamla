<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('pegawai/profil'),
	'Ganti Password',
);

?>

<h1>Ganti Password</h1>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'disposisi-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->passwordFieldGroup($gantiPasswordForm,'password_lama',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>
	<?php echo $form->passwordFieldGroup($gantiPasswordForm,'password_baru',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>
	<?php echo $form->passwordFieldGroup($gantiPasswordForm,'password_baru_konfirmasi',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>

	</div>

	<div class="form-actions well" style='text-align:right'>
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'label'=>'Ganti Password',
		'icon'=>'lock',
		'size'=>'small',
		'url'=>array('update','id'=>$model->id),
	)); ?>&nbsp;
	</div>

	
<?php $this->endWidget(); ?>