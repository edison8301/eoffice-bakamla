<?php

$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Memo',
);

$this->pageTitle=Yii::app()->name;
?>

<h1>Tembusan Surat</h1>

<hr>

<?php $this->renderPartial('_rekap_distribusi'); ?>

<hr>

<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_distribusi',
		'ajaxUpdate'=>false
)); ?> 