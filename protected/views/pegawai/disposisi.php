<?php

$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Memo',
);

$this->pageTitle=Yii::app()->name;
?>

<h1>Disposisi Surat</h1>

<hr>

<?php $this->renderPartial('_rekap_distribusi'); ?>

<hr>

<?php $this->renderPartial('_distribusi',array('model'=>$model)); ?>
