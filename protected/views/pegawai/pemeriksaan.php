<?php
$this->breadcrumbs=array(
	'Pegawais'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Create Pegawai','url'=>array('create')),
array('label'=>'Update Pegawai','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pegawai','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>

<h1>Lihat Pegawai</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			'nama',
			'nip',
			'email',
			'username',
			array(
				'label'=>'Jabatan',
				'value'=>$model->getRelationField('jabatan','nama_jabatan')
			)
		),
)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting Pegawai',
		'icon'=>'pencil',
		'size'=>'small',
		'url'=>array('update','id'=>$model->id),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Pegawai',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Hapus Pegawai',
		'icon'=>'trash',
		'size'=>'small',
		'url'=>array('delete','id'=>$model->id)
)); ?>
</div>
