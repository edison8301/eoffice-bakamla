<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>


<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "column3d",
        "renderAt": "grafik-surat-per-bulan",
        "width": "100%",
        "height": "350",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Jumlah Surat Per Bulan",
              "xAxisName": "Bulan",
              "yAxisName": "Jumlah Surat",
              "formatNumberScale" : 0,
              "theme": "fint"
           },
          "data":        
                [ <?php print Pegawai::getDataGrafikSuratPerBulan(); ?> ], 
        }
    });

    revenueChart.render();
})
		
</script>

<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
      'title'=>'Grafik Surat Per Bulan',
      'context' => 'primary',
      'headerIcon'=>'signal' 
)); ?>  
  <div id="grafik-surat-per-bulan" style="text-align:center">Grafik Surat Per Bulan</div>

<?php $this->endWidget(); ?>
