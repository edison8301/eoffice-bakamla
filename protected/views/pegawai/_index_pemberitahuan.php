<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
      'title'=>'Pemberitahuan',
      'context' => 'primary',
      'headerIcon'=>'bullhorn' 
)); ?>  

<div style="height:350px;overflow:auto">
<?php foreach(Pegawai::findAllPemberitahuan(10) as $data) { ?>
	<?php $class=""; if($data->waktu_dilihat==null) $class = "alert-success"; ?>
	<a href="<?php print Yii::app()->controller->createUrl("pemberitahuan/view",array("id"=>$data->id)) ?>">
		<div class="<?php print $class; ?>" style="padding:10px;">
			<div style="margin-bottom:10px"><?php print $data->isi; ?></div>
			<div style="font-size:12px"><i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></div>
		</div>
	</a>
<?php } ?>
</div>

<?php $this->endWidget(); ?>
