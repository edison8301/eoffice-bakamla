<?php Yii::app()->clientScript->registerScript("struktural","
		$(document).ready(function() {
			refreshJabatan();
		});

		$('#Pegawai_struktural').change(function() {
			refreshJabatan();
		});

		function refreshJabatan() {
			var struktural = $('#Pegawai_struktural').val();
			if(struktural==1) {
				$('#s2id_Pegawai_id_jabatan').parents('.form-group').show();
				$('#s2id_Pegawai_id_jabatan_induk').parents('.form-group').hide();
			}
			if(struktural==2) {
				$('#s2id_Pegawai_id_jabatan').parents('.form-group').hide();
				$('#s2id_Pegawai_id_jabatan_induk').parents('.form-group').show();
			}
		}
"); ?>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pegawai-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal'
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
		<?php echo $form->textFieldGroup($model,'nama',array(
				'htmlOptions'=>array('class'=>'tes','id'=>'tes'),
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),	
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>

		<?php echo $form->textFieldGroup($model,'nip',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>

		<?php if($model->username == null) { ?>
		<?php echo $form->textFieldGroup($model,'username',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('class'=>'span5','maxlength'=>128),
					'hints'=>'Username tidak dapat diuba'
				),
				'hint'=>'<b>Keterangan:</b> Setelah diisi, username tidak dapat diubah'

		)); ?>

		<?php echo $form->passwordFieldGroup($model,'password',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('value'=>'')
				)
		)); ?>
		<?php } else { ?>
		<?php echo $form->textFieldGroup($model,'username',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('readonly'=>'readonly','maxlength'=>128)),
				'hint'=>'<b>Keterangan:</b> Username tidak dapat diubah'
		)); ?>
		<?php } ?>

		<?php echo $form->textFieldGroup($model,'email',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>


		<?php echo $form->select2Group($model,'id_jabatan',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array(
						'data'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama'),
						'htmlOptions'=>array('empty'=>'-- Pilih Jabatan --','maxlength'=>128)
				)
		)); ?>

		<?php if($model->isNewRecord) { ?>			
		<?php echo $form->textFieldGroup($model,'username',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
				'widgetOptions'=>array(
					'htmlOptions'=>array('maxlength'=>128)))); ?>

		<?php echo $form->passwordFieldGroup($model,'password',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128))
		)); ?>
		<?php } ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>