<?php

$this->breadcrumbs=array(
	'Surat Terbit'
);

$this->pageTitle=Yii::app()->name;
?>

<h1>Surat Siap Terbit</h1>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-grid',
		'type'=>'striped bordered condensed hover',
		'dataProvider'=>$model->siap(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'Jenis',
				'name'=>'id_surat_jenis',
				'value'=>'$data->getRelationField("surat_jenis","nama")',
				'filter'=>CHtml::listData(SuratJenis::model()->findAll(),'id','nama')
			),
			array(
				'header'=>'Ringkasan',
				'name'=>'ringkasan',
				'value'=>'$data->ringkasan',
			),
			array(
				'header'=>'Waktu Diajukan',
				'name'=>'waktu_diajukan',
				'value'=>'Helper::getTanggalWaktu($data->waktu_diajukan)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'viewButtonUrl'=>'Yii::app()->createUrl("surat/view", array("id"=>$data->id))'
			),
		),
)); ?>
