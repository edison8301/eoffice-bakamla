<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('admin'),
	$model->nama,
);
?>

<h1>Log Pegawai: <?php print $model->nama; ?></h1>

<div>&nbsp;</div>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'log-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$log->pegawai(),
		'filter'=>$log,
		'columns'=>array(
			'username',
			'ip',
			'controller',
			'action',
			'uri',
			'waktu',
		),
)); ?>
