<?php
/* @var $this SiteController */


$this->pageTitle=Yii::app()->name;
?>

<?php
	$this->breadcrumbs = array(
		'Dashboard'
	)

?>

<h1>Dashboard</h1>

<hr>

<?php $this->renderPartial('_rekap_distribusi_baru'); ?>

<hr>

<?php if(Pegawai::hasAksi()) { ?>
    <?php $this->renderPartial('_rekap_aksi'); ?>
    <hr>
<?php } ?>

<?php if(Pegawai::hasDraf()) { ?>
    <?php $this->renderPartial('_rekap_draf'); ?>
    <hr>
<?php } ?>

<div class="row">
	<div class="col-sm-6">
        <?php $this->renderPartial('_index_pemberitahuan'); ?>
	</div>
	<div class="col-sm-6">
        <?php $this->renderPartial('_grafik_surat_per_bulan'); ?>
        <?php //$this->renderPartial('_grafik_surat_per_jenis'); ?>
	</div>

</div>

<div>&nbsp;</div>


<?php /*
<div class="row">
	<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jumlah Surat Per Bulan',
        		'context'=>'primary',
        		'headerIcon' => 'envelope',
        		'content' => $this->renderPartial('_suratkeluarperbulan',array(),true)
    	)); ?>
    </div>
    <div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jumlah Memo Per Bulan',
        		'context' => 'primary',
        		'headerIcon' => 'file',
        		'content' => $this->renderPartial('_suratperbulan',array(),true)
    	)); ?>
    </div>
</div>


<div class="row">
	<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jenis Surat Keluar',
        		'headerIcon' => 'bullhorn',
        		'context'=>'primary',
        		'content' => $this->renderPartial('_jenissuratkeluar',array(),true)
    	)); ?>
    </div>
    <div class="col-md-6">
		<?php $this->widget('booster.widgets.TbPanel',array(
        		'title' => 'Grafik Jenis Surat Keluar',
        		'context'=>'primary',
        		'headerIcon' => 'share-alt',
        		'content' => $this->renderPartial('_jenissuratkeluarpie',array(),true)
    	)); ?>
    </div>
</div>



<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Draft','content'=>$this->renderPartial('_draft',array(),true),'active'=>true),
			array('label'=>'Review','content'=>$this->renderPartial('_review',array(),true)),
			array('label'=>'Perbaikan','content'=>$this->renderPartial('_perbaikan',array(),true)),
			array('label'=>'Disetujui','content'=>$this->renderPartial('_disetujui',array(),true)),
			array('label'=>'Ditolak','content'=>$this->renderPartial('_ditolak',array(),true)),
		),
)); ?>

*/ ?>