<div class="row">
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>1,"baru"=>1)); ?>" data-toggle="tooltip" title="Jumlah surat baru yang belum dilihat">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Surat Baru</div>
			<div class="content"><?php print Pegawai::countDistribusiBaruByJenis(1); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>2,"baru"=>1)); ?>" data-toggle="tooltip" title="Jumlah tembusan baru yang belum dilihat">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-tags"></i></div>
			<div class="title"> Tembusan Baru</div>
			<div class="content"><?php print Pegawai::countDistribusiBaruByJenis(2); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>3,"baru"=>1)); ?>" data-toggle="tooltip" title="Jumlah disposisi baru yang belum dilihat">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-arrow-down"></i></div>
			<div class="title"> Disposisi Baru</div>
			<div class="content"><?php print Pegawai::countDistribusiBaruByJenis(3); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>4,"baru"=>1)); ?>" data-toggle="tooltip" title="Jumlah terusan surat yang belum dilihat">
		<div class="info-box" style="background:#76564E">
			<div class="icon pull-left"><i class="glyphicon glyphicon-arrow-right"></i></div>
			<div class="title"> Terusan Baru</div>
			<div class="content"><?php print Pegawai::countDistribusiBaruByJenis(4); ?></div>
		</div>
		</a>
	</div>
	<?php /*
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("pegawai/paraf"); ?>" data-toggle="tooltip" title="Jumlah draf surat yang harus diperiksa/dikonfirmasi">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-search"></i></div>
			<div class="title"> Konfirmasi Draf</div>
			<div class="content"><?php print Paraf::countByUserId(); ?></div>
		</div>
		</a>
	</div>
	*/ ?>
</div>