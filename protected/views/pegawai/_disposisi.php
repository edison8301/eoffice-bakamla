<?php $surat_class = null; if($data->waktu_dilihat == null) $surat_class = 'alert-success'; ?>

<div class="item">
	<ul class="media-list">
		<li class="media <?= $surat_class; ?>">
			<a class="pull-left" href="#">
				<img class="media-object" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/envelope.png">
			</a>
			<div class="media-body">
				<h4 class="media-heading">
					<?php print CHtml::link($data->getPengirim(),array('distribusi/view','id'=>$data->id)); ?>
				</h4>
				<span style="font-weight:bold"><?php print $data->catatan; ?> <br>
				<span style="font-weight:bold"><i class="glyphicon glyphicon-calendar"></i> </span><?php print Helper::getCreatedTime($data->waktu_dibuat); ?>
			</div>
		</li>
	</ul>
</div>

<hr>