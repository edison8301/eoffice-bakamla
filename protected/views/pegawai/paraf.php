<?php
	$this->breadcrumbs = array(
		'Paraf Surat'
	)
?>

<h1>Draf Surat Yang Harus Dikonfirmasi</h1>

<hr>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'paraf-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->pegawai(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'name'=>'id_surat',
				'header'=>'Jenis Surat',
				'value'=>'SuratJenis::getNamaById($data->getRelation("surat","id_surat_jenis"))',
			),
			array(
				'name'=>'id_surat',
				'header'=>'Ringkasan',
				'value'=>'$data->getRelation("surat","ringkasan")',
			),
			array(
				'name'=>'id_surat',
				'header'=>'Pengaju',
				'value'=>'$data->surat->getPengaju()',
				'headerHtmlOptions'=>array('style'=>'text-align:center;width:20%'),
			),
			array(
				'name'=>'id_surat',
				'header'=>'Waktu Diajukan',
				'value'=>'Helper::getTanggalWaktu($data->getRelation("surat","waktu_diajukan"))',
				'headerHtmlOptions'=>array('style'=>'text-align:center;width:15%'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'viewButtonUrl'=>'Yii::app()->controller->createUrl("surat/paraf",array("id"=>$data->id_surat))'
			),
		)
)); ?>			