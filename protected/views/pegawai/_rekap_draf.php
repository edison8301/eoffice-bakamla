<div class="row">
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/draf",array("status"=>"konsep")); ?>" data-toggle="tooltip" title="Jumlah draf surat yang dalam status konsep">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-pencil"></i></div>
			<div class="title"> Konsep</div>
			<div class="content"><?php print Pegawai::countSuratByStatus(2); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/draf",array("status"=>"pemeriksaan")); ?>" data-toggle="tooltip" title="Jumlah draf surat yang dalam status pemeriksaan">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-adjust"></i></div>
			<div class="title"> Pemeriksaan</div>
			<div class="content"><?php print Pegawai::countSuratByStatus(3); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/draf",array("status"=>"perbaikan")); ?>" data-toggle="tooltip" title="Jumlah draf surat yang dalam status perbaikan">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-refresh"></i></div>
			<div class="title"> Perbaikan</div>
			<div class="content"><?php print Pegawai::countSuratByStatus(4); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("pegawai/draf",array("status"=>"disetujui")); ?>" data-toggle="tooltip" title="Jumlah draf surat yang dalam status disetujui">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-ok"></i></div>
			<div class="title"> Disetujui</div>
			<div class="content"><?php print Pegawai::countSuratByStatus(5); ?></div>
		</div>
		</a>
	</div>
</div>