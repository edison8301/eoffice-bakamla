<?php $this->breadcrumbs=array(
	'Pegawai'=>array('profil'),
	'Sunting Profil',
); ?>
<h1>Sunting Profil</h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'pegawai-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal'
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
		<?php echo $form->textFieldGroup($model,'nama',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>

		<?php echo $form->textFieldGroup($model,'nip',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>

		<?php echo $form->textFieldGroup($model,'email',array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
				'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>128)))); ?>

	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>