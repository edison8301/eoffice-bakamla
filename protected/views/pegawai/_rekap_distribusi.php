<div class="row">
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>1)); ?>" data-toggle="tooltip" title="Jumlah tembusan yang dimiliki pegawai">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-envelope"></i></div>
			<div class="title"> Surat</div>
			<div class="content"><?php print Pegawai::countDistribusiByJenis(1); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>2)); ?>" data-toggle="tooltip" title="Jumlah disposisi yang dimiliki pegawai">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-tags"></i></div>
			<div class="title">Tembusan</div>
			<div class="content"><?php print Pegawai::countDistribusiByJenis(2); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/distribusi",array("id_distribusi_jenis"=>3)); ?>" data-toggle="tooltip" title="Jumlah terusan surat baru yang dimiliki pegawai">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-arrow-down"></i></div>
			<div class="title">Disposisi</div>
			<div class="content"><?php print Pegawai::countDistribusiByJenis(3); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("pegawai/paraf"); ?>" data-toggle="tooltip" title="Jumlah draf surat yang harus diperiksa/dikonfirmasi">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-search"></i></div>
			<div class="title"> Konfirmasi Draf</div>
			<div class="content"><?php print Paraf::countByUserId(); ?></div>
		</div>
		</a>
	</div>
</div>