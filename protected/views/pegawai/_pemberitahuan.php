<?php $class=""; if($data->waktu_dilihat==null) $class = "alert-success"; ?>
<a href="<?php print Yii::app()->controller->createUrl("pemberitahuan/view",array("id"=>$data->id)); ?>">
<div class="<?php print $class; ?>" style="margin-bottom:10px;padding:10px;background:#efefef">
	<div class="row" style="margin-bottom:3px">
		<div class="col-sm-12"><?php print $data->isi; ?></div>
	</div>
	<div class="row">
		<div class="col-sm-12" style="font-size:12px"><i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></div>
	</div>
</div>
</a>