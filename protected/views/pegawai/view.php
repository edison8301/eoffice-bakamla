<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('admin'),
	$model->nama,
);
?>

<h1>Lihat Pegawai</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			'nama',
			'nip',
			'email',
			'username',
			array(
				'label'=>'Jabatan',
				'value'=>$model->getRelationField('jabatan','nama'),
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Sunting Pegawai',
		'icon'=>'pencil',
		'size'=>'small',
		'url'=>array('update','id'=>$model->id),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Pegawai',
		'icon'=>'plus',
		'size'=>'small',
		'url'=>array('create'),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Set Password',
		'icon'=>'lock',
		'size'=>'small',
		'url'=>array('setPassword','id'=>$model->id),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Hapus Pegawai',
		'icon'=>'trash',
		'size'=>'small',
		'url'=>array('delete','id'=>$model->id)
)); ?>
</div>
