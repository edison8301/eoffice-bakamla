<?php

$this->breadcrumbs=array(
	'Surat Terbit'
);

$this->pageTitle=Yii::app()->name;
?>

<h1>Arsip Surat <?php print ucfirst($_GET['sumber']); ?></h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_rekap_surat'); ?>

<hr>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-grid',
		'type'=>'striped bordered condensed hover',
		'dataProvider'=>$model->terbit(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'Nomor Surat',
				'name'=>'nomor',
				'value'=>'$data->nomor',
			),
			array(
				'header'=>'Tanggal Surat',
				'name'=>'tanggal',
				'value'=>'Helper::getTanggalSingkat($data->tanggal)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Jenis',
				'name'=>'id_surat_jenis',
				'value'=>'$data->getRelationField("surat_jenis","nama")',
				'filter'=>CHtml::listData(SuratJenis::model()->findAll(),'id','nama')
			),
			array(
				'header'=>'Ringkasan',
				'name'=>'ringkasan',
				'value'=>'$data->ringkasan',
			),
			array(
				'header'=>'Waktu Diterbitkan',
				'name'=>'waktu_diterbitkan',
				'value'=>'Helper::getTanggalWaktu($data->waktu_diterbitkan)',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'viewButtonUrl'=>'Yii::app()->createUrl("surat/view", array("id"=>$data->id))'
			),
		),
)); ?>
