<?php

$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Memo',
);

$this->pageTitle=Yii::app()->name;
?>

<h1>Memo</h1>

<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_memo',
		'ajaxUpdate'=>false
)); ?>