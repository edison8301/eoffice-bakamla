<div class="row">
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/surat",array("sumber"=>"dibuat")); ?>" data-toggle="tooltip" title="Jumlah surat terbit yang dibuat atau diajukan sendiri">
		<div class="info-box" style="background:#727CB6">
			<div class="icon pull-left"><i class="glyphicon glyphicon-pencil"></i></div>
			<div class="title"> Dibuat</div>
			<div class="content"><?php print Pegawai::countSuratTerbitBySumber("dibuat"); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/surat",array("sumber"=>"tembusan")); ?>" data-toggle="tooltip" title="Jumlah surat terbit yang berasal dari tembusan">
		<div class="info-box" style="background:#348FE2">
			<div class="icon pull-left"><i class="glyphicon glyphicon-tags"></i></div>
			<div class="title"> Tembusan</div>
			<div class="content"><?php print Pegawai::countSuratTerbitBySumber("tembusan"); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print Yii::app()->controller->createUrl("pegawai/surat",array("sumber"=>"disposisi")); ?>" data-toggle="tooltip" title="Jumlah surat terbit yang berasal dari disposisi">
		<div class="info-box" style="background:#D9534F">
			<div class="icon pull-left"><i class="glyphicon glyphicon-arrow-down"></i></div>
			<div class="title"> Disposisi</div>
			<div class="content"><?php print Pegawai::countSuratTerbitBySumber("disposisi"); ?></div>
		</div>
		</a>
	</div>
	<div class="col-md-3">
		<a href="<?php print $this->createUrl("pegawai/surat",array("sumber"=>"terusan")); ?>" data-toggle="tooltip" title="Jumlah surat terbit yang berasal dari terusan">
		<div class="info-box" style="background:#00ACAC">
			<div class="icon pull-left"><i class="glyphicon glyphicon-arrow-right"></i></div>
			<div class="title"> Terusan</div>
			<div class="content"><?php print Pegawai::countSuratTerbitBySumber("terusan"); ?></div>
		</div>
		</a>
	</div>
</div>