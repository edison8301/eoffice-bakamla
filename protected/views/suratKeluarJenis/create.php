<?php
$this->breadcrumbs=array(
	'Surat Keluar Jenises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SuratKeluarJenis','url'=>array('index')),
array('label'=>'Manage SuratKeluarJenis','url'=>array('admin')),
);
?>

<h1>Tambah Jenis Surat Keluar</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>