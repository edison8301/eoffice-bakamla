<?php
$this->breadcrumbs=array(
	'Surat Keluar Jenises'=>array('index'),
	'Manage',
);


?>

<h1>Kelola Jenis Surat Keluar</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Jenis',
		'icon'=>'plus',
		'url'=>array('create')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-keluar-jenis-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('style'=>'width:8%')
			),
		),
)); ?>
