<?php
$this->breadcrumbs=array(
	'Surat Keluar Jenises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SuratKeluarJenis','url'=>array('index')),
array('label'=>'Create SuratKeluarJenis','url'=>array('create')),
array('label'=>'Update SuratKeluarJenis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SuratKeluarJenis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SuratKeluarJenis','url'=>array('admin')),
);
?>

<h1>View SuratKeluarJenis #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
		'contoh',
),
)); ?>
