<?php
$this->breadcrumbs=array(
	'Surat Keluar Jenises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List SuratKeluarJenis','url'=>array('index')),
	array('label'=>'Create SuratKeluarJenis','url'=>array('create')),
	array('label'=>'View SuratKeluarJenis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SuratKeluarJenis','url'=>array('admin')),
	);
	?>

<h1>Sunting Jenis Surat Keluar</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>