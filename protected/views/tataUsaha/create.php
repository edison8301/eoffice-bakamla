<?php
$this->breadcrumbs=array(
	'Tata Usaha'=>array('index'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List TataUsaha','url'=>array('index')),
array('label'=>'Manage TataUsaha','url'=>array('admin')),
);
?>

<h1>Tambah Tata Usaha</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>