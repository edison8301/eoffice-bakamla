<?php
$this->breadcrumbs=array(
	'Tata Usaha'=>array('index'),
	'Sunting',
);

	$this->menu=array(
	array('label'=>'List TataUsaha','url'=>array('index')),
	array('label'=>'Create TataUsaha','url'=>array('create')),
	array('label'=>'View TataUsaha','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TataUsaha','url'=>array('admin')),
	);
	?>

<h1>Sunting Tata Usaha</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>