<?php

$this->breadcrumbs=array(
	'Tata Usahas'=>array('index'),
	'Manage',
);

?>

<h1>Kelola Tata Usaha</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'tata-usaha-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'id',
			'id_jabatan',
			'id_jabatan_induk',
			array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
),
)); ?>
