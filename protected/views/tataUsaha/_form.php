<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tata-usaha-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal'
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->select2Group($model,'id_jabatan_induk',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5'),
				'data'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama'),
				'options' => array(
					'placeholder' => 'Ketik Nama Jabatan/Pegawai',
					'minimumInputLength'=>2,
				),
			)
	)); ?>
	<?php echo $form->select2Group($model,'id_jabatan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5'),
				'data'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama'),
				'options' => array(
					'placeholder' => 'Ketik Nama Jabatan/Pegawai',
					'minimumInputLength'=>2,
				),
			)
	)); ?>


	</div>
	
	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
