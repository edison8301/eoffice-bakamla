<?php
$this->breadcrumbs=array(
	'Tata Usahas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TataUsaha','url'=>array('index')),
array('label'=>'Create TataUsaha','url'=>array('create')),
array('label'=>'Update TataUsaha','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TataUsaha','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TataUsaha','url'=>array('admin')),
);
?>

<h1>View TataUsaha #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_jabatan',
		'id_jabatan_induk',
),
)); ?>
