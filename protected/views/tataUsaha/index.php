<?php
$this->breadcrumbs=array(
	'Tata Usaha',
);
?>

<h1>Tata Usaha</h1>

<div>&nbsp;</div>

<table class="table">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="45%">Kepala/Atasan</th>
	<th width="45%">Jabatan TU</th>
	<th width="5%">&nbsp;</th>
</tr>
</thead>
<?php $i=1; foreach(TataUsaha::model()->findAll() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->getRelation("jabatan_induk","nama"); ?></td>
	<td><?php print $data->getRelation("jabatan","nama"); ?></td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array("tataUsaha/update","id"=>$data->id)); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'size'=>'small',
		'label'=>'Tambah Tata Usaha',
		'icon'=>'plus',
		'url'=>array('create')
	)); ?>
</div>