<?php
$this->breadcrumbs=array(
	'Pemberitahuans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Pemberitahuan','url'=>array('index')),
	array('label'=>'Create Pemberitahuan','url'=>array('create')),
	array('label'=>'View Pemberitahuan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pemberitahuan','url'=>array('admin')),
	);
	?>

	<h1>Update Pemberitahuan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>