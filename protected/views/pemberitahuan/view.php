<?php
$this->breadcrumbs=array(
	'Pemberitahuans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pemberitahuan','url'=>array('index')),
array('label'=>'Create Pemberitahuan','url'=>array('create')),
array('label'=>'Update Pemberitahuan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pemberitahuan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pemberitahuan','url'=>array('admin')),
);
?>

<h1>View Pemberitahuan #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'isi',
		'tautan',
		'id_jabatan',
		'username',
		'waktu_dilihat',
		'waktu_dibuat',
),
)); ?>
