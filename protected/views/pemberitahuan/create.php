<?php
$this->breadcrumbs=array(
	'Pemberitahuans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Pemberitahuan','url'=>array('index')),
array('label'=>'Manage Pemberitahuan','url'=>array('admin')),
);
?>

<h1>Create Pemberitahuan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>