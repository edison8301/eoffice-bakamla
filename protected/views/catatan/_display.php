<table class="table table-striped">
<thead>
<tr>
	<th style="text-align:right" width="25%">Penulis</th>
	<th width="75%">Catatan</th>
</tr>
</thead>
<?php foreach($model->findAllCatatan() as $data) { ?>
<tr>
	<td style="text-align:right">
		<div style="margin-bottom:5px;font-weight:bold"><i class="glyphicon glyphicon-user"></i>&nbsp;<?php print $data->pembuat; ?></div>
		<div style="font-size:11px"><i class="glyphicon glyphicon-calendar"></i>&nbsp;<?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></div>
	</td>
	<td style="text-align:left">
		<?php print $data->catatan; ?>
		<?php $i=1; foreach($data->findAllLampiran() as $lampiran) { ?>
		<div><?php print CHtml::link('<i class="glyphicon glyphicon-download-alt"></i> Lampiran '.$i,$lampiran->getUrl(),array('target'=>'_blank')); ?></div>
		<?php $i++; } ?>
	</td>
</tr>
<?php } ?>
</table>
