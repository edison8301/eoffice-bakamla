<?php
$this->breadcrumbs=array(
	'Catatans',
);

$this->menu=array(
array('label'=>'Create Catatan','url'=>array('create')),
array('label'=>'Manage Catatan','url'=>array('admin')),
);
?>

<h1>Catatans</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
