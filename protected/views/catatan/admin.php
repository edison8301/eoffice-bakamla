<?php
$this->breadcrumbs=array(
	'Catatans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Catatan','url'=>array('index')),
array('label'=>'Create Catatan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('catatan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Catatans</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'catatan-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'model',
		'id_model',
		'catatan',
		'lampiran',
		'id_jabatan',
		/*
		'username',
		'waktu_dilihat',
		'waktu_dibuat',
		*/
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
