<?php
$this->breadcrumbs=array(
	'Catatans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Catatan','url'=>array('index')),
array('label'=>'Manage Catatan','url'=>array('admin')),
);
?>

<h1>Create Catatan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>