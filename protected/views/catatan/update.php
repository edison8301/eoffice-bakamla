<?php
$this->breadcrumbs=array(
	'Catatans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Catatan','url'=>array('index')),
	array('label'=>'Create Catatan','url'=>array('create')),
	array('label'=>'View Catatan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Catatan','url'=>array('admin')),
	);
	?>

	<h1>Update Catatan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>