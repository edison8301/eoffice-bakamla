<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT KUASA</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>

<table class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="3">Yang bertanda tangan di bawah ini :</td>
	</tr>
	<tr>
		<td width="1%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td width="4%">:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('alamat_pejabat'); ?></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">Dengan ini memberi Surat Kuasa Kepada :</td>
	</tr>
	
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td>:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><?php print $model->getAtribut('unit_kerja'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Untuk</td>
		<td>:</td>
		<td><?php print $model->getAtribut('untuk'); ?></td>
	</tr>

	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td></td>
		<td colspan="3">Demikian Surat Kuasa ini dibuat dengan sebenarnya untuk digunakan sebagaimana mestinya. </td>
	</tr>
	</table>

	<div>&nbsp;</div>

	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 117px">Dikeluarkan Di :<?php print $model->getAtribut('tempat_dikeluarkan'); ?></td>
	</tr>
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 117px">Pada Tanggal :<?php print $model->getAtribut('tanggal'); ?></td>
	</tr>
	<hr>
	<tr>
		<td width="49%" style="text-align:center;font-weight:bold">Yang Menerima Kuasa</td>
		<td colspan="2" style="text-align:center;font-weight:bold">Yang Memberi Kuasa</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td style="padding-right: 80px" style="text-align:center;font-weight:bold"><u><?php print $model->getAtribut('menerima_kuasa'); ?></td>
		<td colspan="2" style="padding-right: 80px" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="padding-right: 80px" style="text-align:center;font-weight:bold">NIP. <?php print $model->getAtribut('nip_penerima_kuasa'); ?></td>
		<td colspan="2" style="padding-right: 80px" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>
