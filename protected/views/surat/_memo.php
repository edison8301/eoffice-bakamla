<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">MEMO</div>
	<div style="text-align:center"><span style="border-top:1px solid #000"><?php $model->setNomor(); print $model->nomor; ?></span></div>
	
	<div>&nbsp;</div>

	<table width="100%" class="surat">

	<tr>
		<td width="5%">&nbsp;</td>
		<td width="15%">Kepada Yth.</td>
		<td width="80%">: <input placeholder="Penerima Memo" type="text" name="SuratAtribut[penerima_memo]" value="<?php print $model->getAtribut('penerima_memo'); ?>" size="40"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: <input placeholder="Pengirim Memo" type="text" name="SuratAtribut[pengirim_memo]" value="<?php print $model->getAtribut('pengirim_memo'); ?>" size="40"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: <input placeholder="Hal yang ingin disampaikan" type="text" name="SuratAtribut[perihal]" value="<?php print $model->getAtribut('perihal'); ?>" size="40"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: <input placeholder="Jumlah Lampiran" type="text" name="SuratAtribut[lampiran]" value="<?php print $model->getAtribut('lampiran'); ?>" size="40"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: <input placeholder="Tanggal/Bulan/Tahun Memo Dibuat" type="text" name="SuratAtribut[tanggal_memo]" value="<?php print $model->getAtribut('tanggal_memo'); ?>" size="40"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="border-bottom:1px solid #000000">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<?php $tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; ?>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><?php print $tab; ?>Dengan hormat,</td>
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>
			<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
					'name'=>'SuratAtribut[isi]',
					'value'=>$model->getAtribut('isi')
			)); ?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>
			<b>Bantuan:</b> Tambahkan teks [tab] pada awal paragraf untuk mendapatkan inden/masuk
		</td>
	</tr>	
</table>
<div>&nbsp;</div>

<table width="100%" style="" class="surat">
	<tr>
		<td width="50%"></td>
		<td width="50%" style="text-align:center;font-weight:bold"><input placeholder="Jabatan" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:center;font-weight:bold"><input placeholder="Nama Penandatangan" type="text" name="SuratAtribut[pengirim_memo_tandatangan]" value="<?php print $model->getAtribut('pengirim_memo_tandatangan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:center;font-weight:bold">NIP. <input placeholder="NIP Penandatangan" type="text" name="SuratAtribut[nip_tandatangan]" value="<?php print $model->getAtribut('nip_tandatangan'); ?>"></td>
	</tr>
	</table>
	
</div>