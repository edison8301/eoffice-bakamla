<style>
table.surat td {
	vertical-align: top;
}
</style>

<table width="100%" class="surat">	
	<tr>

		<td width="10%">Nomor</td>
		<td width="1%" >:</td>
		<td><?php print $model->getNomor(); ?></td>
		<td width="31%" >Jakarta, <?php print $model->getAtribut('surat_tanggal'); ?></td>
	</tr>
	<tr>
		<td>Sifat</td>
		<td>:</td>
		<td><?php print $model->getAtribut('surat_sifat'); ?></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Lampiran</td>
		<td>:</td>
		<td><?php print $model->getAtribut('surat_lampiran'); ?></td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td>:</td>
		<td><u><?php print $model->getAtribut('surat_perihal'); ?></u></td>
		<td style="font-weight: bold">DAFTAR UNDANGAN TERLAMPIR</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>di</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="text-align: left"><u>Jakarta</u></td>
	</tr>
</table>

<div>&nbsp;</div>

<?php $tab = ''; for($i=1;$i<=10;$i++) $tab .= '&nbsp;'; ?>
<table width="100%" class="surat">
	<tr>
		<td colspan="3"><?php print $tab; ?>Dengan Hormat</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td colspan="3"><?php print $tab; ?>Dalam rangka <?php print $model->getAtribut('undangan_deskripsi'); ?> maka kami mengundang Bapak/Ibu/Sdr/i 
		 	untuk hadir dalam rapat yang akan diselenggarakan pada :
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td width="20%"><?php print $tab; ?>Hari</td>
		<td width="1%">:</td>
		<td><?php print $model->getAtribut('undangan_hari'); ?></td>
	</tr>
	<tr>
		<td><?php print $tab; ?>Tanggal</td>
		<td>:</td>
		<td><?php print $model->getAtribut('undangan_tanggal'); ?></td>

	</tr>
	<tr>
		<td><?php print $tab; ?>Pukul</td>
		<td>:</td> 
		<td><?php print $model->getAtribut('undangan_pukul'); ?></td>
		
	</tr>
	<tr>
		<td><?php print $tab; ?>Tempat</td>
		<td>:</td>
		<td><?php print $model->getAtribut('undangan_tempat'); ?></td>
		
	</tr>
	<tr>
		<td><?php print $tab; ?>Pimpinan Rapat</td>
		<td>:</td>
		<td><?php print $model->getAtribut('undangan_pimpinan_rapat'); ?></td>
		
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td colspan="3"><?php print $tab; ?>Demikian atas perhatian dan kehadiran Bapak/Ibu/Sdr/i, kami ucapkan terima kasih.</td>
	</tr>

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('penandatangan_an'); ?></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('penandatangan_jabatan'); ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u><?php print $model->getAtribut('penandatangan_nama'); ?></u></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('penandatangan_nip'); ?></td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php /*$this->renderPartial('cetak_surat_undangan',array('model'=>$model));*/ ?>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>