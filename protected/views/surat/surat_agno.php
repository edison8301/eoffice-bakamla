<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>
	
	<div style="border:1px solid #000;padding:20px">
	<table class="surat" width="100%">
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">Nomor Bagian Kepegawaian</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input type="text" name="SuratAtribut[nomor_bag_pegawai]" value="<?php print $model->getAtribut('nomor_bag_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">Tanggal</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input type="text" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>"></td>
	</tr>
	<tr>		
	</table>

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KETERANGAN TIDAK MASUK</div>
	<div style="text-align:center;font-weight:bold"><?php $model->setNomor(); print $model->nomor; ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>1.</td>
		<td colspan="2">Yang Bertanda Tangan di Bawah ini:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>: <input placeholder="Nama Atasan" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>: <input placeholder="NIP/NRP Atasan" type="text" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <input placeholder="Jabatan Atasan" type="text" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>: <input placeholder="Unit Kerja Atasan" type="text" name="SuratAtribut[unit_kerja_pejabat]" value="<?php print $model->getAtribut('unit_kerja_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td colspan="2">Dengan ini Menerangkan Bahwa</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>: <input placeholder="Nama Pegawai Yang Tidak Masuk" type="text" name="SuratAtribut[nama]" value="<?php print $model->getAtribut('nama'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>: <input placeholder="NIP/NRP Pegawai" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <input placeholder="Jabatan Pegawai" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>: <input placeholder="Unit Kerja Pegawai" type="text" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>			
	<tr>
		<td></td>
		<td colspan="2">Pada Hari
				<input placeholder="hari" type="text" name="SuratAtribut[hari]" value="<?php print $model->getAtribut('hari'); ?>">
				 Tanggal
				<input placeholder="Tanggal" type="text" name="SuratAtribut[tanggal_inti]" value="<?php print $model->getAtribut('tanggal_inti'); ?>">, 
				Pukul 
				<input placeholder="Pukul" type="text" name="SuratAtribut[pukul]" value="<?php print $model->getAtribut('pukul'); ?>">
					<b>Tidak Bisa Masuk Kantor</b> Karena <textarea placeholder="Alasan Tidak Masuk" name="SuratAtribut[isi]" rows="1" cols="40"><?php print $model->getAtribut('isi'); ?></textarea> Karena Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.	</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	


	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">Jakarta, <input placeholder="Tanggal" type="text" name="SuratAtribut[tanggal_lengkap]" value="<?php print $model->getAtribut('tanggal_lengkap'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Nama Atasasn" type="text" name="SuratAtribut[nama_pejabat_tandatangan]" value="<?php print $model->getAtribut('nama_pejabat_tandatangan'); ?>"></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="NIP Atasan" type="text" name="SuratAtribut[nip_pejabat_tandatangan]" value="<?php print $model->getAtribut('nip_pejabat_tandatangan'); ?>"></td>
	</tr>
	</table>

	</div>

	<div>&nbsp;</div>

	<?php $this->renderPartial('_tembusan',array('model'=>$model,'form'=>$form)); ?>

	<div class="well">
	<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Save',
	)); ?>
	<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'link',
				'context'=>'success',
				'icon'=>'print',
				'url'=>array('surat/cetakPdf','id'=>$model->id),
				'label'=>'Cetak',
				'htmlOptions'=>array('target'=>'_blank'),
	)); ?>
	</div>

<?php $this->endWidget(); ?>