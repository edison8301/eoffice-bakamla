<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">MEMO</div>
	<div style="text-align:center;font-weight:bold">
	Nomor: M-
	<input placeholder="Nomor Surat" type="text" name="SuratAtribut[no_surat]" value="<?php print $model->getAtribut('no_surat'); ?>">
	/
	<input placeholder="Jabatan" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>">
	/
	<input placeholder="Bag Organisassi" type="text" name="SuratAtribut[bag_organisasi]" value="<?php print $model->getAtribut('bag_organisasi'); ?>">
	/Sestama/Bakamla/
	<input type="text" placeholder="Bulan" name="SuratAtribut[bulan_romawi]" size="2" value="<?php print $model->getAtribut('bulan_romawi'); ?>">
	/
	<input type="text" Placeholder="Tahun" name="SuratAtribut[tahun]" size="4" value="<?php print $model->getAtribut('tahun'); ?>"></div>
	
	<div>&nbsp;</div>

	<table class="surat">

	<tr>
		<td>&nbsp;</td>
		<td>Kepada Yth.</td>
		<td>: <input type="text" name="SuratAtribut[penerima_memo]" value="<?php print $model->getAtribut('penerima_memo'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: <input type="text" name="SuratAtribut[pengirim_memo]" value="<?php print $model->getAtribut('pengirim_memo'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: <input type="text" name="SuratAtribut[perihal]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: <input type="text" name="SuratAtribut[lampiran]" value="<?php print $model->getAtribut('lampiran'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: <input type="text" name="SuratAtribut[tanggal_memo]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"> Dengan Hormat,</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Bersama Ini Kami Sampaikan <textarea name="SuratAtribut[isi]" rows="1" cols="40"><?php print $model->getAtribut('isi'); ?></textarea>.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Demikian disampaikan, mohon arahan lebih lanjut dan terima kasih.</td>
	</tr>	
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Jabatan" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Pengirim" type="text" name="SuratAtribut[pengirim_memo]" value="<?php print $model->getAtribut('pengirim_memo'); ?>"></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. <input placeholder="NIP Pengrim" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td style="font-weight:bold;text-decoration:underline">Tembusan Yth.:</td>
	</tr>
	<tr>
		<td>
			<textarea name="SuratAtribut[tembusan]" rows="5" cols="40"><?php print $model->getAtribut('tembusan'); ?></textarea>
		</td>
	</tr>
	
	</table>

</div>