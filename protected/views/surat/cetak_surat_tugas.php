<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT TUGAS</div>
<div style="text-align:center;">Nomor : <?php print $model->getNomor(); ?></div>

<table width="100%" style="font-family:Arial">

	<tr>
		<td width="25%">&nbsp;</td>
		<td width="3%">&nbsp;</td>
		<td width="3%">&nbsp;</td>
		<td width="69%">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold">Pertimbangan</td>
		<td>:</td>
		<td colspan="2">
			Bahwa dalam rangka <?php print $model->getAtribut('pertimbangan_nama_kegiatan'); ?>
			di <?php print $model->getAtribut('pertimbangan_tempat_kegiatan'); ?>, maka perlu
			dikeluarkan surat tugas.
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold">Dasar</td>
		<td>:</td>
		<td>1. </td>
		<td>Undang-undang Nomor 32 Tahun 2014 tentang Kelautan;	</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td>2.</td>
		<td>Peraturan Presiden Republik Indonesia Nomor 178 Tahun 2014 Tentang Badan Keamanan Laut RI;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td>3.</td>
		<td>Keputusan Presiden Nomor 44/M tahun 2015 tentang Pemberhentian dan Pengangkatan Dari dan Dalam Jabatan Kepala Badan Keamanan Laut RI;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td>4.</td>
		<td>
			Surat dari <?php print $model->getAtribut('dasar_nama_pejabat'); ?>
			Nomor <?php print $model->getAtribut('dasar_nomor_surat'); ?>
			Tanggal <?php print $model->getAtribut('dasar_tanggal_surat'); ?>
			Perihal <?php print $model->getAtribut('dasar_perihal_surat'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td>5.</td>
		<td>
			Program Kerja Bakamla Tahun <?php print $model->getAtribut('tahun_dibuat'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:center;font-weight:bold">MENUGASKAN</td>
	</tr>
	<?php
		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));
	?>	
	<?php if(count($daftar_pegawai)<=3) { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2" style="font-weight: bold">
			<ol>
				<?php foreach($daftar_pegawai as $pegawai) { ?>
				<li><?php print $pegawai; ?></li>
				<?php } ?>
			</ol>
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2"><b>DAFTAR NAMA TERLAMPIR</b></td>
	</tr>
	<?php } ?>
	<tr>
		<td style="font-weight:bold">Untuk</td>
		<td>:</td>
		<td style="vertical-align:top">1.</td>
		<td>
			Seterimanya surat ini segera mengikuti/menghadiri <?php print $model->getAtribut('untuk_nama_kegiatan'); ?>
			di <?php print $model->getAtribut('untuk_tempat_kegiatan'); ?> 
			sebagai <?php print $model->getAtribut('untuk_sebagai'); ?>;
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td style="vertical-align:top">2.</td>
		<td>
			Pelaksanaan kegiatan pada <?php print $model->getAtribut('untuk_tanggal_kegiatan'); ?>
			pukul <?php print $model->getAtribut('untuk_pukul_kegiatan'); ?>;
		</td>
	</tr>
	<?php
		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));
	?>	
	<tr>
		<td></td>
		<td></td>
		<td>3.</td>
		<td>Mempersiapkan dan koordinasi dengan pihak-pihak terkait;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>4.</td>
		<td>Melaksanakan tugas sebaik-baiknya dengan penuh rasa tanggung jawab;</td>
	</tr>		
	<tr>
		<td></td>
		<td></td>
		<td>5.</td>
		<td>
			Melaporkan pelaksanaan tugas kepada <?php print $model->getAtribut('untuk_tujuan_lapor'); ?>.
		</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:60%"></td>
		<td style="width:40%; padding-left: 4%">Dikeluarkan di : <?php print $model->getAtribut('ttd_tempat_dikeluarkan'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td style="padding-left: 4%"><u>Pada Tanggal : <?php print $model->getAtribut('ttd_tanggal_dikeluarkan'); ?></u></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>