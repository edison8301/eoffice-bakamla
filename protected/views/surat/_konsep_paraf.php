<h3>Paraf</h3>

<hr>

<table class='table' width="100%">
<thead>
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th width="40%" style="text-align:left">Jabatan</th>
	<th width="20%" style="text-align:center">Pemangku</th>
	<th width="15%" style="text-align:center">Status</th>
	<th width="20%" style="text-align:center">Waktu Disetujui</th>
	<th width="10%" style="text-align:center">Urutan Paraf</th>
	<th>&nbsp;</th>
</tr>
</thead>
<?php $i=1; foreach($model->findAllParaf() as $paraf) { ?>
<?php $class = ''; ?>
<?php if($paraf->id_paraf_status == 1) $class = "success"; ?>
<?php if($paraf->id_paraf_status == 3) $class = "info"; ?>
<?php if($paraf->id_paraf_status == 4) $class = "danger"; ?>
<?php if($paraf->id_paraf_status == 5) $class = "warning"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $i; //print $paraf->urutan; ?></td>
	<td><?php print $paraf->getNamaJabatan(); ?></td>
	<td style="text-align:center"><?php print Jabatan::getNamaPemangkuByIdJabatan($paraf->id_jabatan); ?></div>
	<td style="padding-top:13px;text-align:center">
		<?php $label = "default"; ?>
		<?php if($paraf->id_paraf_status==1) $label = "success"; ?>
		<?php if($paraf->id_paraf_status==3) $label = "primary"; ?>
		<?php if($paraf->id_paraf_status==4) $label = "danger"; ?>
		<?php if($paraf->id_paraf_status==5) $label = "warning"; ?>
		
		<span class="label label-<?php print $label; ?>">
		<?php print $paraf->getRelationField('paraf_status','nama'); ?>
		</span>
		
	</td>
	<td style="text-align:center"><?php print Helper::getTanggalWaktu($paraf->waktu_disetujui); ?></td>
	<td style="text-align:center">
		<?php $this->widget('booster.widgets.TbEditableField',array(
        		'type' => 'text',
        		'model' => $paraf,
        		'attribute' => 'urutan',
        		'url' => Yii::app()->controller->createUrl('paraf/updateEditable')
    	)); ?>
	</td>
	<td>
		<?php if($paraf->id_paraf_status!=1 AND $paraf->id_paraf_status!=5) { ?>
		<?php if($model->id_surat_status==2) { ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array("paraf/hapus","id"=>$paraf->id),array(
				"data-toggle"=>"tooltip",
				"title"=>"Hapus",
				"confirm"=>"Yakin akan menghapus paraf?"
		)); ?>
		<?php } else { ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-remove"></i>',array("paraf/lewati","id"=>$paraf->id),array(
				"data-toggle"=>"tooltip",
				"title"=>"Lewati",
				"confirm"=>"Yakin akan melewati paraf?"
		)); ?>
		<?php } ?>
		<?php } else { ?>&nbsp;<?php } ?>
	</td>
</tr>
<?php $i++; } ?>
</table>

<div>&nbsp;</div>

<?php if($model->id_surat_status==2) { ?>
<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'Tambah Paraf',		
					'context'=>'primary',
					'icon'=>'plus',
					'size'=>'small',
					'htmlOptions'=>array(
						'onClick'=>'$("#dialogPengaju").dialog("open"); return false;',
						'value'=>'add',
						'style'=>'margin-bottom:3px',
					),
	)); ?>
</div>


<div>&nbsp;</div>			

<?php //Simulasi Dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPengaju',
    'options'=>array(
        'title'=>'Daftar Pejabat',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
    $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped hover condensed',
		'dataProvider'=>Jabatan::model()->dialog(),
		'filter'=>Jabatan::model(),
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
		      	'name'=>'nama',
		      	'value'=>'$data->nama',
			),	
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-plus\"></i>",array("surat/createParaf","id"=>'.$model->id.',"id_jabatan"=>$data->id),array("data-toggle"=>"tooltip","title"=>"Pilih"))',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
        	),
		),
	));
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer

?>
<?php } ?>
