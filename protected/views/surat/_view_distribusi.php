<h3><?php print DistribusiJenis::getNamaById($jenis); ?> Surat</h3>

<div>&nbsp;</div>

<table class="table table-hover table-condensed">
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th width="55%">Pengirim</th>
	<th width="10%" style="text-align:center">Jenis</th>
	<th width="20%" style="text-align:center">Waktu Pengiriman</th>
	<th width="20%" style="text-align:center">Waktu Dilihat</th>
</tr>
<?php $i=1; foreach($model->findAllDistribusiByJenis($jenis) as $data) { ?>
<?php $class = ""; ?>
<?php if($data->waktu_dilihat==null) $class = "danger"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $i; ?></td>
	<td>
		<?php print $data->getPengirim(); ?><br>
		<i class="glyphicon glyphicon-arrow-right"></i>&nbsp;
		<?php print $data->getPenerima(); ?>
	</td>
	<td style="text-align:center"><?php print $data->getRelation("distribusi_jenis","nama"); ?></td>
	<td style="text-align:center"><?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></td>
	<td style="text-align:center"><?php print $data->waktu_dilihat; ?></td>
</tr>
<?php $i++; } ?>
</table>

<div>&nbsp;</div>