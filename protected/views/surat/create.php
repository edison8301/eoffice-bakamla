<?php
$this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/draf'),
	'Buat Draf Surat Baru',
);

?>

<h1>Buat Draf Surat Baru</h1>

<?php echo $this->renderPartial('_create', array('model'=>$model)); ?>
