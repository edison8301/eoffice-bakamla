<?php
$this->breadcrumbs=array(
	'Surat'=>array('surat/admin'),
	'Kelola',
);

$this->menu=array(
array('label'=>'List SuratKeluar','url'=>array('index')),
array('label'=>'Create SuratKeluar','url'=>array('create')),
);

?>

<h1>Data Surat</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'surat-grid',
		'type'=>'striped bordered condensed hover',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'Nomor',
				'name'=>'nomor',
				'value'=>'$data->nomor',
			),
			array(
				'header'=>'Tanggal',
				'name'=>'tanggal',
				'value'=>'Helper::getTanggalSingkat($data->tanggal)',
			),
			array(
				'header'=>'Jenis',
				'name'=>'id_surat_jenis',
				'value'=>'$data->getRelationField("surat_jenis","nama")',
				'filter'=>CHtml::listData(SuratJenis::model()->findAll(),'id','nama')
			),
			array(
				'header'=>'Waktu Terbit',
				'name'=>'waktu_diterbitkan',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Waktu Dibuat',
				'name'=>'waktu_dibuat',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header'=>'Status',
				'name'=>'id_surat_status',
				'value'=>'$data->getRelationField("surat_status","nama")',
				'filter'=>CHtml::listData(SuratStatus::model()->findAll(),'id','nama'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				'visible'=>User::isAdmin()
			),

			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'visible'=>!User::isAdmin()
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'visible'=>User::isAdmin()
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'size'=>'small',
		'label'=>'Tambah Surat',
		'icon'=>'plus',
		'url'=>array('create')
)); ?>
</div>
