<?php

$this->breadcrumbs=array(
    'Surat'=>array('admin'),
    'Laporan',
);

?>
<h1>Laporan Surat</h1>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'id'=>'laporan-form',
    'type'=>'horizontal',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Filter Laporan Surat  </p>

    <?php echo $form->errorSummary($laporanform); ?>

    <div class="form-actions well">



    <?php echo $form->datePickerGroup($laporanform,'tanggal_awal',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
            'widgetOptions'=>array(
                    'options'=>array(
                        'showAnim'=>'fold',
                        'format'=>'yyyy-mm-dd',
                        'autoclose' =>true
                ),
                    'htmlOptions'=>array(
                        'class'=>'form-control',
                        'placeholder'=>'Tanggal Awal'
                    ),
            ),      
            'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
    )); ?>    

    <?php echo $form->datePickerGroup($laporanform,'tanggal_akhir',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
            'widgetOptions'=>array(
                    'options'=>array(
                        'showAnim'=>'fold',
                        'format'=>'yyyy-mm-dd',
                        'autoclose' =>true
                ),
                    'htmlOptions'=>array(
                        'class'=>'form-control',
                        'placeholder'=>'Tanggal Akhir'
                    ),
            ),      
            'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
    )); ?>  

    <?php echo $form->select2Group($laporanform,'jenis_surat',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
            'widgetOptions'=>array(
                    'data' => CHtml::ListData(SuratJenis::model()->findAll(),'id','nama'),
                    'htmlOptions'=>array(
                        'class'=>'form-control',
                        'placeholder'=>'jenis_surat'
                    ),
            ),      
            'prepend'=>'<i class="glyphicon glyphicon-list"></i>'
    )); ?> 
</div>    

    <div class="form-actions well" style='text-align:right'>
    <?php $this->widget('booster.widgets.TbButton',array(
        'buttonType'=>'submit',
        'context'=>'primary',
        'label'=>'Proses',
        'icon'=>'ok',
    )); ?>&nbsp;
    </div>

    
<?php $this->endWidget(); ?>