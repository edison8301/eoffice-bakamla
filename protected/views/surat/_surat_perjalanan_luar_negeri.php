<?php $tab = ''; for($i=1;$i<=10;$i++) $tab .= '&nbsp;'; ?>	
<div style="border:1px solid #000;padding:20px">

	<div>&nbsp;</div>

	<table width="100%" class="surat">
	<tr>

		<td colspan="2">Nomor</td>
		<td>:</td>
		<td><?php print $model->getNomor(); ?>
		</td>
		<td>Jakarta <input placeholder="tanggal" type="text" size="4" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>">
					<input placeholder="bulan" type="text" size="4" name="SuratAtribut[bulan]" value="<?php print $model->getAtribut('bulan'); ?>">
					<input placeholder="tahun" type="text" size="4" name="SuratAtribut[tahun]" value="<?php print $model->getAtribut('tahun'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">Sifat</td>
		<td>:</td>
		<td><input placeholder="Sifat Surat" type="text" name="SuratAtribut[sifat]" value="<?php print $model->getAtribut('sifat'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">Lampiran</td>
		<td>:</td>
		<td><input type="text" placeholder="Lampiran Surat" name="SuratAtribut[jumlah_lampiran]" value="<?php print $model->getAtribut('jumlah_lampiran'); ?>"></td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td colspan="2">Perihal</td>
		<td>:</td>
		<td><input type="text" placeholder="Perihal Surat" name="SuratAtribut[perihal]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
		<td width="260" style="font-weight: bold">SEKRETARIS KEMENTRIAN<br>SEKRETARIS NEGARA</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td>di</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td style="text-align: center"><u>Jakarta</u></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table>
	
	<tr>
		<td width="40px">&nbsp;</td>
		<td colspan="2" style="font-weight: bold">Up. Kepala Biro KTLN</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?php print $tab; ?>Berdasarkan Surat dari 
							<input placeholder="Dasar Surat" type="text" name="SuratAtribut[dasar_surat]" value="<?php print $model->getAtribut('dasar_surat'); ?>">
						Nomor : 
							<input placeholder="Nomor Surat" type="text" name="SuratAtribut[no_surat]" value="<?php print $model->getAtribut('no_surat'); ?>">
						tanggal 
							<input type="text" placeholder="Tanggal Surat" name="SuratAtribut[tanggal_dasar]" value="<?php print $model->getAtribut('tanggal_dasar'); ?>"> Perihal  : <input type="text" placeholder="Perihal Surat" name="SuratAtribut[perihal_dasar]" value="<?php print $model->getAtribut('perihal_dasar'); ?>">
						terkait pertemuan tersebut,  maka disampaikan dengan hormat bahwa 
							<input placeholder="Staf Pejabat" type="text" name="SuratAtribut[staf_pejabat]" value="<?php print $model->getAtribut('staf_pejabat'); ?>"> 
						Bakamla menurut rencana akan menghadiri 
						undangan dimaksud.
		</td>
		
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?php print $tab; ?>Sehubungan hal tersebut di atas, kami mohon dengan hormat kiranya Bapak dapat memberikan persetujuan kepada 
				<input placeholder="Staf Pejabat" type="text" name="SuratAtribut[staf_pejabat_pusat]" value="<?php print $model->getAtribut('staf_pejabat_pusat'); ?>"> 
			Bakamla untuk melaksanakan tugas ke luar negeri 
				<input placeholder="Nama Negara" type="text" name="SuratAtribut[negara]" value="<?php print $model->getAtribut('negara'); ?>">
			pada
				<input placeholder="Tanggal mulai" type="text" name="SuratAtribut[tanggal_mulai]" value="<?php print $model->getAtribut('tanggal_mulai'); ?>">  
			s.d. 
				<input placeholder="tanggal Selesai" type="text" name="SuratAtribut[tanggal_selesai]" value="<?php print $model->getAtribut('tanggal_selesai'); ?>">:
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>: <input type="text" placeholder="Nama Pegawai" name="SuratAtribut[nama]" value="<?php print $model->getAtribut('nama'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <input type="text" placeholder="Jabatan Pegawai" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>

	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>: <input type="text" placeholder="NIP Pegawai" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
		
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NO. Paspor</td>
		<td>: <input type="text" placeholder="Nomor Passpor" name="SuratAtribut[paspor]" value="<?php print $model->getAtribut('paspor'); ?>"></td>
		
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">
			<?php print $tab; ?>Adapun dapat kami sampaikan bahwa akomodasi di tanggung oleh 
				<input placeholder="Nama Instansi yang Menanggung" style="width: 40%" type="text" name="SuratAtribut[instansi]" value="<?php print $model->getAtribut('instansi'); ?>">
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">
			<?php print $tab; ?>Demikian atas perhatian dan kerjasama yang baik, kami mengucapkan terima kasih.
		</td>
	</tr>			

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:66%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>