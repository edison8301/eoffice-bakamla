<div style="border:1px solid #000;padding:20px">
	<table class="surat" width="100%">
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">Nomor Bagian Kepegawaian</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input placeholder="Nomor Bagian Kepegawaian" size="25" type="text" name="SuratAtribut[surat_nomor_bag_pegawai]" value="<?php print $model->getAtribut('surat_nomor_bag_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">Tanggal</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input type="text" placeholder="tanggal" size="25" name="SuratAtribut[surat_tanggal]" value="<?php print $model->getAtribut('surat_tanggal'); ?>"></td>
	</tr>
	<tr>		
	</table>
	<div>&nbsp;</div>

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KETERANGAN PULANG SEBELUM WAKTUNYA</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>1.</td>
		<td colspan="3">Yang Bertanda Tangan di Bawah ini:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Atasan" type="text" size="40" name="SuratAtribut[pejabat_nama]" value="<?php print $model->getAtribut('pejabat_nama'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="NIP/NRP Atasan" type="text" name="SuratAtribut[pejabat_nip]" value="<?php print $model->getAtribut('pejabat_nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Atasan" type="text" name="SuratAtribut[pejabat_jabatan]" value="<?php print $model->getAtribut('pejabat_jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Unit Kerja Atasan" type="text" name="SuratAtribut[pejabat_unit_kerja]" value="<?php print $model->getAtribut('pejabat_unit_kerja'); ?>"></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td colspan="3">Dengan ini Menerangkan Bahwa</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama yang pulang sebelum waktunya" type="text" name="SuratAtribut[pegawai_nama]" value="<?php print $model->getAtribut('pegawai_nama'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="NIP" type="text" name="SuratAtribut[pegawai_nip]" value="<?php print $model->getAtribut('pegawai_nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan" type="text" name="SuratAtribut[pegawai_jabatan]" value="<?php print $model->getAtribut('pegawai_jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Unit Kerja" type="text" name="SuratAtribut[pegawai_unit_kerja]" value="<?php print $model->getAtribut('pegawai_unit_kerja'); ?>"></td>
	</tr>	
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>			
	<tr>
		<td></td>
		<td colspan="3">Pada Hari
				<input placeholder="Hari" type="text" name="SuratAtribut[surat_hari]" value="<?php print $model->getAtribut('surat_hari'); ?>">
				 Tanggal
				<input placeholder="Tanggal" type="text" name="SuratAtribut[surat_tanggal_inti]" value="<?php print $model->getAtribut('surat_tanggal_inti'); ?>">, 
				Pukul 
				<input placeholder="Pukul" type="text" name="SuratAtribut[surat_pukul]" value="<?php print $model->getAtribut('surat_pukul'); ?>">
					<b>Pulang Sebelum Waktunya</b> Karena <textarea placeholder="Alasan Pulang sebelum waktunya" name="SuratAtribut[surat_isi]" rows="1" cols="40"><?php print $model->getAtribut('surat_isi'); ?></textarea> Karena Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.	</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	


	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">Jakarta, <input placeholder="Tanggal" type="text" name="SuratAtribut[tanggal_lengkap]" value="<?php print $model->getAtribut('tanggal_lengkap'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Nama Atasasn" type="text" name="SuratAtribut[tandatangan_nama_pejabat]" value="<?php print $model->getAtribut('tandatangan_nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="NIP Atasan" type="text" name="SuratAtribut[tandatangan_nip_pejabat]" value="<?php print $model->getAtribut('tandatangan_nip_pejabat'); ?>"></td>
	</tr>
	</table>
	
</div>