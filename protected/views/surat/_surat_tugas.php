<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT TUGAS</div>
	<div style="text-align:center;font-weight:bold">Nomor: <?php print $model->getNomor(); ?></div>
	
	<div>&nbsp;</div>

	<table width="100%" class="surat">
	<tr>
		<td width="10%">&nbsp;</td>
		<td width="5%">&nbsp;</td>
		<td width="5%">&nbsp;</td>
		<td width="80%">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold;vertical-align:top">Pertimbangan</td>
		<td style="vertical-align:top" width="10px">:</td>
		<td colspan="2">Bahwa dalam rangka 
			<input type="text" name="SuratAtribut[pertimbangan_nama_kegiatan]" value="<?php print $model->getAtribut('pertimbangan_nama_kegiatan'); ?>" placeholder="nama kegiatan">
			di 
			<input type="text" name="SuratAtribut[pertimbangan_tempat_kegiatan]" value="<?php print $model->getAtribut('pertimbangan_tempat_kegiatan'); ?>" placeholder="nama daerah kegiatan">, maka perlu dikeluarkan Surat Tugas.
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight: bold;vertical-align:top">Dasar</td>
		<td style="vertical-align:top">:</td>
		<td style="vertical-align:top">1.</td>
		<td>Undang-undang Nomor 32 Tahun 2014 tentang Kelautan;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">2.</td>
		<td style="vertical-align:top">Peraturan Presiden Republik Indonesia Nomor 178 Tahun 2014 Tentang Badan Keamanan Laut RI;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">3.</td>
		<td>Keputusan Presiden Nomor 44/M tahun 2015 tentang Pemberhentian dan Pengangkatan Dari dan Dalam Jabatan Kepala Badan Keamanan Laut RI;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">4.</td>
		<td>Surat dari <input type="text" style="width: 40%" name="SuratAtribut[dasar_nama_pejabat]" size="30" value="<?php print $model->getAtribut('dasar_nama_pejabat'); ?>" placeholder="pejabat yang menandatangan surat">
			Nomor : <input type="text" name="SuratAtribut[dasar_nomor_surat]" size="30" value="<?php print $model->getAtribut('dasar_nomor_surat'); ?>" placeholder="nomor surat">
			Tanggal <input type="text" name="SuratAtribut[dasar_tanggal_surat]" value="<?php print $model->getAtribut('dasar_tanggal_surat'); ?>" placeholder="tanggal surat"> 
			Perihal :<input type="text" name="SuratAtribut[dasar_perihal_surat]" size="30" value="<?php print $model->getAtribut('dasar_perihal_surat'); ?>" placeholder="perihal surat">
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">5.</td>
		<td>Program Kerja Bakamla Tahun <input type="text" name="SuratAtribut[dasar_tahun]" value="<?php print $model->getAtribut('dasar_tahun'); ?>" placeholder="tahun program kerja">.
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align: center; font-weight: bold">MENUGASKAN</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td style="font-weight:bold;vertical-align:top">Kepada</td>
		<td style="vertical-align:top">:</td>
		<td colspan="2"><textarea name="SuratAtribut[kepada_pegawai]" style="width:100%" rows="5" placeholder="Pangkat Maritim, Nama huruf Kapital, Gelar (Jabatan dalam Organisasi)"><?php print $model->getAtribut('kepada_pegawai'); ?></textarea>
			<br><span style="font-weight:bold">Keterangan:</span> Satu pegawai per baris
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold;vertical-align:top">Untuk</td>
		<td style="vertical-align:top">:</td>
		<td style="vertical-align:top">1.</td>
		<td>
			Seterimanya surat ini segera mengikuti/menghadiri <input type="text" name="SuratAtribut[untuk_nama_kegiatan]" value="<?php print $model->getAtribut('untuk_nama_kegiatan'); ?>" placeholder="nama kegiatan">
			di <input type="text" name="SuratAtribut[untuk_tempat_kegiatan]" value="<?php print $model->getAtribut('untuk_tempat_kegiatan'); ?>" placeholder="tempat kegiatan"> 
			sebagai <input type="text" style="width: 40%" name="SuratAtribut[untuk_sebagai]" value="<?php print $model->getAtribut('untuk_sebagai'); ?>" placeholder="sesuai perintah kegiatan">;	
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td style="vertical-align:top">2.</td>
		<td>
			Pelaksanaan kegiatan pada <input type="text" name="SuratAtribut[untuk_tanggal_kegiatan]" value="<?php print $model->getAtribut('untuk_tanggal_kegiatan'); ?>" placeholder="tanggal kegiatan">
			pukul <input type="text" name="SuratAtribut[untuk_pukul_kegiatan]" value="<?php print $model->getAtribut('untuk_pukul_kegiatan'); ?>" placeholder="pukul/jam kegiatan">;
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">3.</td>
		<td>Mempersiapkan dan berkoordinasi dengan pihak-pihak terkait;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">4.</td>
		<td>Melaksanakan tugas sebaik-baiknya dengan penuh rasa tanggung jawab;</td>
	</tr>	
	<tr>
		<td colspan="2"></td>
		<td style="vertical-align:top">5.</td>
		<td>
			Melaporkan pelaksanaan tugas kepada <input type="text" style="width: 40%" name="SuratAtribut[untuk_tujuan_lapor]" value="<?php print $model->getAtribut('untuk_tujuan_lapor'); ?>" placeholder="Presiden/Kepala/Sestama">;
			<br><span style="font-weight:bold">Keterangan:</span> Presiden (jika yang berangkat Kepala Bakamla), Kepala Bakamla (jika yang berangkat eselon I), Sestama Bakamla (jika yang berangkat eselon II kebawah).
		</td>
	</tr>
	</table>



	<div>&nbsp;</div>
	

	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td>Dikeluarkan di : <input type="text" name="SuratAtribut[ttd_tempat_dikeluarkan]" value="<?php print $model->getAtribut('ttd_tempat_dikeluarkan'); ?>" placeholder="tempat"></td>
	</tr>	
	<tr>
		<td style="width:50%"></td>
		<td>Pada Tanggal : <input type="text" name="SuratAtribut[ttd_tanggal_dikeluarkan]" value="<?php print $model->getAtribut('ttd_tanggal_dikeluarkan'); ?>" placeholder="tanggal"></td>
	</tr>		
	<tr>	
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;"><span style="font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</span></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;"><span style="font-weight:bold">SEKRETARIS UTAMA</span></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;"><b>DICKY R. MUNAF</b></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;"><b>NIP. 19610527 198503 1 001</b></td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>