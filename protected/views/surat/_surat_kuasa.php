<style>
	td{
		vertical-align: top;
	}

</style>

<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KUASA</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td></td>
		<td colspan="3">Yang Bertanda Tangan di Bawah Ini</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td width="200" >Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pemberi Kuasa" size="40" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP</td>
		<td>:</td>
		<td><input placeholder="NIP Pemberi Kuasa" size="40"  type="text" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Pemberi Kuasa" size="40" type="text" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Alamat</td>
		<td>:</td>
		<td><textarea placeholder="Alamat Pemberi Kuasa" size="40" rows="1" cols="80" name="SuratAtribut[alamat_pejabat]"><?php print $model->getAtribut('alamat_pejabat'); ?></textarea></td>
	</tr>	
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" rowspan="2">Dengan ini memberi Surat Kuasa Kepada :</td>
	</tr>	
	<tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Alamat Penerima Kuasa" size="40" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP</td>
		<td>:</td>
		<td><input placeholder="NIP Penerima Kuasa" size="40" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Penerima Kuasa" size="40" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Unit Kerja Penerima Kuasa" size="40" type="text" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>	
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>Untuk</td>
		<td>:</td>
		<td><textarea placeholder="Tujuan Memberikan Kuasa" rows="1" cols="80" name="SuratAtribut[untuk]"><?php print $model->getAtribut('untuk'); ?></textarea></td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">Demikian Surat Kuasa ini dibuat dengan sebenarnya untuk digunakan sebagaimana mestinya. </td>
	</tr>
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>		
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat" >
	<tr>
		<td style="width:50%"></td>
		<td>Dikeluarkan Di : <input placeholder="Tempat Surat Dikeluarkan" size="25" type="text" name="SuratAtribut[tempat_dikeluarkan]" value="<?php print $model->getAtribut('tempat_dikeluarkan'); ?>"></td>
	</tr>	
	<tr>
		<td style="width:50%"></td>
		<td>Pada Tanggal : <input placeholder="Tanggal Surat Dikeluarkan" size="25" type="date" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>"></td>
	</tr>		
	<tr>
		<th style="width:50%">Yang Menerima Kuasa</th>
		<th style="width:50%" style="text-align:center;font-weight:bold">Yang Memberi Kuasa</th>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"><input placeholder="Nama Penerima Kuasa" type="text" name="SuratAtribut[menerima_kuasa]" value="<?php print $model->getAtribut('menerima_kuasa'); ?>"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"> NIP./NRP. <input placeholder="NIP Penerima Kuasa" type="text" name="SuratAtribut[nip_penerima_kuasa]" value="<?php print $model->getAtribut('nip_penerima_kuasa'); ?>"></td></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>