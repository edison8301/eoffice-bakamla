<div style="padding:0px 117px;margin-bottom:20px">
	<div class="row well">
		<label class="col-sm-2 control-label">Kop Surat</label>
		<div class="col-sm-8">
		<?php echo $form->dropDownList($model,'id_surat_kop',CHtml::listData(SuratKop::model()->findAll(),'id','nama'),array('class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php $this->widget('booster.widgets.TbButton',array(
					'buttonType'=>'submit',
					'label'=>'Submit',
					'context'=>'primary',
					'icon'=>'ok',
					'size'=>'sm',
			)); ?>
		</div>
	</div>
</div>

<div style="padding:0px 117px">
<?php 
	if($model->id_surat_jenis==1) $this->renderPartial('_surat_keputusan',array('model'=>$model));
	if($model->id_surat_jenis==2) $this->renderPartial('_surat_perintah',array('model'=>$model));
	if($model->id_surat_jenis==3) $this->renderPartial('_surat_tugas',array('model'=>$model));
	if($model->id_surat_jenis==4) $this->renderPartial('_surat_dinas',array('model'=>$model));
	if($model->id_surat_jenis==5) $this->renderPartial('_surat_perjalanan_luar_negeri',array('model'=>$model));
	if($model->id_surat_jenis==6) $this->renderPartial('_surat_edaran',array('model'=>$model)); 
	if($model->id_surat_jenis==7) $this->renderPartial('_surat_izin',array('model'=>$model)); 
	if($model->id_surat_jenis==8) $this->renderPartial('_keterangan',array('model'=>$model)); 
	if($model->id_surat_jenis==9) $this->renderPartial('_surat_keterangan_jalan',array('model'=>$model)); 
	if($model->id_surat_jenis==10) $this->renderPartial('_surat_kuasa',array('model'=>$model)); 
	if($model->id_surat_jenis==11) $this->renderPartial('_surat_pernyataan',array('model'=>$model)); 
	if($model->id_surat_jenis==12) $this->renderPartial('_surat_cuti',array('model'=>$model)); 
	if($model->id_surat_jenis==13) $this->renderPartial('_surat_undangan',array('model'=>$model)); 
	if($model->id_surat_jenis==14) $this->renderPartial('_surat_terlambat',array('model'=>$model)); 
	if($model->id_surat_jenis==15) $this->renderPartial('_surat_keterangan_pulang',array('model'=>$model)); 	
	if($model->id_surat_jenis==16) $this->renderPartial('_surat_tidak_masuk',array('model'=>$model)); 	
	if($model->id_surat_jenis==17) $this->renderPartial('_memo',array('model'=>$model)); 		
	if($model->id_surat_jenis==18) $this->renderPartial('_agno',array('model'=>$model));
	if($model->id_surat_jenis==19) $this->renderPartial('_memo_kepala',array('model'=>$model));
	if($model->id_surat_jenis==20) $this->renderPartial('_memo_sestama',array('model'=>$model)); 			
?>
</div>
	
<div>&nbsp;</div>

	

