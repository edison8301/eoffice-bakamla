<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT KETERANGAN</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>

<table class="surat">
	<tr>
		<td width="5%">&nbsp;</td>
		<td colspan="3">Yang bertanda tangan di bawah ini :</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="10%">Nama</td>
		<td width="4%">:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('pangkat_golongan_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><?php print $model->getAtribut('unit_kerja_pejabat'); ?></td>
	</tr>

	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">Dengan ini Menerangkan Bahwa :</td>
	</tr>
	
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td>:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('pangkat_golongan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><?php print $model->getAtribut('unit_kerja'); ?></td>
	</tr>	
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td></td>
		<td colspan="3">Adalah benar Pegawai Badan Keamanan Laut Republik Indonesia dengan Nama dan Jabatan sebagaimana tersebut di atas yang beralamatkan di <?php print $model->getAtribut('alamat_instansi'); ?> .</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>