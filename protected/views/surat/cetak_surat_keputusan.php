<style>
td {
	vertical-align:top;
}
</style>
	<div style="text-align:center;font-weight:bold;font-size:14px">KEPUTUSAN</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
			<?php print $model->getAtribut('nama_pejabat'); ?>
	<div style="text-align:center;font-weight:bold;font-size:14px">BADAN KEAMANAN LAUT RI</div>
	</div>
	<div>&nbsp;</div>
	<div style="text-align:center; font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>
	<div>&nbsp;</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">TENTANG</div>
	<div>&nbsp;</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
			<?php print $model->getAtribut('tujuan_skep'); ?>
	</div>
	<div>&nbsp;</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
			<?php print $model->getAtribut('pejabat_penandatangan'); ?>
	</div>
	
	<div>&nbsp;</div>

	<table style="font-weight: bold" width="100%" class="surat">
	<tr>
		<td width="3%">&nbsp;</td>
		<td>Menimbang</td>
		<td width="3%">: </td>
		<td width="66%"><?php print $model->getAtribut('menimbang'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Mengingat</td>
		<td>: </td>
		<td><?php print $model->getAtribut('mengingat'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Memperhatikan</td>
		<td>: </td>
		<td><?php print $model->getAtribut('memperhatikan'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Menetapkan</td>
		<td>: </td>
		<td>KEPUTUSAN 
				<?php print $model->getAtribut('nama_pejabat_inti'); ?>
			TENTANG
				<?php print $model->getAtribut('tujuan_skep_inti'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>PERTAMA</td>
		<td>: </td>
		<td><?php print $model->getAtribut('isi_skep'); ?> </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>KEDUA</td>
		<td>: </td>
		<td><?php print $model->getAtribut('jangka_waktu'); ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>KETIGA</td>
		<td>: </td>
		<td colspan="3">Keputusan ini berlaku sejak tanggal ditetapkan</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td>&nbsp;</td>
		<td colspan="3"> SALINAN Keputusan ini disampaikan kepada Yth.<br>
			<?php print $model->getAtribut('disampaikan'); ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td>&nbsp;</td>
		<td style="font-weight: none !important;" colspan="3">PETIKAN Keputusan ini disampaikan kepada yang bersangkutan untuk digunakan sebagaimana mestinya.</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	</table>