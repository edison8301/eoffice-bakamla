<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT PERINTAH</div>
<div style="text-align:center;">Nomor : <?php print $model->getNomor(); ?></div>

<table width="100%" style="font-family:Arial">

	<tr>
		<td width="25%">&nbsp;</td>
		<td width="3%">&nbsp;</td>
		<td width="3%">&nbsp;</td>
		<td width="69%">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold">Pertimbangan</td>
		<td>:</td>
		<td colspan="2">
			Bahwa dalam rangka <?php print $model->getAtribut('pertimbangan_nama_kegiatan'); ?>
			di <?php print $model->getAtribut('pertimbangan_tempat_kegiatan'); ?>, maka perlu
			dikeluarkan surat perintah.
		</td>
	</tr>
	<?php $i=1; ?>
	<tr>
		<td style="font-weight:bold">Dasar</td>
		<td>:</td>
		<td><?php print $i; $i++; ?>. </td>
		<td>Undang-undang Nomor 32 Tahun 2014 tentang Kelautan;	</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td><?php print $i; $i++; ?>.</td>
		<td>Peraturan Presiden Republik Indonesia Nomor 178 Tahun 2014 Tentang Badan Keamanan Laut RI;</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td><?php print $i; $i++; ?>.</td>
		<td>Keputusan Presiden Nomor 44/M tahun 2015 tentang Pemberhentian dan Pengangkatan Dari dan Dalam Jabatan Kepala Badan Keamanan Laut RI;</td>
	</tr>
	<?php $list_dasar_nama_pejabat = explode(';',$model->getAtribut('dasar_nama_pejabat')); ?>
	<?php $list_dasar_nomor_surat = explode(';',$model->getAtribut('dasar_nomor_surat')); ?>
	<?php $list_dasar_tanggal_surat = explode(';',$model->getAtribut('dasar_tanggal_surat')); ?>
	<?php $list_dasar_perihal_surat = explode(';',$model->getAtribut('dasar_perihal_surat')); ?>
	<?php if(count($list_dasar_nama_pejabat)!=0 AND $model->getAtribut('dasar_nama_pejabat')!='') { ?>
	<?php $j=0; foreach($list_dasar_nama_pejabat as $dasar_nama_pejabat) { ?>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td><?php print $i; $i++; ?>.</td>
		<td>
			Surat dari <?php print $list_dasar_nama_pejabat[$j]; ?>
			Nomor <?php print $list_dasar_nomor_surat[$j]; ?>
			Tanggal <?php print $list_dasar_tanggal_surat[$j]; ?>
			Perihal <?php print $list_dasar_perihal_surat[$j]; ?>
		</td>
	</tr>
	<?php $j++; } ?>
	<?php } ?>
	<tr>
		<td colspan="2"></td>
		<td><?php print $i; $i++; ?>.</td>
		<td>
			Program Kerja Bakamla Tahun <?php print $model->getAtribut('tahun_dibuat'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:center;font-weight:bold">MEMERINTAHKAN</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<?php
		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));
	?>	
	<?php if(count($daftar_pegawai)==0) { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2">&nbsp;</td>
	</tr>
	<?php } elseif(count($daftar_pegawai)==1) { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2">
			<?php foreach($daftar_pegawai as $pegawai) { ?>
			<?php $data_pegawai = explode(';',$pegawai); ?>
			<b><?php print $data_pegawai[0]; ?></b><br><?php print $data_pegawai[1]; ?>
			<?php } ?>
		</td>
	</tr>
	<?php } elseif(count($daftar_pegawai)<=3) { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2">
			<ol>
				<?php foreach($daftar_pegawai as $pegawai) { ?>
				<?php $data_pegawai = explode(';',$pegawai); ?>
				<li><b><?php print $data_pegawai[0]; ?></b><br>&nbsp;&nbsp;&nbsp;<?php print $data_pegawai[1]; ?></li>
				<?php } ?>
			</ol>
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td style="font-weight: bold">Kepada</td>
		<td>:</td>
		<td colspan="2"><b>DAFTAR NAMA TERLAMPIR</b></td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold">Untuk</td>
		<td>:</td>
		<td style="vertical-align:top">1.</td>
		<td>
			Seterimanya surat ini segera mengikuti/menghadiri <?php print $model->getAtribut('untuk_nama_kegiatan'); ?>
			di <?php print $model->getAtribut('untuk_tempat_kegiatan'); ?> 
			sebagai <?php print $model->getAtribut('untuk_sebagai'); ?>;
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td style="vertical-align:top">2.</td>
		<td>
			Pelaksanaan kegiatan pada <?php print $model->getAtribut('untuk_tanggal_kegiatan'); ?>
			pukul <?php print $model->getAtribut('untuk_pukul_kegiatan'); ?>;
		</td>
	</tr>
	<?php
		$daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai'));
	?>	
	<?php if(count($daftar_pegawai)<=3) { ?>
	<tr>
		<td></td>
		<td></td>
		<td style="vertical-align:top">3.</td>
		<td>
			
			Berangkat 
			<?php if($model->getAtribut('untuk_tempat_berangkat')!='') { ?>
			dari <?php print $model->getAtribut('untuk_tempat_berangkat'); ?> 
			<?php } ?>
			tanggal <?php print $model->getAtribut('untuk_tanggal_berangkat'); ?>
			dan kembali 
			<?php if($model->getAtribut('untuk_tempat_kembali')!='') { ?>
			ke <?php print $model->getAtribut('untuk_tempat_kembali'); ?>
			<?php } ?>
			tanggal <?php print $model->getAtribut('untuk_tanggal_kembali'); ?>;
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td></td>
		<td></td>
		<td style="vertical-align:top">3.</td>
		<td>
			Berangkat dan kembali sesuai daftar terlampir;
		</td>
	</tr>


	<?php } ?>	
	<tr>
		<td></td>
		<td></td>
		<td>4.</td>
		<td>Melakukan persiapan dan koordinasi dengan pihak-pihak terkait;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>5.</td>
		<td>Melaksanakan tugas sebaik-baiknya dengan penuh rasa tanggung jawab;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>6.</td>
		<td>Akomodasi dan tiket perjalanan dibiayai oleh <?php print $model->getAtribut('sumber_anggaran'); ?>;</td>
	</tr>		
	<tr>
		<td></td>
		<td></td>
		<td>7.</td>
		<td>
			Melaporkan pelaksanaan tugas kepada <?php print $model->getAtribut('untuk_tujuan_lapor'); ?>.
		</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:60%"></td>
		<td style="width:40%; padding-left: 30px;">Dikeluarkan di : <?php print $model->getAtribut('ttd_tempat_dikeluarkan'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td style="padding-left: 30px;"><u>Pada Tanggal : <?php print $model->getAtribut('ttd_tanggal_dikeluarkan'); ?></u></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>