<style>
td {
	vertical-align:top;
	font-size: 14px;
}

p {
	text-indent: 50px;
}
</style>

<div style="font-weight:bold;text-align:center; font-size: 14px;">MEMO</div>
<div style="text-align:center; font-size: 14px;"><span style="border-top:1px solid #000000">Nomor : <?php print $model->getNomor(); ?></span></div>

<div>&nbsp;</div>

<table width="100%">
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="20%">Kepada Yth.</td>
		<td width="75%">: <b><?php print $model->getAtribut('penerima_memo'); ?></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: <?php print $model->getAtribut('pengirim_memo'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: <b><?php print $model->getAtribut('perihal'); ?></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: <?php print $model->getAtribut('lampiran'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: <?php print $model->getAtribut('tanggal_memo'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2" style="border-bottom:1px solid #000000">&nbsp;</td>
	</tr>
</table>

<div>&nbsp;</div>


<?php $tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; ?>

<?php 
	$isi = $model->getAtribut('isi'); 
	$isi = str_replace('[tab]',$tab, $isi);
?>
			
<div style="width:100%">
	<div style="width:25%;float:left;font-size:">&nbsp;</div>
	<div style="width:75%;float:left;font-size:14px"><?php print $tab; ?>Dengan hormat,</div>
</div>

<div>&nbsp;</div>

<div style="width:100%">
	<div style="width:25%;float:left">&nbsp;</div>
	<div style="width:75%;float:left;font-size:14px"><?php print $isi; ?></div>
</div>

<div>&nbsp;</div>

<table width="100%">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">
			<?php print $model->getAtribut('jabatan'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><span style="border-bottom:1px solid #000;padding-bottom:2px"><?php print $model->getAtribut('pengirim_memo_tandatangan'); ?></span></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. <?php print $model->getAtribut('nip_tandatangan'); ?></td>
	</tr>
	</table>
	
<div>&nbsp;</div>


<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>