<style>
	tr{
		vertical-align: top;
	}
</style>

<div style="border:1px solid #000;padding:20px">
	<table class="surat" width="100%">
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">NOMOR BAGIAN KEPEGAWAIAN</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input placeholder="Nomor Bag Kepegawaian" type="text" name="SuratAtribut[nomor_bag_pegawai]" value="<?php print $model->getAtribut('nomor_bag_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="70%">&nbsp;</td>	
		<td style="text-align: center">TANGGAL</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align: center"><input placeholder="Tanggal" type="text" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>"></td>
	</tr>
	<tr>		
	</table>

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KETERANGAN TERLAMBAT</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>1.</td>
		<td colspan="3">Yang Bertanda Tangan di Bawah ini:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input type="text" placeholder="Nama Atasan" size="40" name="SuratAtribut[nama_pegawai_pejabat]" value="<?php print $model->getAtribut('nama_pegawai_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input type="text" placeholder="NIP/NRP" size="40" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input type="text" placeholder="Jabatan" size="40" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input type="text" placeholder="Unit Kerja" size="40" name="SuratAtribut[unit_kerja_pejabat]" value="<?php print $model->getAtribut('unit_kerja_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td colspan="3">Dengan ini Menerangkan Bahwa</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input type="text" placeholder="Nama Pegawai Yang Terlambat" size="40" name="SuratAtribut[nama]" value="<?php print $model->getAtribut('nama'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input type="text" placeholder="NIP/NRP" size="40" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input type="text" placeholder="Jabatan" size="40" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input type="text" placeholder="Unit Kerja" size="40" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>	
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>			
	<tr>
		<td></td>
		<td colspan="3">Pada Hari
				<input placeholder="Hari Pegawai Tersebut terlambat"  size="30" type="text" name="SuratAtribut[hari]" value="<?php print $model->getAtribut('hari'); ?>">
				 Tanggal
				<input placeholder="Tanggal Pegawai Tersebut Terlambat"  size="30" type="text" name="SuratAtribut[tanggal_inti]" value="<?php print $model->getAtribut('tanggal_inti'); ?>">, 
				Pukul 
				<input placeholder="Waktu Pejabat Tersebut Terlambat"  size="30" type="text" name="SuratAtribut[pukul]" value="<?php print $model->getAtribut('pukul'); ?>">
					<b>Datang Terlambat</b> Karena <textarea placeholder="Alasan pegawai tersebut terlambat" name="SuratAtribut[isi]" rows="1" cols="40"><?php print $model->getAtribut('isi'); ?></textarea> Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.	</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	


	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">Jakarta, <input placeholder="Tanggal" type="text" name="SuratAtribut[tanggal_lengkap]" value="<?php print $model->getAtribut('tanggal_lengkap'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Nama Atasasn" type="text" name="SuratAtribut[nama_pejabat_tandatangan]" value="<?php print $model->getAtribut('nama_pejabat_tandatangan'); ?>"></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="NIP Atasan" type="text" name="SuratAtribut[nip_pejabat_tandatangan]" value="<?php print $model->getAtribut('nip_pejabat_tandatangan'); ?>"></td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>