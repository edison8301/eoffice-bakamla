<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT PERNYATAAN</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>


<table width="100%" class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="3">Yang bertanda tangan di bawah ini :</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="20%">Nama</td>
		<td width="1%">:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('alamat_pejabat'); ?></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">Dengan ini menyatakan dengan sesungguhnya bahwa <?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>

	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	

	<tr>
		<td></td>
		<td>
			<ol>
			<?php
				$berubah = array();
				$data = explode(PHP_EOL,$model->getAtribut('poin_bebas_masalah'));

				$berubah = array_merge($berubah,$data);
				$no =1;
				foreach ($berubah as $daftar) 
				{ ?>
					
						<li><?php print $daftar; ?></li>
					
				<?php $no++; }
			?>
			</ol>
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">Demikian Surat pernyataan ini dubuat dengan sesungguhnya untuk dipergunakan  sebagaimana mestinya.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 65px;">Dikeluarkan Di :<?php print $model->getAtribut('tempat_dikeluarkan'); ?></td>
	</tr>
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 65px;">Pada Tanggal :<?php print $model->getAtribut('tanggal'); ?></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td colspan="2" style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td colspan="2" style="width:50%" style="text-align:center;font-weight:bold"><u>SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td colspan="2" style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td colspan="2" style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>