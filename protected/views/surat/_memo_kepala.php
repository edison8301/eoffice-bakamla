<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">MEMO KEPALA</div>
	<div style="text-align:center;font-weight:bold"><?php $model->setNomor(); print $model->nomor; ?></div>
	
	<div>&nbsp;</div>

	<table class="surat">

	<tr>
		<td>&nbsp;</td>
		<td>Kepada Yth.</td>
		<td>: <input placeholder="Penerima Memo" type="text" name="SuratAtribut[penerima_memo]" value="<?php print $model->getAtribut('penerima_memo'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: <input placeholder="Pengirim Memo" type="text" name="SuratAtribut[pengirim_memo]" value="<?php print $model->getAtribut('pengirim_memo'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: <input placeholder="Hal yang ingin disampaikan" type="text" name="SuratAtribut[perihal]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: <input placeholder="Jumlah Lampiran" type="text" name="SuratAtribut[lampiran]" value="<?php print $model->getAtribut('lampiran'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: <input placeholder="Tanggal/Bulan/Tahun Memo Dibuat" type="text" name="SuratAtribut[tanggal_memo]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"> Dengan Hormat,</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Bersama Ini Kami Sampaikan <textarea placeholder="Isi Memo" name="SuratAtribut[isi]" rows="1" cols="40"><?php print $model->getAtribut('isi'); ?></textarea>.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Demikian disampaikan, mohon arahan lebih lanjut dan terima kasih.</td>
	</tr>	
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Jabatan" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><input placeholder="Pengirim" type="text" name="SuratAtribut[pengirim_memo_tandatangan]" value="<?php print $model->getAtribut('pengirim_memo_tandatangan'); ?>"></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. <input placeholder="NIP Pengrim" type="text" name="SuratAtribut[nip_tandatangan]" value="<?php print $model->getAtribut('nip_tandatangan'); ?>"></td>
	</tr>
	</table>
	
</div>