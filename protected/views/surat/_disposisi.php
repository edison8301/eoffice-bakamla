<h3>Distribusi Memo</h3>

<div>&nbsp;</div>

<table class="table table-hover table-condensed">
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th width="30%">Jabatan Penerima</th>
	<th width="25%">Nama Penerima</th>
	<th width="20%" style="text-align:center">Waktu Pengiriman</th>
	<th width="20%" style="text-align:center">Waktu Dilihat</th>
</tr>
<?php $i=1; foreach($model->findAllDisposisi() as $data) { ?>
<?php $class = ""; ?>
<?php if($data->waktu_dilihat==null) $class = "danger"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $i; ?></td>
	<td><?php print Jabatan::getNamaJabatanById($data->id_jabatan_penerima); ?></td>
	<td><?php print Pegawai::getNamaByUsername($data->username_penerima); ?></td>
	<td style="text-align:center"><?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></td>
	<td style="text-align:center"><?php print $data->waktu_dilihat; ?></td>
</tr>
<?php $i++; } ?>
</table>

<div>&nbsp;</div>