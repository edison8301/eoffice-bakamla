<style>
td {
	vertical-align:top;
}
</style>

<?php $tab = ""; for($i=1; $i<11; $i++)
{
	$tab .= "&nbsp;"; 
} ?>

<table width="100%" class="surat">	
	<tr>
		<td width="10%" colspan="2">Nomor</td>
		<td>: </td>
		<td width="59%" ><?php print $model->getNomor(); ?></td>
		<td width="31%" >Jakarta <?php print $model->getAtribut('tanggal'); ?>
					<?php print $model->getAtribut('bulan'); ?>
					<?php print $model->getAtribut('tahun'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Sifat</td>
		<td>: </td>
		<td><?php print $model->getAtribut('sifat'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">Lampiran</td>
		<td>: </td>
		<td width="57%"><?php print $model->getAtribut('jumlah_lampiran'); ?></td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td colspan="2">Perihal</td>
		<td>: </td>
		<td><?php print $model->getAtribut('perihal'); ?></td>
		<td style="font-weight: bold"><?php print $model->getAtribut('penerima_surat'); ?> <br> <?php print $model->getAtribut('alamat'); ?> </td>
	</tr>
	
	<tr>
		<td colspan="3">&nbsp;</td>
		<td></td>
		<td>di</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td style="text-align: left"><u>Jakarta</u></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>

</table>

<div>&nbsp;</div>

<table class="surat">		
	
	<tr>
		<td width="40px">&nbsp;</td>
		<td colspan="2" style="font-weight: bold">Dengan hormat, / U.p.</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td width="82%" colspan="3"><?= $tab; ?>Berdasarkan Surat
							<?php print $model->getAtribut('pengirim_instansi'); ?>
						Nomor : 
							<?php print $model->getAtribut('no_surat'); ?>
						tanggal 
							<?php print $model->getAtribut('tanggal_dasar'); ?>
						Perihal  : <?php print $model->getAtribut('perihal_dasar'); ?>
						maka kami sampaikan bahwa surat telah diterima dengan baik pada tanggal
							<?php print $model->getAtribut('tanggal_diterima'); ?>
						Bakamla menurut rencana akan menghadiri 
						undangan dimaksud.
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?= $tab; ?>Sehubungan hal tersebut di atas, kami sampaikan dengan hormat bahwa
				 <?php print $model->getAtribut('isi'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">
			<?= $tab; ?>Demikian atas perhatian dan kerjasama yang baik, kami mengucapkan terima kasih.
		</td>
	</tr>	
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>