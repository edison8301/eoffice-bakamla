<?php /*
<h2>Draft</h2>

<iframe width="100%" height="500px" frameborder="0" src="<?php print Yii::app()->baseUrl; ?>/vendors/wodotexteditor/#../../template/<?php print $model->getContohSurat(); ?>"></iframe>

*/?> 

<?php $this->renderPartial('_draft',array('model'=>$model)); ?>


<?php $paraf = $model->findParafByStatus(4); ?>
<?php
	$paraf_komentar = new ParafKomentar;
	$paraf_komentar->id_paraf = $paraf->id;

	if(isset($_POST['ParafKomentar']))
	{
		$paraf_komentar->attributes = $_POST['ParafKomentar'];
		$paraf_komentar->pembuat = Yii::app()->user->id;
		$paraf_komentar->waktu_dibuat = date('Y-m-d H:i:s');
		$paraf_komentar->save();

		$this->redirect(array('suratKeluar/view','id'=>$model->id));
	}
?>

<h2>Komentar/Perbaikan</h2>

<div>&nbsp;</div>



<table class="table table-striped">
<thead>
<tr>
	<th style="text-align:right">Penulis</th>
	<th>Komentar</th>
</tr>
</thead>
<?php foreach($paraf->findAllParafKomentar() as $data) { ?>
<tr>
	<td width="25%" style="text-align:right">
		<div><?php print $data->pembuat; ?></div>
		<div><?php print Helper::getTanggalJam($data->waktu_dibuat); ?></div>
	</td>
	<td width="75%">
		<?php print $data->komentar; ?>
	</td>
</tr>
<?php } ?>
</table>

<div>&nbsp;</div>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<?php echo $form->hiddenField($paraf_komentar,'id_paraf'); ?>

	<?php echo $form->hiddenField($paraf_komentar,'pembuat'); ?>

	<?php echo $form->textAreaGroup($paraf_komentar,'komentar',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

