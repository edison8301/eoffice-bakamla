<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT IZIN</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>

<table class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="3">Kepada Pegawai Badan Keamanan Laut:</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="20%">Nama</td>
		<td width="4%">:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('pangkat_golongan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><?php print $model->getAtribut('unit_kerja'); ?></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">Terhitung mulai <b> <?php print $model->getAtribut('tanggal_mulai'); ?> sd <?php print $model->getAtribut('tanggal_selesai'); ?></b>, 
			diberikan izin untuk <?php print $model->getAtribut('tujuan_izin'); ?></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>2.</td>
		<td colspan="3">Demikian Surat Izin ini dibuat untuk dipergunakan sebagaimana mestinya.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>