<?php if($model->tembusan!='' OR $model->tembusan_laporan !='' OR $model->tembusan_pegawai!='') { ?>

<?php $gabung = array(); ?>

<?php if($model->tembusan_laporan!='') { ?>
<?php $daftar_tembusan_laporan = explode(';',$model->tembusan_laporan); ?>
<?php foreach($daftar_tembusan_laporan as $tembusan_laporan) { ?>
<?php $daftar_tembusan_laporan_baru = array($tembusan_laporan.', sebagai laporan'); ?>
<?php } ?>
<?php $gabung = array_merge($gabung,$daftar_tembusan_laporan_baru); ?>
<?php } ?>

<?php if($model->tembusan!='') { ?>
<?php $daftar_tembusan = explode(';',$model->tembusan); ?>
<?php $gabung = array_merge($gabung,$daftar_tembusan); ?>
<?php } ?>

<?php $total_tembusan = count($gabung); ?>

<table class="surat">
	<tr>
		<td style="font-weight:bold;text-decoration:underline">Tembusan Yth. :</td>
	</tr>
	
	<tr>
		<td>
			<ol>
				<?php $i=1; foreach($gabung as $coba) { ?>
				<li><?php print $coba; ?><?php if($i==$total_tembusan) print '.'; else print ';'; ?></li>
				<?php $i++; } ?>
			</ol>
		</td>
	</tr>	
</table>
<?php } ?>