<style>
	tr{
		vertical-align: top;
	}
</style>

<?php $tab = ""; for($i=1; $i<11; $i++)
{
	$tab .= "&nbsp;"; 
} ?>

<div style="border:1px solid #000;padding:20px">

	<div>&nbsp;</div>

	<table width="100%" class="surat">
	<tr>

	<td colspan="2">Nomor</td>
	<td>:</td>
	<td><?php print $model->getNomor(); ?> </td>
	<td>Jakarta <input placeholder="tanggal" type="text" size="4" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>">
		<input placeholder="bulan" type="text" size="6" name="SuratAtribut[bulan]" value="<?php print $model->getAtribut('bulan'); ?>">
		<input placeholder="tahun" type="text" size="3" name="SuratAtribut[tahun]" value="<?php print $model->getAtribut('tahun'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">Sifat</td>
		<td>:</td>
		<td><input placeholder="Sifat" type="text" name="SuratAtribut[sifat]" value="<?php print $model->getAtribut('sifat'); ?>"></td>
	</tr>
	<tr>
		<td colspan="2">Lampiran</td>
		<td>:</td>
		<td><input placeholder="Lampiran" type="text" name="SuratAtribut[jumlah_lampiran]" value="<?php print $model->getAtribut('jumlah_lampiran'); ?>"></td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td colspan="2">Perihal</td>
		<td>:</td>
		<td><input placeholder="Perihal" type="text" name="SuratAtribut[perihal]" value="<?php print $model->getAtribut('perihal'); ?>"></td>
		<td width="260" style="font-weight: bold"><input placeholder="Penerima Surat" type="text" name="SuratAtribut[penerima_surat]" value="<?php print $model->getAtribut('penerima_surat'); ?>"><br><input placeholder="Alamat" type="text" name="SuratAtribut[alamat]" value="<?php print $model->getAtribut('alamat'); ?>"></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
		<td></td>
		<td>di</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td style="text-align: center"><u>Jakarta</u></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</table>
<div>&nbsp;</div>
<table>
	
	<tr>
		<td width="40px">&nbsp;</td>
		<td colspan="2" style="font-weight: bold">Dengan hormat, / U.p.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?=$tab; ?>Berdasarkan Surat
							<input placeholder="Pengirim Instansi" type="text" name="SuratAtribut[pengirim_instansi]" value="<?php print $model->getAtribut('pengirim_instansi'); ?>">
						Nomor : 
							<input placeholder="Nomor Surat" type="text" name="SuratAtribut[no_surat]" value="<?php print $model->getAtribut('no_surat'); ?>">
						tanggal 
							<input type="text" placeholder="Tanggal Surat" name="SuratAtribut[tanggal_dasar]" value="<?php print $model->getAtribut('tanggal_dasar'); ?>">
						Perihal  : <input type="text" placeholder="Perihal Surat" name="SuratAtribut[perihal_dasar]" value="<?php print $model->getAtribut('perihal_dasar'); ?>">
						maka kami sampaikan bahwa surat telah diterima dengan baik pada tanggal
							<input placeholder="Tanggal Diterima" type="text" name="SuratAtribut[tanggal_diterima]" value="<?php print $model->getAtribut('tanggal_diterima'); ?>"> 
						Bakamla menurut rencana akan menghadiri 
						undangan dimaksud.
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">Sehubungan hal tersebut di atas, kami sampaikan dengan hormat bahwa
				 <textarea placeholder="Isi Surat" cols="100%" rows="5%" name="SuratAtribut[isi]" rows="1" cols="40"><?php print $model->getAtribut('isi'); ?></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">
			Demikian atas perhatian dan kerjasama yang baik, kami mengucapkan terima kasih.
		</td>
	</tr>			

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:66%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>