<?php $this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/draf'),
	'Perbaikan',
); ?>

<h1 id="detil">Perbaikan Draf Surat</h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_view',array('model'=>$model)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Kirim Draf Untuk Diperiksa',
		'icon'=>'ok',
		'size'=>'small',
		'url'=>array('surat/kirimUntukDiperiksa','id'=>$model->id),
		'htmlOptions'=>array('confirm'=>'Yakin akan mengirimkan draf surat untuk diperiksa?')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Hapus Draf',
		'icon'=>'trash',
		'size'=>'small',
		'url'=>array('surat/directDelete','id'=>$model->id),
		'htmlOptions'=>array('confirm'=>'Yakin akan menghapus draf surat?')
)); ?>
</div>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Konsep Surat','content'=>$this->renderPartial('_konsep',array('model'=>$model),true),'active'=>true),
			array('label'=>'Paraf Surat','content'=>$this->renderPartial('_konsep_paraf',array('model'=>$model),true)),	
		),
)); ?>

<hr>

<h3 id="catatan">
	<i class="glyphicon glyphicon-bullhorn"></i> Catatan
</h3>

<div>&nbsp;</div>


<?php $this->renderPartial('_catatan',array('model'=>$model,'paraf'=>$paraf,'catatan'=>$catatan)); ?>