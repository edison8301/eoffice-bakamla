<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KETERANGAN JALAN</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td></td>
		<td colspan="3">Yang Bertanda Tangan di Bawah Ini</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pejabat Yang Berwenang" size="40" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP</td>
		<td>:</td>
		<td><input placeholder="NIP" size="40" type="text" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan" size="40" type="text" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Alamat</td>
		<td>:</td>
		<td><textarea placeholder="Alamat" cols="40" name="SuratAtribut[alamat_pejabat]"><?php print $model->getAtribut('alamat_pejabat'); ?></textarea></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" rowspan="2">Dengan ini memberi Surat Keterangan Jalan Kepada :</td>
	</tr>	
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pegawai" size="40" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan" size="40" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tujuan</td>
		<td>:</td>
		<td><input placeholder="tujuan(kota)(provinsi)" size="40" type="text" name="SuratAtribut[tujuan]" value="<?php print $model->getAtribut('tujuan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Keperluan</td>
		<td>:</td>
		<td><input placeholder="keperluan" size="40" type="text" name="SuratAtribut[keperluan]" value="<?php print $model->getAtribut('keperluan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kendaraan</td>
		<td>:</td>
		<td><input placeholder="transportasi yang digunakan" size="40" type="text" name="SuratAtribut[kendaraan]" value="<?php print $model->getAtribut('kendaraan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Berangkat</td>
		<td>:</td>
		<td><input placeholder="tanggal keberangkatan" size="40" type="text" name="SuratAtribut[berangkat]" value="<?php print $model->getAtribut('berangkat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kembali</td>
		<td>:</td>
		<td><input placeholder="tanggal kembali" size="40" type="text" name="SuratAtribut[kembali]" value="<?php print $model->getAtribut('kembali'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pengikut</td>
		<td>:</td>
		<td><input placeholder="Pengikut" size="40" type="text" name="SuratAtribut[pengikut]" value="<?php print $model->getAtribut('pengikut'); ?>"></td>
	</tr>		
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">Demikian Surat Keterangan Jalan ini dibuat, dan kepada petugas dimohonkan bantuan Seperlunya untuk kelancaran perjalanan. </td>
	</tr>
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>		
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td>Dikeluarkan Di : <input placeholder="Tempat Surat Dikeluarkan" size="25" type="text" name="SuratAtribut[tempat_dikeluarkan]" value="<?php print $model->getAtribut('tempat_dikeluarkan'); ?>"></td>
	</tr>	
	<tr>
		<td style="width:50%"></td>
		<td>Pada Tanggal : <input placeholder="Tanggal Surat Dikeluarkan" size="25" type="text" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>"></td>
	</tr>		
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
	<div>&nbsp;</div>

</div>