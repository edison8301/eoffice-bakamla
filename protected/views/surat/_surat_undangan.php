<div style="border:1px solid #000;padding:20px">

	<div>&nbsp;</div>

	<table class="surat">
	<tr>

		<td>Nomor</td>
		<td width="1%">:</td>
		<td><?php print $model->getNomor(); ?></td>
		<td>&nbsp;</td>
		<td>Jakarta, <input placeholder="Tanggal Surat" type="text" name="SuratAtribut[surat_tanggal]" value="<?php print $model->getAtribut('surat_tanggal'); ?>" placeholder="Tanggal Surat"></td>
	</tr>
	<tr>
		<td>Sifat</td>
		<td width="1%">:</td>
		<td> <input type="text" name="SuratAtribut[surat_sifat]" value="<?php print $model->getAtribut('surat_sifat'); ?>" placeholder="Sifat Surat"></td>
	</tr>
	<tr>
		<td>Lampiran</td>
		<td width="1%">:</td>
		<td><input type="text" name="SuratAtribut[surat_lampiran]" value="<?php print $model->getAtribut('surat_lampiran'); ?>" placeholder="Lampiran Surat"></td>
		<td>&nbsp;</td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td width="1%">:</td>
		<td><input type="text" name="SuratAtribut[surat_perihal]" value="<?php print $model->getAtribut('surat_perihal'); ?>" placeholder="Perihal Surat"></td>
		<td>&nbsp;</td>
		<td width="260" style="font-weight: bold">DAFTAR UNDANGAN TERLAMPIR</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>di</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><u>Jakarta</u></td>
	</tr>
</table>

<?php $tab = ''; for($i=1;$i<=10;$i++) $tab .= '&nbsp;'; ?>
<table>
	<tr>
		<td colspan="3"><?php print $tab; ?>Dengan Hormat</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td colspan="3"><?php print $tab; ?>Dalam rangka <input type="text" name="SuratAtribut[undangan_deskripsi]" value="<?php print $model->getAtribut('undangan_deskripsi'); ?>" size="60" placeholder="Deskripsi acara/rapat"> maka kami mengundang Bapak/Ibu/Sdr/i 
			untuk hadir dalam rapat yang akan diselenggarakan pada :
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td width="24%"><?php print $tab; ?>Hari</td>
		<td width="1%">:</td>
		<td><input type="text" name="SuratAtribut[undangan_hari]" value="<?php print $model->getAtribut('undangan_hari'); ?>" size="40"></td>
	</tr>
	<tr>
		<td width="24%"><?php print $tab; ?>Tanggal</td>
		<td width="1%">:</td>
		<td><input type="text" name="SuratAtribut[undangan_tanggal]" value="<?php print $model->getAtribut('undangan_tanggal'); ?>" size="40"></td>
	</tr>
	<tr>
		<td width="24%"><?php print $tab; ?>Pukul</td>
		<td width="1%">:</td>
		<td><input type="text" name="SuratAtribut[undangan_pukul]" value="<?php print $model->getAtribut('undangan_pukul'); ?>" size="40"></td>
		
	</tr>
	<tr>
		<td><?php print $tab; ?>Tempat</td>
		<td>:</td>
		<td><input type="text" name="SuratAtribut[undangan_tempat]" value="<?php print $model->getAtribut('undangan_tempat'); ?>" size="40"></td>
		
	</tr>
	<tr>
		<td><?php print $tab; ?>Pimpinan Rapat</td>
		<td>:</td>
		<td><input type="text" name="SuratAtribut[undangan_pimpinan_rapat]" value="<?php print $model->getAtribut('undangan_pimpinan_rapat'); ?>" size="40"></td>
		
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td colspan="3"><?php print $tab; ?>Demikian atas perhatian dan dan kehadiran Bapak/Ibu/Sdr/i, kami ucapkan terima kasih.</td>
	</tr>

	</table>
	
	<div>&nbsp;</div>

	<table width="100%" style="" class="surat">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" style="text-align:center;font-weight:bold"><input type="text" name="SuratAtribut[penandatangan_an]" value="<?php print $model->getAtribut('penandatangan_an'); ?>" placeholder="AN, misal: AN. Kepala Badan Keamanan Laut" size="30"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:center;font-weight:bold"><input type="text" name="SuratAtribut[penandatangan_jabatan]" value="<?php print $model->getAtribut('penandatangan_jabatan'); ?>" placeholder="Jabatan Penandatangan" size="30"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:center;font-weight:bold"><input type="text" name="SuratAtribut[penandatangan_nama]" value="<?php print $model->getAtribut('penandatangan_nama'); ?>" placeholder="Nama Penandatangan" size="30"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:center;font-weight:bold"><input type="text" name="SuratAtribut[penandatangan_nip]" value="<?php print $model->getAtribut('penandatangan_nip'); ?>" placeholder="NIP Penandatangan" size="30"></td>
	</tr>
	</table>

	<hr>

	<h2>Lampiran</h2>

	<table>
	<tr>
		<td>
			<input type="text" style="font-weight: bold; width: 50%" name="SuratAtribut[lampiran_judul]" value="<?php print $model->getAtribut('judul_lampiran'); ?>" placeholder="Judul Lampiran">
		</td>
	</tr>
		<tr>
			<td>
				<textarea name="SuratAtribut[lampiran_daftar_pegawai]" style="width:100%" cols="200%" placeholder="Pangkat Maritim, Nama huruf Kapital, Gelar (Jabatan dalam Organisasi)"><?php print $model->getAtribut('lampiran_daftar_pegawai'); ?></textarea>
				<br>
				<span style="font-weight:bold">Keterangan:</span> Satu pegawai per baris. <b>Format: Nama; Jabatan Penugasan; Jabatan</b>	
			</td>
		</tr>
	</table>

</div>