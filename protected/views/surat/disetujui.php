<?php
$this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/draf'),
	'Disetujui',
);

?>

<h1 id="detil">Draf Surat Disetujui</h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_view',array('model'=>$model)); ?>

<div>&nbsp;</div>

<?php if(Surat::isPublishPermitted()) { ?>
<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'success',
			'label'=>'Terbitkan Surat',
			'icon'=>'ok',
			'size'=>'small',
			'url'=>array('surat/publish','id'=>$model->id),
	)); ?>&nbsp;
</div>
<?php } ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Surat','content'=>$this->renderPartial('_pratinjau',array('model'=>$model),true),'active'=>true),
			array('label'=>'Paraf Surat','content'=>$this->renderPartial('_konsep_paraf',array('model'=>$model),true)),	
			array('label'=>'Tujuan','content'=>$this->renderPartial('_konsep_tujuan',array('model'=>$model,'jenis'=>1),true)),
			array('label'=>'Tembusan','content'=>$this->renderPartial('_konsep_tembusan',array('model'=>$model,'jenis'=>2),true)),
			
		),
)); ?>
