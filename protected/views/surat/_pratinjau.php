<div>&nbsp;</div>

<?php if($model->berkas!=null) { ?>

	<iframe width="100%" height="500px" frameborder="0" src="<?php print Yii::app()->baseUrl; ?>/uploads/surat/berkas/<?php print $model->berkas; ?>"></iframe>

<?php } else { ?>

	<?php if($model->id_surat_metode == 1) { ?>
		<iframe width="100%" height="500px" frameborder="0" src="<?php print Yii::app()->controller->createUrl("surat/cetakPdf",array("id"=>$model->id)); ?>"></iframe>
	<?php } ?>

	<?php if($model->id_surat_metode == 2 AND $model->draf!==null) { ?>

		<?php $tipe = substr($model->draf, -3); ?>

		<?php if($tipe=='pdf' OR $tipe=='PDF') { ?>
			<iframe width="100%" height="500px" frameborder="0" src="<?php print Yii::app()->baseUrl; ?>/uploads/surat/draf/<?php print $model->draf; ?>"></iframe>
		<?php } ?>

		<?php if($tipe=='jpg' OR $tipe=='JPG' OR $tipe=='png' OR $tipe=='PNG') { ?>
			<div width="100%" height="500px" style="overflow-y:auto;height:500px;padding:0px 50px;border:1px solid #666666;text-align:center;background:#525659">
				<img src="<?php print Yii::app()->baseUrl; ?>/uploads/surat/draf/<?php print $model->draf; ?>" class="img-responsive" style="margin-left:auto;margin-right:auto">
			</div>
		<?php } ?>

		<div>&nbsp;</div>

	<?php } ?>

<?php } ?>