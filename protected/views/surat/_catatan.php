<table class="table table-striped">
<thead>
<tr>
	<th style="text-align:right" width="25%">Penulis</th>
	<th width="75%">Catatan</th>
</tr>
</thead>
<?php foreach($paraf->findAllCatatan() as $data) { ?>
<tr>
	<td style="text-align:right">
		<div style="margin-bottom:5px;font-weight:bold"><i class="glyphicon glyphicon-user"></i>&nbsp;<?php print $data->pembuat; ?></div>
		<div style="font-size:11px"><i class="glyphicon glyphicon-calendar"></i>&nbsp;<?php print Helper::getTanggalWaktu($data->waktu_dibuat); ?></div>
	</td>
	<td style="text-align:left">
		<?php print $data->catatan; ?>
		<?php $i=1; foreach($data->findAllLampiran() as $lampiran) { ?>
		<div><?php print CHtml::link('<i class="glyphicon glyphicon-download-alt"></i> Lampiran '.$i,$lampiran->getUrl(),array('target'=>'_blank')); ?></div>
		<?php $i++; } ?>
	</td>
</tr>
<?php } ?>
</table>

<div>&nbsp;</div>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>
	
	<div class="well">
		<?php echo $form->textAreaGroup($catatan,'catatan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label">Lampiran</label>
			<div class="col-sm-9">
				<?php $this->widget('CMultiFileUpload', array(
						'name'=>'lampirans',
     					'accept'=>'jpg|gif|pdf|png',
     					'duplicate' => 'Duplicate file!', // useful, i think
                		'denied' => 'Tipe file ditolak',
  				)); ?>
  			</div>
  		</div>
	</div>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Kirim Catatan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>