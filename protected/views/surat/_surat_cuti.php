<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT CUTI</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>1.</td>
		<td colspan="4">Kepada Pegawai Badan Keamanan Laut:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pegawai" size="40" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="NIP Pegawai"size="40" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><input placeholder="Pangkat Golongan Pegawai"size="40" type="text" name="SuratAtribut[pangkat_golongan]" value="<?php print $model->getAtribut('pangkat_golongan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Pegawai"size="40" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Unit Kerja Pegawai"size="40" type="text" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td width="1%"></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>	
	<tr>
		<td></td>
		<td colspan="4">Terhitung mulai 
				<input placeholder="Tanggal Mulai Cuti" type="text" name="SuratAtribut[tanggal_mulai_cuti]" value="<?php print $model->getAtribut('tanggal_mulai_cuti'); ?>">
				 s.d. 
				<input placeholder="Tanggal Selesai Cuti" type="text" name="SuratAtribut[tanggal_selesai_cuti]" value="<?php print $model->getAtribut('tanggal_selesai_cuti'); ?>">, 
			dengan ketentuan sebagai
			berikut :</td>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>	
	<tr>
		<td></td>
		<td>a. </td>
		<td colspan="3">Sebelum menjalankan cuti wajib menyerahkan pekerjaan kepada atasan langsung.</td>
	</tr>
	<tr>
		<td></td>
		<td>b. </td>
		<td colspan="3">Setelah selesai menjalankan cuti, wajib melaporkan diri kepada atasan langsungnya
						dan bekerja kembali sebagaimana mestinya.
		</td>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>		
	<tr>
		<td>2.</td>
		<td colspan="4">Demikian Surat Cuti ini dibuat untuk dipergunakan sebagaimana mestinya.</td>
	</tr>

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>

</div>