<?php
$this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/draf'),
	'Terbitkan Draf',
);

?>
<h1>Terbitkan Surat</h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('enctype'=>'multipart/form-data')

)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="well">
	<?php echo $form->textFieldGroup($model,'nomor',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'tanggal',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
			'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>

	<?php echo $form->fileFieldGroup($model,'berkas',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Terbitkan Surat',
			'htmlOptions'=>array('confirm'=>'Yakin akan menerbitkan surat?')
		)); ?>
	</div>

<?php $this->endWidget(); ?>
