<?php
$this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/draf'),
	'Pemeriksaan',
);

?>

<h1 id="detil">Pemeriksaan Surat</h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_view',array('model'=>$model)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Paraf','content'=>$this->renderPartial('_konsep_paraf',array('model'=>$model),true),'active'=>true),
			array('label'=>'Pratinjau','content'=>$this->renderPartial('_pratinjau',array('model'=>$model),true)),	
		),
)); ?>