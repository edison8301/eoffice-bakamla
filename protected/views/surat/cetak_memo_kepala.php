<style>
td {
	vertical-align:top;
	font-size: 14px;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline; font-size: 14px;">MEMO</div>
<div style="text-align:center;font-weight: bold; font-size: 14px;">Nomor : <?php print $model->getNomor(); ?></div>

<table class="surat">
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Kepada Yth.</td>
		<td width="65%">: <?php print $model->getAtribut('penerima_memo'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Dari</td>
		<td>: <?php print $model->getAtribut('pengirim_memo'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perihal</td>
		<td>: <?php print $model->getAtribut('perihal'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Lampiran</td>
		<td>: <?php print $model->getAtribut('lampiran'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tanggal</td>
		<td>: <?php print $model->getAtribut('tanggal_memo'); ?></td>
	</tr>	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"> Dengan Hormat,</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Bersama Ini Kami Sampaikan <?php print $model->getAtribut('isi'); ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Demikian disampaikan, mohon arahan lebih lanjut dan terima kasih.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('pengirim_memo_tandatangan'); ?></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. <?php print $model->getAtribut('nip_tandatangan'); ?></td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>