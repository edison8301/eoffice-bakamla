<table width="100%" style="font-size:11px">
<tr>
	<td width="60%">&nbsp;</td>
	<td width="40%" colspan="2">Lampiran Surat Perintah Sestama Bakamla</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>Nomor</td>
	<td>: <?php print $model->getNomor(); ?></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>Tanggal</td>
	<td>: <?php print $model->getTanggal(); ?></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>Tentang</td>
	<td style="border-bottom:1px solid #000000">: <?php print $model->getAtribut('pertimbangan_nama_kegiatan'); ?></td>
</tr>
</table>

<div>&nbsp;</div>

<div style="font-weight:bold;text-align:center;">DAFTAR NAMA</div>
<div style="font-weight:bold;text-align:center;"><?php print strtoupper($model->getAtribut('pertimbangan_nama_kegiatan')); ?></div>
<div style="font-weight:bold;text-align:center;text-decoration:underline;"><?php print strtoupper($model->getAtribut('untuk_tanggal_kegiatan')); ?>, <?php print strtoupper($model->getAtribut('untuk_tempat_kegiatan')); ?></div>

<div>&nbsp;</div>

<table width="100%" border="1" cellpadding="3" cellspacing="0">
<tr>
	<th width="5%">No</th>
	<th width="25%">NAMA</th>
	<th width="25%">JABATAN</th>
	<th width="15%">TANGGAL BERANGKAT</th>
	<th width="15%">TANGGAL KEMBALI</th>
	<th width="15%">KETERANGAN</th>
</tr>
<?php $daftar_pegawai = explode(PHP_EOL,$model->getAtribut('kepada_pegawai')); ?>
<?php $no=1; foreach($daftar_pegawai as $pegawai) { ?>
<?php $data = explode(';',$pegawai); ?>
<tr>
	<td style="text-align:center"><?php print $no; ?></td>
	<td><?php print $data[0]; ?></td>
	<td style="text-align:center"><?php print $data[1]; ?></td>
	<td style="text-align:center"><?php print $data[2]; ?></td>
	<td style="text-align:center"><?php print $data[3]; ?></td>
	<td><?php print $data[4]; ?></td>
</tr>
<?php $no++; } ?>
</table>


