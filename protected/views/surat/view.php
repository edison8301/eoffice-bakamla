<?php
$this->breadcrumbs=array(
	'Arsip Surat',
	$model->nomor,
);

?>

<h1 id="detil">Detail Surat</h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_view',array('model'=>$model)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Kirim Disposisi',
		'icon'=>'arrow-down',
		'size'=>'small',
		'url'=>array('distribusi/create','id_surat'=>$model->id,'id_distribusi_jenis'=>3),
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Kirim Terusan',
		'icon'=>'arrow-right',
		'size'=>'small',
		'url'=>array('distribusi/create','id_surat'=>$model->id,'id_distribusi_jenis'=>4),
)); ?>&nbsp;

<?php if($model->username_pembuat == Yii::app()->user->id OR $model->username_pengaju == Yii::app()->user->id) { ?>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'warning',
		'label'=>'Revisi Surat',
		'icon'=>'repeat',
		'size'=>'small',
		'url'=>array('surat/createRevisi','id'=>$model->id),
		'htmlOptions'=>array('confirm'=>'Yakin akan membuat revisi surat?')
)); ?>&nbsp;
<?php } ?>
</div>


<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Surat','content'=>$this->renderPartial('_pratinjau',array('model'=>$model),true),'active'=>true),
			array('label'=>'Ditujukan','content'=>$this->renderPartial('_view_distribusi',array('model'=>$model,'jenis'=>1),true)),
			array('label'=>'Tembusan','content'=>$this->renderPartial('_view_distribusi',array('model'=>$model,'jenis'=>2),true)),
			array('label'=>'Disposisi','content'=>$this->renderPartial('_view_distribusi',array('model'=>$model,'jenis'=>3),true)),
			array('label'=>'Terusan','content'=>$this->renderPartial('_view_distribusi',array('model'=>$model,'jenis'=>4),true)),
			array('label'=>'Paraf','content'=>$this->renderPartial('_konsep_paraf',array('model'=>$model),true)),
		),
)); ?>