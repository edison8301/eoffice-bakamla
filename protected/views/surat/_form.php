<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal'
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">

	<?php echo $form->textFieldGroup($model,'nomor',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'tanggal',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php echo $form->textFieldGroup($model,'waktu_diajukan',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	
	<?php echo $form->textAreaGroup($model,'ringkasan',array(
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>

	<?php echo $form->dropDownListGroup($model,'id_surat_status',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(SuratStatus::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>
	
	<?php echo $form->fileFieldGroup($model,'lampiran',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	</div>

	<div class="well" style="text-align:right">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Save',
			)); ?>
	</div>
	

<?php $this->endWidget(); ?>


<?php //Simulasi Dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPengaju',
    'options'=>array(
        'title'=>'Simulasi',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
    $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped hover',
		'dataProvider'=>Pegawai::model()->search(),
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:left',
				),
			),
			array(
		      	'name'=>'nama',
		      	'value'=>'$data->nama',
			),
			'nip',
			array(
				'name'=>'id_jabatan',
				'header'=>'Jabatan',
				'value'=>'$data->getRelationField("jabatan","nama_jabatan")',
				'filter'=>CHtml::listData(Jabatan::model()->findAll(),'id','nama_jabatan')
			),	
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+", 
					array("name" => "send_insuredcode", 
					"id" => "send_insuredcode", 
					"onClick" => "$(\"#dialogPengaju\").dialog(\"close\");
							    $(\"#SuratKeluar_username_pengaju\").val(\"$data->username\");
							    $(\"#nama_pengaju\").val(\"$data->nama\");
					"))',
					'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'30px',
					),
        	),
		),
	));
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer

?>