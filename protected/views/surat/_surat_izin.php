<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT IZIN</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>
	
	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>1.</td>
		<td colspan="3">Kepada Pegawai Badan Keamanan Laut:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td width="20%">Nama</td>
		<td width="1%">:</td>
		<td><input placeholder="Nama Yang Mengajukan Izin" size="40" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="No Induk Pegawai Yang Mengajukan Izin" size="40" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><input placeholder="Golongan Yang Mengajukan Izin" size="40" type="text" name="SuratAtribut[pangkat_golongan]" value="<?php print $model->getAtribut('pangkat_golongan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Pegawai Yang Mengajukan Izin" size="40" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Satker Kerja" size="40" type="text" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">Terhitung mulai <b><input placeholder="tanggal izin" type="text" name="SuratAtribut[tanggal_mulai]" size="16" value="<?php print $model->getAtribut('tanggal_mulai'); ?>"> sd
		 				<input placeholder="tanggal selesai izin" type="text" name="SuratAtribut[tanggal_selesai]" size="16" value="<?php print $model->getAtribut('tanggal_selesai'); ?>"></b>, 
						diberikan izin untuk <input placeholder="tujuan izin" type="text" name="SuratAtribut[tujuan_izin]" size="26" value="<?php print $model->getAtribut('tujuan_izin'); ?>"></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>2.</td>
		<td colspan="3">Demikian Surat Izin ini dibuat untuk dipergunakan sebagaimana mestinya.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>

</div>