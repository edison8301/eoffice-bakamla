<?php
$this->breadcrumbs=array(
	'Surat Keluars'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SuratKeluar','url'=>array('index')),
array('label'=>'Create SuratKeluar','url'=>array('create')),
array('label'=>'Update SuratKeluar','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SuratKeluar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SuratKeluar','url'=>array('admin')),
);
?>

<h1>Detail Surat Keluar</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'no_surat',
			array(
				'label'=>'Jenis',
				'value'=>$model->getRelationField('surat_keluar_jenis','nama')
			),
			'pembuat',
			'waktu_dibuat',
			array(
				'label'=>'Status',
				'value'=>$model->getRelationField('surat_keluar_status','nama')
			),
		),
)); ?>


<?php if($model->id_surat_keluar_status == 2) { ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Kirim Untuk Review',
		'icon'=>'ok',
		'url'=>array('suratKeluar/setStatus','id'=>$model->id,'status'=>3)
)); ?>
</div>

<?php } ?>


<h2>Paraf</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'label'=>'Tambah Paraf',
		'icon'=>'plus',
		'url'=>array('paraf/create','id_surat'=>$model->id)
)); ?>



<div>&nbsp;</div>

<table class='table'>
<tr>
	<th width="5%" style="text-align:center">No</th>
	<th>Jabatan</th>
	<th>Pemangku</th>
	<th>Status</th>
	<th>Keterangan</th>
</tr>
<?php foreach($model->findAllParaf() as $paraf) { ?>
<?php $class = ''; ?>
<?php if($paraf->id_paraf_status == 1) $class = "success"; ?>
<?php if($paraf->id_paraf_status == 3) $class = "info"; ?>
<?php if($paraf->id_paraf_status == 4) $class = "danger"; ?>
<tr class="<?php print $class; ?>">
	<td style="text-align:center"><?php print $paraf->urutan; ?></td>
	<td><?php print $paraf->id_jabatan; ?></td>
	<td>&nbsp;</div>
	<td><?php print $paraf->getRelationField('paraf_status','nama'); ?></td>
	<td>
		<?php print CHtml::link("<i class='glyphicon glyphicon-search'></i>",array("paraf/view","id"=>$paraf->id)); ?>
		<?php print CHtml::link("<i class='glyphicon glyphicon-pencil'></i>",array("paraf/update","id"=>$paraf->id)); ?>
	</td>
</tr>
<?php } ?>
</table>
