<?php
$this->breadcrumbs=array(
	'Draf Surat'=>array('pegawai/paraf'),
	'Konfirmasi Draf Surat',
);

?>

<h1 id="detil">Konfirmasi Draf Surat</h1>

<div>&nbsp;</div>

<?php $this->renderPartial('_view',array('model'=>$model)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'success',
		'label'=>'Paraf Surat',
		'icon'=>'ok',
		'url'=>array('paraf/setujui','id'=>$paraf->id),
		'htmlOptions'=>array('confirm'=>'Yakin akan memberikan paraf pada surat?')
)); ?>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'danger',
		'label'=>'Perbaiki Surat',
		'icon'=>'remove',
		'url'=>array('paraf/perbaiki','id'=>$paraf->id),
		'htmlOptions'=>array('confirm'=>'Yakin akan meminta perbaikan pada surat?')
)); ?>
</div>

<hr>

<h3 id="pratinjau"><i class="glyphicon glyphicon-print"></i> Pratinjau Surat</h3>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			array('label'=>'Draf Surat','content'=>$this->renderPartial('_paraf',array('model'=>$model),true),'active'=>true),
			array('label'=>'Paraf Surat','content'=>$this->renderPartial('_konsep_paraf',array('model'=>$model),true)),	
		),
)); ?>

<hr>

<h3 id="catatan"><i class="glyphicon glyphicon-bullhorn"></i> Catatan</h2>

<div>&nbsp;</div>

<?php $this->renderPartial('_catatan',array('model'=>$model,'paraf'=>$paraf,'catatan'=>$catatan)); ?>
