<?php

$criteria = new CDbCriteria;

$criteria->with = array(
	'distribusi'
); 

$criteria->addCondition('distribusi.id_distribusi_jenis = 1');
$criteria->addCondition('id_surat_status = 1');

?>


<table class="table">
<tr>
	<th>No</th>
	<th>Id</th>
	<th>Nomor</th>
	<th>Tanggal</th>
	<th>Username Pembuat</th>
	<th>Jumlah Distribusi</th>
</tr>
<?php $no=1; foreach(Surat::model()->findAll($criteria) as $surat) { ?>
<tr>
	<td><?php print $no; ?></td>
	<td><?php print $surat->id; ?></td>
	<td><?php print $surat->nomor; ?></td>
	<td><?php print $surat->tanggal; ?></td>
	<td><?php print $surat->username_pembuat; ?></td>
	<td><?php print $surat->countDistribusi(); ?></td>
</tr>
<?php $no++; } ?>
</table>