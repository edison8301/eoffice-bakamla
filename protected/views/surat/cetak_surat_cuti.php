<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT CUTI</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>

<table width="100%" class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="4">Kepada Pegawai Badan Keamanan Laut:</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td colspan="2" width="30%">Nama</td>
		<td>:</td>
		<td width="35%"><?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('pangkat_golongan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Unit Kerja</td>
		<td>:</td>
		<td><?php print $model->getAtribut('unit_kerja'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td width="100%" colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="4">Terhitung mulai <b><?php print $model->getAtribut('tanggal_mulai_cuti'); ?></b> sd <b><?php print $model->getAtribut('tanggal_selesai_cuti'); ?></b>, 
			dengan ketentuan sebagai berikut :</td>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td width="5%">a. </td>
		<td colspan="3">Sebelum menjalankan cuti wajib menyerahkan pekerjaan kepada atasan langsung.</td>
	</tr>
	<tr>
		<td></td>
		<td>b. </td>
		<td colspan="3">Setelah selesai menjalankan cuti, wajib melaporkan diri kepada atasan langsungnya
						dan bekerja kembali sebagaimana mestinya.
		</td>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>
	<tr>
		<td>2.</td>
		<td colspan="4">Demikian Surat Cuti ini dibuat untuk dipergunakan sebagaimana mestinya.</td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>