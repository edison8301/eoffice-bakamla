<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<?php if($model->id_surat_metode==1) { ?>
		<?php $this->renderPartial('_pengisian_kolom_isian',array('model'=>$model,'form'=>$form)); ?>
	<?php } ?>

	<?php if($model->id_surat_metode==2) { ?>
	<div class="well">
		<?php echo $form->fileFieldGroup($model,'draf',array(
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5')
			)
		)); ?>
	</div>
	<?php } ?>

	<div class="well">

		<?php echo $form->select2Group($model,'tujuan',array(
				'wrapperHtmlOptions' => array('class' => 'col-sm-4'),
				'widgetOptions' => array(
					'asDropDownList' => false,
					'options' => array(
						'tags' => Surat::model()->getListAutoCompleteTujuan(),
						'placeholder' => 'Ketik Nama Jabatan/Pegawai',
						'separator' => ';',
						'minimumInputLength'=>2,
					),
				),
		)); ?>

		<?php echo $form->select2Group($model,'tembusan',array(
				'wrapperHtmlOptions' => array('class' => 'col-sm-4'),
				'widgetOptions' => array(
					'asDropDownList' => false,
					'options' => array(
						'tags' => Surat::model()->getListAutoCompleteTembusan(),
						'placeholder' => 'Ketik Nama Jabatan/Pegawai',
						'separator' => ';',
						'minimumInputLength'=>2,
					)
				)
		)); ?>

		<?php echo $form->select2Group($model,'tembusan_laporan',array(
				'wrapperHtmlOptions' => array('class' => 'col-sm-4'),
				'widgetOptions' => array(
					'asDropDownList' => false,
					'options' => array(
						'tags' => Jabatan::model()->getListAutoComplete(),
						'placeholder' => 'Ketik Nama Jabatan/Pegawai',
						'separator' => ';',
						'minimumInputLength'=>2,
					)
				)
		)); ?>

	</div>

	

	<div class="well">
		<div class="form-group">
			<label class="col-sm-3 control-label">Lampiran</label>
			<div class="col-sm-9">
				<?php $this->widget('CMultiFileUpload', array(
						'name'=>'lampirans',
     					'accept'=>'jpg|gif|pdf|png',
     					'duplicate' => 'Duplicate file!', // useful, i think
                		'denied' => 'Tipe file ditolak',
  				)); ?>
  			</div>
  		</div>
	</div>


	<div class="well">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Save',
		)); ?>
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'link',
				'context'=>'success',
				'icon'=>'print',
				'url'=>array('surat/cetakPdf','id'=>$model->id),
				'label'=>'Cetak',
				'htmlOptions'=>array('target'=>'_blank'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>