<style>
	td{
		vertical-align: top;
	}
</style>
<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT PERNYATAAN</div>
	<div style="text-align:center;font-weight:bold"><?php print $model->getNomor(); ?></div>

	<div>&nbsp;</div>

	<table class="surat" >
	<tr>
		<td></td>
		<td colspan="3">Yang Bertanda Tangan di Bawah Ini</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pejabat yang Bertanggung jawab" size="40" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP</td>
		<td>:</td>
		<td><input placeholder="NIP" size="40" type="text" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan" size="40" type="text" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Alamat</td>
		<td>:</td>
		<td><textarea placeholder="Alamat" cols="37" name="SuratAtribut[alamat_pejabat]"><?php print $model->getAtribut('alamat_pejabat'); ?></textarea></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" rowspan="2">Dengan ini menyatakan dengan sesungguhnya bahwa <input placeholder="Nama Pegawai" placeholder="Nama Pegawai" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>">
		<br>
		</td>
	</tr>	
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"> <textarea rows="3" cols="75" placeholder="Poin-poin yang menyatakan bahwa Yang bersangkutan tidak dalam masalah" name="SuratAtribut[poin_bebas_masalah]"><?php print $model->getAtribut('poin_bebas_masalah'); ?></textarea>
		<br><span>(<i>Pisahkan Tiap Poin-poin dengan <strong>Garis Baru / Enter</strong>)<br> *penomoran otomatis</i></span>
		</td>
	</tr>
		<td>&nbsp;</td>
		<td colspan="3">Demikian Surat pernyataan ini dubuat dengan sesungguhnya untuk dipergunakan  sebagaimana mestinya.</td>
	</tr>
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>		
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td>Dikeluarkan Di : <input placeholder="Tempat Surat Dikeluarkan" size="25" type="text" name="SuratAtribut[tempat_dikeluarkan]" value="<?php print $model->getAtribut('tempat_dikeluarkan'); ?>"></td>
	</tr>	
	<tr>
		<td style="width:50%"></td>
		<td>Pada Tanggal : <input placeholder="Tanggal Surat Dikeluarkan" size="25" type="text" name="SuratAtribut[tanggal]" value="<?php print $model->getAtribut('tanggal'); ?>"></td>
	</tr>		
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>

</div>