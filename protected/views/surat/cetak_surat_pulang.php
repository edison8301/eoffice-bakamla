<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT KETERANGAN PULANG SEBELUM WAKTUNYA</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>

<table width="100%" style="" class="surat">
	<tr>
		<td style="width:60%"></td>
		<td style="width:60%" style="text-align:center;font-weight:bold">NOMOR BAGAN KEPEGAWAIAN</td>
	</tr>
	<tr>
		<td style="width:60%"></td>
		<td style="width:60%" style="text-align:center"><?php print $model->getAtribut('surat_nomor_bag_pegawai'); ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>
		<td style="width:60%"></td>
		<td style="width:60%" style="text-align:center;font-weight:bold">TANGGAL</td>
	</tr>
	<tr>
		<td style="width:60%"></td>
		<td style="width:60%" style="text-align:center"><?php print $model->getAtribut('surat_tanggal'); ?></td>
	</tr>	

	</table>


		<table class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="2">Yang Bertandatangan di bawah ini:</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td width="65%">: <?php print $model->getAtribut('pejabat_nama'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>: <?php print $model->getAtribut('pejabat_nip'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <?php print $model->getAtribut('pejabat_jabatan'); ?></td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>: <?php print $model->getAtribut('pejabat_unit_kerja'); ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2">Dengan Ini Menyatakan Bahwa.</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td width="65%">: <?php print $model->getAtribut('pegawai_nama'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>: <?php print $model->getAtribut('pegawai_nip'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <?php print $model->getAtribut('pegawai_jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>: <?php print $model->getAtribut('pegawai_unit_kerja'); ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">Pada Hari <?php print $model->getAtribut('surat_hari'); ?>, Tanggal <?php print $model->getAtribut('surat_tanggal_inti'); ?> Pukul
		<?php print $model->getAtribut('surat_pukul'); ?> <b> Pulang Sebelum Waktunya</b> karena <?php print $model->getAtribut('surat_isi'); ?>
		Karena Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya. 
		 </td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">Jakarta, <?php print $model->getAtribut('tanggal_lengkap'); ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><?php print $model->getAtribut('tandatangan_nama_pejabat'); ?></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. <?php print $model->getAtribut('tandatangan_nip_pejabat'); ?></td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>