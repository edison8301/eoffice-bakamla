<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'No Surat',
				'value'=>$model->getNomor(),
			),
			array(
				'label'=>'Tanggal Surat',
				'value'=>Helper::getTanggal($model->getTanggal()),
			),	
			array(
				'label'=>'Jenis',
				'value'=>$model->getRelationField('surat_jenis','nama')
			),
			array(
				'label'=>'Sifat',
				'value'=>$model->getRelationField('surat_sifat','nama')
			),
			array(
				'label'=>'Penandatangan',
				'value'=>$model->getRelationField('jabatan_penandatangan','nama')
			),
			array(
				'label'=>'Ringkasan',
				'value'=>$model->ringkasan
			),
			array(
				'label'=>'Pengaju',
				'value'=>$model->getNamaPengaju().' ('.$model->getJabatanPengaju().')'
			),
			array(
				'label'=>'Pembuat',
				'value'=>$model->getNamaPembuat().' ('.$model->getJabatanPembuat().')',
				'htmlOptions'=>array('style'=>'padding-top:10px')
			),
			array(
				'label'=>'Status',
				'type'=>'raw',
				'value'=>'<div style="margin-top:5px"><span class="label label-primary">'.$model->getRelationField('surat_status','nama').'</span></div>'
			),
			array(
				'label'=>'Waktu Diterbitkan',
				'value'=>Helper::getTanggalJam($model->waktu_diterbitkan),
				'visible'=>$model->id_surat_status == 1 ? 1 : 0
			),
			array(
				'label'=>'Waktu Disetujui',
				'value'=>Helper::getTanggalJam($model->waktu_disetujui),
				'visible'=>$model->id_surat_status == 5 ? 1 : 0
			),
			array(
				'label'=>'Waktu Diajukan',
				'value'=>Helper::getTanggalJam($model->waktu_diajukan),
				'visible'=>$model->id_surat_status == 3 ? 1 : 0
			),
			array(
				'label'=>'Waktu Dibuat',
				'value'=>Helper::getTanggalJam($model->waktu_dibuat),
			),
		),
)); ?>
