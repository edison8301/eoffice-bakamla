<h3 id="pratinjau">
	<i class="glyphicon glyphicon-print"></i> Pratinjau Surat : 
	<?php if($model->id_surat_metode == 1) { ?>
	<a href="<?php print Yii::app()->controller->createUrl("surat/ubahMetode",array("id"=>$model->id)); ?>" data-toggle="tooltip" title="Ubah Metode Pengisian ke Unggah Draf">
	<span class="label label-primary" style="margin-botom:50px">Metode Kolom Isian</span>
	</a>
	<?php } ?>
	<?php if($model->id_surat_metode == 2) { ?>
	<a href="<?php print Yii::app()->controller->createUrl("surat/ubahMetode",array("id"=>$model->id)); ?>" data-toggle="tooltip" title="Ubah Metode Pengisian ke Kolom Isian">
		<span class="label label-primary" style="margin-botom:50px">Metode Unggah Draf (PDF)</span>
	</a>
	<?php } ?>
</h3>

<hr>

<?php $this->renderPartial('_pratinjau',array('model'=>$model)); ?>

<hr>

<h3 id="lampiran">
	<i class="glyphicon glyphicon-paperclip"></i> Lampiran Surat
</h3>

<div>&nbsp;</div>

<?php $this->renderPartial('_lampiran',array('model'=>$model)); ?>

<div id="shortcut">
    <a class="btn btn-warning btn-sm" href="#detil"><i class="glyphicon glyphicon-file"></i> Detil</a>
    <a class="btn btn-primary btn-sm" href="#pratinjau"><i class="glyphicon glyphicon-print"></i> Pratinjau</a>
    <a class="btn btn-success btn-sm" href="#pengisian"><i class="glyphicon glyphicon-pencil"></i> Pengisian</a>
    <a class="btn btn-danger btn-sm" href="#lampiran"><i class="glyphicon glyphicon-paperclip"></i> Lampiran</a>
     <a class="btn btn-info btn-sm" href="#catatan"><i class="glyphicon glyphicon-bullhorn"></i> Catatan</a>
</div>

<style>
#shortcut {
 	position: fixed;
    padding: 5px 5px;
    background: #EEEEEE;
    bottom: 20px;
    right: 20px;
    border:1px solid #999999;
}
</style>