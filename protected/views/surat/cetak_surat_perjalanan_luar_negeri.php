<style>
td {
	vertical-align:top;
}
</style>

<?php $tab = ''; for($i=1;$i<=10;$i++) $tab .= '&nbsp;'; ?>
<table width="100%" class="surat">	
	<tr>

		<td width="10%" colspan="2">Nomor</td>
		<td>:</td>
		<td width="72%" ><?php print $model->getNomor(); ?></td>
		<td>Jakarta <?php print $model->getAtribut('tanggal'); ?>
					<?php print $model->getAtribut('bulan'); ?>
					<?php print $model->getAtribut('tahun'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="2">Sifat</td>
		<td>:</td>
		<td><?php print $model->getAtribut('sifat'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">Lampiran</td>
		<td>:</td>
		<td width="50%"> <?php print $model->getAtribut('jumlah_lampiran'); ?></td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td colspan="2">Perihal</td>
		<td>:</td>
		<td><?php print $model->getAtribut('perihal'); ?></td>
		<td width="260" style="font-weight: bold">SEKRETARIS KEMENTRIAN<br>SEKRETARIS NEGARA</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td>di</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td>Jakarta</td>
	</tr>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>

</table>
<table class="surat">	
	
	<tr>
		<td width="40px">&nbsp;</td>
		<td colspan="2" style="font-weight: bold">Up. Kepala Biro KTLN</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td width="82%" colspan="3"><p><?php print $tab; ?>Berdasarkan Surat dari 
							<?php print $model->getAtribut('dasar_surat'); ?>
						Nomor : 
							<?php print $model->getAtribut('no_surat'); ?>
						tanggal 
							<?php print $model->getAtribut('tanggal_dasar'); ?> Perihal  : <?php print $model->getAtribut('perihal_dasar'); ?>
						terkait pertemuan tersebut,  maka disampaikan dengan hormat bahwa 
							<?php print $model->getAtribut('staf_pejabat'); ?>
						Bakamla menurut rencana akan menghadiri 
						undangan dimaksud. </p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?php print $tab; ?>Sehubungan hal tersebut di atas, kami mohon dengan hormat kiranya Bapak dapat memberikan persetujuan kepada 
				<?php print $model->getAtribut('staf_pejabat_pusat'); ?>
			Bakamla untuk melaksanakan tugas ke luar negeri 
				<?php print $model->getAtribut('negara'); ?>
			pada
				<?php print $model->getAtribut('tanggal_mulai'); ?>
			s.d. 
				<?php print $model->getAtribut('tanggal_selesai'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>: <?php print $model->getAtribut('nama'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>: <?php print $model->getAtribut('jabatan'); ?></td>

	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP / NRP</td>
		<td>: <?php print $model->getAtribut('nip'); ?></td>
		
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>No Paspor</td>
		<td>: <?php print $model->getAtribut('paspor'); ?></td>
		
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
		<td width="82%" colspan="3">
			<?php print $tab; ?>Adapun dapat kami sampaikan bahwa akomodasi di tanggung<b> oleh 
				<?php print $model->getAtribut('instansi'); ?></b>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3">
			<?php print $tab; ?>Demikian atas perhatian dan kerjasama yang baik, kami mengucapkan terima kasih.
		</td>
	</tr>

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>