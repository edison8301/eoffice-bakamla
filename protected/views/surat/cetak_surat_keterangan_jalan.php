<style>
td {
	vertical-align:top;
}
</style>

<div style="font-weight:bold;text-align:center;text-decoration:underline;">SURAT KETERANGAN JALAN</div>
<div style="text-align:center;font-weight: bold">Nomor : <?php print $model->getNomor(); ?></div>
<table class="surat">
	<tr>
		<td width="5%">1.</td>
		<td colspan="3">Yang bertanda tangan di bawah ini :</td>
	</tr>
	<tr>
		<td width="1%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td width="4%">:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><?php print $model->getAtribut('nip_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan_pejabat'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('alamat_pejabat'); ?></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">Dengan ini memberi Surat Keterangan Jalan Kepada :</td>
	</tr>
	
	<tr>
		<td width="5%">&nbsp;</td>
		<td width="30%">Nama</td>
		<td>:</td>
		<td width="65%"><?php print $model->getAtribut('nama_pegawai'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('jabatan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Tujuan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('tujuan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Keperluan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('keperluan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kendaraan</td>
		<td>:</td>
		<td><?php print $model->getAtribut('kendaraan'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Berangkat</td>
		<td>:</td>
		<td><?php print $model->getAtribut('berangkat'); ?></td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>Kembali</td>
		<td>:</td>
		<td><?php print $model->getAtribut('kembali'); ?></td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>Pengikut</td>
		<td>:</td>
		<td><?php print $model->getAtribut('pengikut'); ?></td>
	</tr>	

	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td></td>
		<td colspan="3">Demikian Surat Keterangan Jalan ini dibuat, dan kepada petugas dimohonkan bantuan Seperlunya untuk kelancaran perjalanan. </td>
	</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 65px">Dikeluarkan Di :<?php print $model->getAtribut('tempat_dikeluarkan'); ?></td>
	</tr>
	<tr>
		<td style="width:56%"></td>
		<td style="padding-left: 65px">Pada Tanggal :<?php print $model->getAtribut('tanggal'); ?></td>
	</tr>
	<hr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</u></td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
	
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>