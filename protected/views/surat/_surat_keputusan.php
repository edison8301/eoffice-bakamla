<style>
td {
	vertical-align:top;
}
textarea {
	width: 500px;
}

</style>
<div style="border:1px solid #000;padding:20px; font-weight: bold">

	<div style="text-align:center;font-weight:bold;font-size:14px">KEPUTUSAN</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
		<input placeholder="NAMA PEJABAT PEMBERI KEPUTUSAN" style="text-align: center; width: 40%;" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>">
		<div style="text-align:center;font-weight:bold;font-size:14px">BADAN KEAMANAN LAUT RI</div>
	</div>
	<div style="text-align:center;font-weight:bold">Nomor: <?php print $model->getNomor(); ?></div>
	<div style="text-align:center;font-weight:bold;font-size:14px">TENTANG</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
			<input style="text-align: center; width: 35%;" placeholder="TUJUAN DIBUATNYA SKEP" type="text" name="SuratAtribut[tujuan_skep]" value="<?php print $model->getAtribut('tujuan_skep'); ?>">
	</div>
	<div>&nbsp;</div>
	<div style="text-align:center;font-weight:bold;font-size:14px">
			<input style="text-align: center; width: 50%" placeholder="NAMA PEJABAT YANG MENANDATANGANI SKEP" type="text" name="SuratAtribut[pejabat_penandatangan]" value="<?php print $model->getAtribut('pejabat_penandatangan'); ?>">
	</div>
	
	<div>&nbsp;</div>
	<div>&nbsp;</div>

	<table class="surat">
	<tr>
		<td>&nbsp;</td>
		<td width="20%">Menimbang</td>
		<td>: </td>
		<td><textarea placeholder ="Dasar Dibuatnya SKEP" name="SuratAtribut[menimbang]" rows="1" cols="40"><?php print $model->getAtribut('menimbang'); ?></textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Mengingat</td>
		<td>: </td>
		<td><textarea placeholder ="undang-undang/perpres/kepres yang mendasari SKEP tersebut" name="SuratAtribut[mengingat]" rows="1" cols="40"><?php print $model->getAtribut('mengingat'); ?></textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Memperhatikan</td>
		<td>: </td>
		<td><textarea placeholder ="Dasar Surat Sebelumnya" name="SuratAtribut[memperhatikan]" rows="1" cols="40"><?php print $model->getAtribut('memperhatikan'); ?></textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Menetapkan</td>
		<td>: </td>
		<td>KEPUTUSAN 
				<input placeholder="(Pejabat Penandatangan SKEP)" style="width: 40%" type="text" name="SuratAtribut[nama_pejabat_inti]" value="<?php print $model->getAtribut('nama_pejabat_inti'); ?>">
			TENTANG
				<textarea placeholder ="Tujuan Dibuatnya SKEP" name="SuratAtribut[tujuan_skep_inti]" rows="1" cols="40"><?php print $model->getAtribut('tujuan_skep_inti'); ?></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>PERTAMA</td>
		<td>: </td>
		<td><textarea placeholder ="Isi SKEP" name="SuratAtribut[isi_skep]" rows="1" cols="40"><?php print $model->	getAtribut('isi_skep'); ?></textarea></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>KEDUA</td>
		<td>: </td>
		<td><input placeholder="Jangka Waktu Pelaksanaan" style="width: 40%" type="text" name="SuratAtribut[jangka_waktu]" value="<?php print $model->getAtribut('jangka_waktu'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>KETIGA</td>
		<td></td>
		<td>Keputusan ini berlaku sejak tanggal ditetapkan </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td colspan="3"> SALINAN Keputusan ini disampaikan kepada Yth.<br>
			<textarea placeholder ="Uraian Nama Pejabat" name="SuratAtribut[disampaikan]" rows="1" cols="40"><?php print $model->	getAtribut('disampaikan'); ?></textarea></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>PETIKAN Keputusan ini disampaikan kepada yang bersangkutan untuk digunakan sebagaimana mestinya.</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	</table>

</div>