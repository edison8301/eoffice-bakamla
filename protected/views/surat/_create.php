<?php /* Yii::app()->clientScript->registerScript("updatePenandatangan","
		$('#Surat_id_surat_jenis').change(function() {
			var id_surat_jenis = $(this).val();
			alert(id_surat_jenis);
			$.ajax({
				url: ".Yii::app()->controller->createUrl()
			})
			$('#Surat_id_jabatan_penandatangan').html();
		})

"); */ ?>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'surat-keluar-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal'
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">

	<?php echo $form->dropDownListGroup($model,'id_surat_jenis',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(SuratJenis::model()->findAll(),'id','nama'),
				'htmlOptions'=>array(
					'empty'=>'-- Pilih Jenis Surat --',
					/*
					'onchange'=>CHtml::ajax(array(
						'type'=>'GET',
						'url'=>array('surat/ajaxListPenandatangan','id_surat_jenis'=>'js:$("#Surat_id_surat_jenis").val()'),
						//'url'=>array('surat/ajaxListPenandatangan','id_surat_jenis'=>'1'),
						'success'=>'function(result) {
							$("#tes").html(result);
						}'
					)) */
				)
			)
	)); ?>

	<?php echo $form->dropDownListGroup($model,'id_surat_sifat',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array(
				'data'=>CHtml::listData(SuratSifat::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>

	<?php echo $form->dropDownListGroup($model,'id_jabatan_penandatangan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array(
				'data'=>Pegawai::getListPenandatangan(),
				'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Penandatangan --',)
			)
	)); ?>

	<?php /* echo $form->dropDownListGroup($model,'penomoran',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
			'widgetOptions'=>array(
				'data'=>array('1'=>'Pada Saat Surat Ditandatangani','2'=>'Pada Saat Surat Dibuat'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); */ ?>

	<?php echo $form->textAreaGroup($model,'ringkasan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-6'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('rows'=>'3')
			)
	)); ?>

	<div class="form-group">
		<?php echo $form->hiddenField($model,'username_pengaju',array('class'=>'span5')); ?>
		<label class="col-sm-3 control-label">Nama Pengaju</label>
		<div class="col-sm-9">
		<input type="text" class="form-control" style="width:300px;display:inline" id="nama_pengaju" value="<?php //echo $model->getNamaPengaju(); ?>" readonly>
			<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'[..]',		
					'context'=>'danger',
					'size'=>'',
					'htmlOptions'=>array(
						'onClick'=>'$("#dialogPengaju").dialog("open"); return false;',
						'value'=>'add',
						'style'=>'margin-bottom:3px',
					),
			)); ?>
			<?php $this->widget('booster.widgets.TbButton', array(
					'label'=>'X',		
					'context'=>'danger',
					'size'=>'',
					'htmlOptions'=>array(
						'onClick'=>'$("#Surat_username_pengaju").val(null); $("#nama_pengaju").val(null); return false;',
						'value'=>'add',
						'style'=>'margin-bottom:3px',
					),
			)); ?>
		</div>
	</div>


	<?php echo $form->dropDownListGroup($model,'paraf',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-2'),
			'widgetOptions'=>array(
				'data'=>array('1'=>'Otomatis','2'=>'Manual'),
				'htmlOptions'=>array('rows'=>'3')
			)
	)); ?>



	<?php if(User::isAdmin()) { ?>
	<?php echo $form->dropDownListGroup($model,'id_surat_status',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(SuratStatus::model()->findAll(),'id','nama'),
				'htmlOptions'=>array('class'=>'span5')
			)
	)); ?>
	
	<?php echo $form->fileFieldGroup($model,'lampiran',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php } ?>
	</div>

	<div class="well" style="text-align:right">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Buat Draf Surat',
			)); ?>
	</div>
	

<?php $this->endWidget(); ?>


<?php //Simulasi Dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPengaju',
    'options'=>array(
        'title'=>'Daftar Pegawai',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
    $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped hover condensed',
		'dataProvider'=>Pegawai::model()->dialog(),
		'filter'=>Pegawai::model(),
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:left',
				),
			),
			array(
		      	'name'=>'nama',
		      	'value'=>'$data->nama',
			),
			array(
				'name'=>'id_jabatan',
				'header'=>'Jabatan',
				'value'=>'$data->getRelationField("jabatan","nama")',
			),	
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+", 
					array("name" => "send_insuredcode", 
					"id" => "send_insuredcode", 
					"onClick" => "$(\"#dialogPengaju\").dialog(\"close\");
							    $(\"#Surat_username_pengaju\").val(\"$data->username\");
							    $(\"#nama_pengaju\").val(\"$data->nama\");
					"))',
					'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'30px',
						'class'=>'btn btn-xs'
					),
        	),
		),
	));
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer

?>