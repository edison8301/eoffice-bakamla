<style>
td {
	vertical-align:top;
}
</style>
<?php $tab = ''; for($i=1;$i<=10;$i++) $tab .= '&nbsp;'; ?>
<table>
	<tr>
		<td width="8%">Perihal :</td>
		<td width="60%"><?php print $model->getAtribut('perihal'); ?></td>
		<td colspan="2" width="">
			Jakarta <?php print $model->getAtribut('tanggal'); ?>
					<?php print $model->getAtribut('bulan'); ?>
					<?php print $model->getAtribut('tahun'); ?>
		</td>

	</tr>
	<tr>
		<td rowspan="15">&nbsp;</td>
		<td  rowspan="15">&nbsp;</td>
		
	</tr>
	<tr>
		<td width="2px">1.</td>
		<td>Para Pejabat Eselon I;</td>
	</tr>
	<tr>
		<td>2</td>
		<td>Para Deputi Bakamla;</td>	
	</tr>
	<tr>
		<td>3.</td>	
		<td>Inspektur;</td>
	</tr>
	<tr>
		<td>4.</td>
		<td>Para Kepala Biro/Direktur;</td>	
	</tr>
	<tr>
		<td>5.</td>	
		<td>Kepala Kantor Kamla Zona Maritim Wilayah Barat, Timur & Tengah;</td>
	</tr>
	<tr>
		<td>6.</td>
		<td>Para Pejabat Eselon III;</td>	
	</tr>	
	<tr>
		<td>7.</td>
		<td>Para Anggota IK2MI, URCL, dan ULP;</td>	
	</tr>
	<tr>
		<td>8.</td>
		<td>Para Pejabat Eselon IV;</td>	
	</tr>
	<tr>
		<td>9.</td>
		<td>Para Dan Kapal Patroli 4801, 4802, dan 4803;</td>	
	</tr>
	<tr>
		<td>10.</td>
		<td>Para Pawas kapal, Pangkalan armada Kamla, Stasiun Pemantau Kamla dan Kesla;</td>	
	</tr>
	<tr>
		<td>11.</td>
		<td>Para Kepala Stasiun Pemantau Kamladan Kesla, Kepala Stasiun  Bumi, Pangkalan Armada Kamla</td>	
	</tr>
	<tr>
		<td>12.</td>
		<td>Para Karyawan/Karyawati.</td>	
	</tr>	
	<tr>
		<td></td>
		<td>di</td>	
	</tr>
	<tr>
		<td></td>
		<td style="text-align: center;"><u>Lingkungan Bakamla</u></td>	
	</tr>						
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	</table>
	<table>
	<tr>
		<td width="40px">&nbsp;</td>
		<td colspan="3">Dengan Hormat</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	

	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?php print $tab; ?>Dalam rangka <?php print $model->getAtribut('isi'); ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><?php print $tab; ?>Demikian atas perhatian diucapkan terima kasih.</td>
	</tr>	
	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
 	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold"><u>DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>
<div>&nbsp;</div>

<?php $this->renderPartial('_tembusan',array('model'=>$model)); ?>