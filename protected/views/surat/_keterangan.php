<div style="border:1px solid #000;padding:20px">

	<div style="text-align:center;font-weight:bold;font-size:18px">SURAT KETERANGAN</div>
	<div style="text-align:center;font-weight:bold"><?php $model->setNomor(); print $model->nomor; ?></div>

	<div>&nbsp;</div>

	<table width="100%" class="surat">
	<tr>
		<td></td>
		<td colspan="3">Yang Bertanda Tangan di Bawah Ini</td>
	</tr>
	<tr>
		<td width="1%">&nbsp;</td>
		<td width="20%">Nama</td>
		<td width="2%">:</td>
		<td><input placeholder="Nama Pejabat Yang Berwenang" size="50" type="text" name="SuratAtribut[nama_pejabat]" value="<?php print $model->getAtribut('nama_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="No Induk Pejabat Yang Berwenang" size="50" type="text" name="SuratAtribut[nip_pejabat]" value="<?php print $model->getAtribut('nip_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><input placeholder="Golongan Pejabat Yang Berwenang" size="50" type="text" name="SuratAtribut[pangkat_golongan_pejabat]" value="<?php print $model->getAtribut('pangkat_golongan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Pejabat Yang Berwenang" size="50" type="text" name="SuratAtribut[jabatan_pejabat]" value="<?php print $model->getAtribut('jabatan_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Satker Kerja" size="50" type="text" name="SuratAtribut[unit_kerja_pejabat]" value="<?php print $model->getAtribut('unit_kerja_pejabat'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" rowspan="2">Dengan ini Menerangkan Bahawa :</td>
	</tr>	
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nama</td>
		<td>:</td>
		<td><input placeholder="Nama Pegawai" size="50" type="text" name="SuratAtribut[nama_pegawai]" value="<?php print $model->getAtribut('nama_pegawai'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>NIP/NRP</td>
		<td>:</td>
		<td><input placeholder="No Induk Pegawai" size="50" type="text" name="SuratAtribut[nip]" value="<?php print $model->getAtribut('nip'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Pangkat/Ruang Gol</td>
		<td>:</td>
		<td><input placeholder="Golongan Pegawai" size="50" type="text" name="SuratAtribut[pangkat_golongan]" value="<?php print $model->getAtribut('pangkat_golongan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Jabatan</td>
		<td>:</td>
		<td><input placeholder="Jabatan Pegawai" size="50" type="text" name="SuratAtribut[jabatan]" value="<?php print $model->getAtribut('jabatan'); ?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Unit Kerja</td>
		<td>:</td>
		<td><input placeholder="Satker Kerja" size="50" type="text" name="SuratAtribut[unit_kerja]" value="<?php print $model->getAtribut('unit_kerja'); ?>"></td>
	</tr>
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>		
	<tr>
		<td>&nbsp;</td>
	</tr>
		<td colspan="4">Adalah benar Pegawai Badan Keamanan Laut Republik Indonesia dengan Nama dan Jabatan sebagaimana tersebut di atas yang beralamatkan di <input placeholder="Alamat Instansi" size="30" type="text" name="SuratAtribut[alamat_instansi]" value="<?php print $model->getAtribut('alamat_instansi'); ?>">. </td>
	<tr>	
		<td colspan="4">&nbsp;</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"> Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya. </td>
	</tr>

	</table>
	<div>&nbsp;</div>
	<table width="100%" style="" class="surat">
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">AN. KEPALA BADAN KEAMANAN LAUT</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">SEKRETARIS UTAMA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">DICKY R. MUNAF</td>
	</tr>
	<tr>
		<td style="width:50%"></td>
		<td style="width:50%" style="text-align:center;font-weight:bold">NIP. 19610527 198503 1 001</td>
	</tr>
	</table>

</div>
