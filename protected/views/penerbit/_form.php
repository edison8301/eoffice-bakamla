<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'penerbit-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->select2Group($model,'id_jabatan_penandatangan',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5'),
				'data'=>CHtml::listData(Jabatan::findAllStruktural(),'id','nama'),
				'options' => array(
					'placeholder' => 'Ketik Nama Jabatan Struktural',
					'minimumInputLength'=>2,
				),
			)
	)); ?>

	<?php echo $form->select2Group($model,'id_jabatan_penerbit',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'hint'=>'<b>Ket:</b> Diisi jika penerbit merupakan pejabat struktural',
			'widgetOptions'=>array(
				'htmlOptions'=>array('empty'=>'-- Kosong --'),
				'data'=>CHtml::listData(Jabatan::findAllStruktural(),'id','nama'),
				'options' => array(
					'placeholder' => 'Ketik Nama Jabatan Struktural',
					'minimumInputLength'=>2,
				),
			)
	)); ?>

	<?php echo $form->select2Group($model,'username_penerbit',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'hint'=>'<b>Ket:</b> Diisi jika penerbit merupakan staf fungsional',
			'widgetOptions'=>array(
				'htmlOptions'=>array('empty'=>'-- Kosong --'),	
				'data'=>CHtml::listData(Pegawai::model()->findAll(),'username','nama'),
				'options' => array(
					'placeholder' => 'Ketik Nama Pegawai Fungsional',
					'minimumInputLength'=>2,
				),
			)
	)); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
	)); ?>
	</div>

<?php $this->endWidget(); ?>
