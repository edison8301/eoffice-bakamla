<?php
$this->breadcrumbs=array(
	'Penerbits'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Penerbit','url'=>array('index')),
array('label'=>'Create Penerbit','url'=>array('create')),
array('label'=>'Update Penerbit','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Penerbit','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Penerbit','url'=>array('admin')),
);
?>

<h1>View Penerbit #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_jabatan_penandatangan',
		'id_jabatan_penerbit',
		'username_penerbit',
),
)); ?>
