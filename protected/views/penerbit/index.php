<?php
$this->breadcrumbs=array(
	'Penerbit Surat',
);
?>

<h1>Penerbit Surat</h1>

<div>&nbsp;</div>

<table class="table">
<thead>
<tr>
	<th width="5%">No</th>
	<th width="45%">Penandatangan</th>
	<th width="45%">Penerbit</th>
	<th width="5%">&nbsp;</th>
</tr>
</thead>
<?php
	$criteria = new CDbCriteria;
	$criteria->order = 'id_jabatan_penandatangan ASC';
?>
<?php $i=1; foreach(Penerbit::model()->findAll($criteria) as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->getRelation("jabatan_penandatangan","nama"); ?></td>
	<td>
		<?php if($data->id_jabatan_penerbit!=null) print $data->getRelation("jabatan_penerbit","nama"); ?>
		<?php if($data->username_penerbit!=null) print $data->getPegawaiPenerbit(); ?>
	</td>
	<td>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array("penerbit/directDelete","id"=>$data->id),array('confirm'=>'Yakin akan menghapus data?')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'size'=>'small',
		'label'=>'Tambah Penandatangan',
		'icon'=>'plus',
		'url'=>array('create')
	)); ?>
</div>